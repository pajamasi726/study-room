package com.study.admin.controller;


import com.study.admin.annotations.PageController;
import com.study.admin.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@PageController
@RequestMapping("/main/pages")
public class MainController {


    String[] arrays = {"1","2"};

    @RequestMapping("/index")
    public ModelAndView index() throws CustomException {
        return new ModelAndView("index");
    }
    @RequestMapping("/index2")
    public ModelAndView index2() throws CustomException {
        return new ModelAndView("index");
    }
    @RequestMapping("/index3")
    public ModelAndView index3() throws CustomException {
        return new ModelAndView("index");
    }
}
