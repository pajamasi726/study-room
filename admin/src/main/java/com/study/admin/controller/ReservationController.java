package com.study.admin.controller;

import com.study.common.service.RoomTimeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/reservation")
public class ReservationController {

    @Autowired
    private RoomTimeService roomTimeService;

    @ResponseBody
    @RequestMapping("/roomTime")
    public String reservation(){
        // todo 하경 2L : 유빈 3L : 종모 4L
        return roomTimeService.roomReservation(2L);
    }
}
