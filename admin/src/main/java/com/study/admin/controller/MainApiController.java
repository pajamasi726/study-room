package com.study.admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/main/api")
public class MainApiController {

    @RequestMapping("/index")
    public String index1(){
        log.info("index api init");
        return "index";
    }

}
