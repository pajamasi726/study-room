package com.study.admin.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomException extends Exception{
    private static final long serialVerssionUID = -1L;
    private String customMessage;
}
