package com.study.admin.config;

import com.study.admin.interceptor.Level1Interceptor;
import com.study.admin.interceptor.Level2Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    String[] excludePaths = {
            "/api/ping",            // jenkins 배포
            "health_check.html"     // L7 장비가 확인
    };

    @Autowired
    private Level1Interceptor level1Interceptor;
    @Autowired
    private Level2Interceptor level2Interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(level1Interceptor).excludePathPatterns(excludePaths); // 1단계
        registry.addInterceptor(level2Interceptor).excludePathPatterns(excludePaths); // 2단계
    }
}
