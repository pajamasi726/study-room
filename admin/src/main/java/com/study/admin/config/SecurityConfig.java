package com.study.admin.config;


import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    // http에 관한보안 설정
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.headers().frameOptions().sameOrigin();

        // filter
        http.authorizeRequests().anyRequest().permitAll();

        http.exceptionHandling().accessDeniedPage("/pages/exception/reject");
    }
}
