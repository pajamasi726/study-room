package com.study.admin.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class GlobalAdvice {
    @ExceptionHandler(value = ArrayIndexOutOfBoundsException.class)
    public ModelAndView outBoundException(HttpServletRequest request, Exception e){
        return new ModelAndView("/error");
    }
}
