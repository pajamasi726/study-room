package com.study.admin;

import com.study.common.ifs.DrawInterface;
import com.study.common.service.example.FacadeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdminApplication.class)
public class facadeTest {

    @Autowired
    private FacadeService facadeService;

    @Test
    public void facadeTest(){

        DrawInterface circle = facadeService.getCircle();
        circle.draw();


        DrawInterface rectangle = facadeService.getRectangle();
        rectangle.draw();
    }
}
