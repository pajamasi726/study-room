# 스터디룸 예약 플랫폼

- [프로젝트의 목표](#1)
- [아키텍처](#2)
- [소스 코드 구조](#3)
- 개발
  - [common](#4)
  - [api](#5)
  - [mockbiller](#6)
  - [front](#7)



#### 프로젝트의 목표 {#1}

스터디룸 예약 플랫폼 프로젝트는 각기 다른 스티디룸 카페의 정보를 한 곳에 모아 사용자가 이를 이용하여 예약까지 할 수 있도록 만든 프로젝트입니다.



#### 아키텍처 {#2}

![](./project_architecture.PNG "아키텍처")



#### 코드 구조{#3}

![](./source_code_struct.PNG)

[gradle setting](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/build.gradle?at=master&fileviewer=file-view-default)

#### 개발

##### Common(공통 모듈){#4}

공통적으로 사용하는 Dto, Entity, Service, Repository 등..

[소스코드](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/common/src/main/java/com/study/common/?at=master)

##### Api 서버

[기능]

- 데이터 등록
  [DTO](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/common/src/main/java/com/study/common/dto/external/register/?at=master), [Controller](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/api/src/main/java/com/study/api/controller/RegisterController.java?at=master&fileviewer=file-view-default), [Service](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/api/src/main/java/com/study/api/process/ExternalRegisterProcess.java?at=master&fileviewer=file-view-default), [Validator](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/api/src/main/java/com/study/api/util/RegisterRequestValidator.java?at=master&fileviewer=file-view-default)
- 예약 취소
  [DTO](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/common/src/main/java/com/study/common/dto/external/holding/HoldingCancelRequest.java?at=master&fileviewer=file-view-default), [Controller](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/api/src/main/java/com/study/api/controller/externalApi/holding/HoldingCancelApiController.java?at=master&fileviewer=file-view-default), [Service](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/api/src/main/java/com/study/api/service/external/HoldingCancelService.java?at=master&fileviewer=file-view-default) 

##### TestBiller

[기능]

- 예약

  [Service](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/testbiller/src/main/java/com/biller/testbiller/process/reservation/MockBillerReservationProcess.java?at=master&fileviewer=file-view-default), [시간 체크](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/testbiller/src/main/java/com/biller/testbiller/service/RoomTimeTableLogicService.java?at=master&fileviewer=file-view-default) 

##### Front 서버

- Spring security
  [설정](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/front/src/main/java/com/study/front/auth/SecurityConfig.java?at=master&fileviewer=file-view-default), [회원 판별](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/front/src/main/java/com/study/front/auth/UserPrincipal.java?at=master&fileviewer=file-view-default), [로그인 로그아웃 후 후속 작업](https://bitbucket.org/pajamasi726/study-room/src/4e3920a1597bbeb4b3cd92a17429cc008abf870a/front/src/main/java/com/study/front/handler/?at=master), 

