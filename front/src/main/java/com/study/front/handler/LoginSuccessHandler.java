package com.study.front.handler;

import com.study.common.entity.User;
import com.study.common.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;

@Service
@Slf4j
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        // 로그인후 이동할 페이지
        setDefaultTargetUrl("/pages/index");
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // todo login success 후 해야 할일 정의
        String userId = authentication.getName(); // 로그인에 성공한 사람의 id
        log.info("로그인 성공 : {}", userId);
        User user = userRepository.findByUserId(userId).get();

        // 마지막 로그인 일자 업데이트
        user.setLoginAccessDate(LocalDateTime.now());
        user.setFailCount(0);
        userRepository.save(user);

        SavedRequest savedRequest = (SavedRequest)request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        if(!Objects.isNull(savedRequest)) {
            String prev = savedRequest.getRedirectUrl();
            log.info("prev : {}",prev);
            response.sendRedirect(prev);
        }
        super.onAuthenticationSuccess(request, response, authentication);

    }
}
