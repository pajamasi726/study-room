package com.study.front.handler;

import com.study.common.entity.User;
import com.study.common.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
@Service
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private UserRepository userRepository;

    @PostConstruct
    public void init(){
        setDefaultFailureUrl("/pages/login/loginForm");
    }
    @Autowired
    public LoginFailureHandler(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("로그인 실패 exception : {}, message : {}",exception.getClass(), exception.getMessage());
        // 로그인 실패시 적용할 로직

        if(exception instanceof UsernameNotFoundException){
            //유저 없음
        }else if(exception instanceof BadCredentialsException){
            //비밀번호가 틀렸을 때 후속 작업
            User user = userRepository.findByUserId(request.getParameter("id")).get();
            user.setFailCount(user.getFailCount()+1);
            userRepository.save(user);
        }
        response.setContentType("text/html; charset=UTF-8;");
        PrintWriter writer = response.getWriter();
        writer.println("<script>alert('아이디 및 비밀번호를 확인하세요');history.go(-1);</script>");
        writer.flush();
    }
}
