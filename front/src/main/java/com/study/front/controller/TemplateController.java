package com.study.front.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/pages")
public class TemplateController {

    // controller = > html file
    // restController => json result

    @RequestMapping("/search")
    public String search(){
        return "search";
    }
}
