package com.study.front.controller.page.index;

import com.study.front.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
@RequestMapping("/pages")
public class IndexPageController {

    @Autowired
    private CacheService cacheService;

    @RequestMapping("/index")
    public String index(HttpServletRequest httpServletRequest){
        log.info("index");
        return "index";
    }
}
