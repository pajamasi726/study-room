package com.study.front.controller.page.cafe;

import com.study.front.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
@RequestMapping("/pages/cafe")
public class CafePageController {

    @Autowired
    private CacheService cacheService;

    @RequestMapping("/info/{cafeId}")
    public ModelAndView info(@PathVariable(name = "cafeId") Long cafeId){
        log.info("[CafePageController] info cafeId : {}",cafeId);
        ModelAndView modelAndView = new ModelAndView("cafe/info");
        modelAndView.addObject("cafeId",cafeId);
        return modelAndView;
    }
}
