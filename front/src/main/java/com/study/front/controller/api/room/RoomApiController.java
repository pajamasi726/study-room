package com.study.front.controller.api.room;

import com.study.common.dto.internal.room.RoomInfoResponse;
import com.study.front.service.room.RoomApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/api/room")
public class RoomApiController {

    @Autowired
    private RoomApiService roomApiService;

    @RequestMapping(value = "info",method = RequestMethod.GET)
    public RoomInfoResponse info(@RequestParam Long id){
        log.info("id :{}",id);
        return roomApiService.getRoomInfo(id);
    }
}
