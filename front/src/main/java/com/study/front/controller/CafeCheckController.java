package com.study.front.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cafe")
public class CafeCheckController {

    @RequestMapping("/search")
    public String search(){
       return "check";
    }
}
