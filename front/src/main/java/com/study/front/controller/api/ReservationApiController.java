package com.study.front.controller.api;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;
import com.study.front.process.FrontHoldingProcess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/reservation")
public class ReservationApiController {

    @Autowired
    private FrontHoldingProcess frontHoldingProcess;

    @RequestMapping("/holding")
    public CommonHeader<InternalHoldingResponse> holding(@RequestBody CommonHeader<InternalHoldingRequest> request){
        log.info("[ReservationApiController holding] request param : {}",request);
        return frontHoldingProcess.holding(request);
    }

}
