package com.study.front.controller.api;

import com.study.common.dto.external.holding.HoldingFrontRequest;
import com.study.front.auth.UserPrincipal;
import com.study.front.service.HoldingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/platform/reservation")
public class HoldingController {

    @Autowired
    private HoldingService holdingService;

    @RequestMapping(value = "/holding",method = RequestMethod.GET)
    public String holding(Authentication auth ,HoldingFrontRequest request){

        UserPrincipal principal = (UserPrincipal) auth.getPrincipal();
        log.info("request id = {}",principal.getUserId());
        request.setUserId(principal.getUserId());
        log.info("[HoldingController] request :  {}",request);
        boolean result = holdingService.putMessage(request);
        if(result)
            return "예약 접수를 완료하였습니다.";
        else
            return "예약 접수에 실패하였습니다";
    }
}
