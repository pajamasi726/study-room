package com.study.front.controller.page.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@Slf4j
@RequestMapping("/pages/login")
public class LoginPageController {
    @RequestMapping(value = "/loginForm",method = RequestMethod.GET)
    public ModelAndView page(){
        return new ModelAndView("login/loginForm");
    }

}
