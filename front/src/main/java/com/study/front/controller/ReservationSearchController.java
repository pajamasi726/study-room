package com.study.front.controller;

import com.study.common.dto.ReservationSearchRequest;
import com.study.common.dto.ReservationSearchResponse;
import com.study.front.service.ReservationSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/platform/reservation")
public class ReservationSearchController {

    @Autowired
    private ReservationSearchService reservationSearchService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ReservationSearchResponse search(ReservationSearchRequest request) {
        log.info("[Front] ReservationSearchController request:{}", request);
        return reservationSearchService.search(request);
    }


}
