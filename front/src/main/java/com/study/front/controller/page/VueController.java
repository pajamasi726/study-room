package com.study.front.controller.page;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/pages")
public class VueController {

    @RequestMapping("/vueExam01")
    public String exam01(){
        return "vueExam01";
    }
}
