package com.study.front.controller.api;

import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/api/cafe")
public class SearchAddressController {

    @Value("${url.api.server}") //목적지URL
    private String apiServer;

    @Autowired
    private RestTemplateService restTemplateService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public CafeListResponse search(@RequestParam String keyword){
        log.info("keyword:{}",keyword);

        //uri
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer)
                .path("/api/cafe/search")
                .queryParam("keyword",keyword)
                .build()
                .toUri();

        //response type
        ParameterizedTypeReference<CafeListResponse> type = new ParameterizedTypeReference<CafeListResponse>(){};

        try {
            return restTemplateService.send(uri, HttpMethod.GET,null,type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new CafeListResponse();
    }


}
