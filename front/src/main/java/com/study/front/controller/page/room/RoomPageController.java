package com.study.front.controller.page.room;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/room")
public class RoomPageController {

    @RequestMapping("/info/{roomId}")
    public ModelAndView info(@PathVariable(name = "roomId") Long roomId){
        log.info("RoomPageController info : {}",roomId);
        ModelAndView modelAndView = new ModelAndView("room/info");
        modelAndView.addObject("roomId",roomId);
        return modelAndView;
    }
}
