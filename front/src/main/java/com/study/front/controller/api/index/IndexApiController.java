package com.study.front.controller.api.index;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.dto.internal.index.topList.TopListResponse;
import com.study.common.dto.internal.search.CafeSearchRequest;
import com.study.front.service.FrontMainApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/api/index")
public class IndexApiController {

    @Autowired
    private FrontMainApiService frontMainApiService;


    @RequestMapping(value = "/cafeList")
    public CommonHeader<CafeListResponse> cafeList(@RequestParam(name = "page") int page){
        log.info("request page = {}",page);
        return frontMainApiService.cafeList(page);
    }

    @RequestMapping(value = "/cafeListAll",method = RequestMethod.GET)
    public CafeListResponse cafeListAll(){
        return frontMainApiService.cafeListAll();
    }

    @RequestMapping(value = "/topList")
    public TopListResponse topList(){
        return frontMainApiService.topList();
    }

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public CafeListResponse search(CafeSearchRequest request){
        log.info("type ={}, word={}",request.getSearchType(),request.getWord());
        return frontMainApiService.search(request);
    }
}
