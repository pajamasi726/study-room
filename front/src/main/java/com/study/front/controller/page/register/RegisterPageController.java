package com.study.front.controller.page.register;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.register.JoinRequest;
import com.study.common.service.RestTemplateService;
import com.study.front.auth.service.EncoderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Controller
@Slf4j
@RequestMapping("/pages/regist")
public class RegisterPageController {

    @Value("${url.api.server}")
    private String apiServer;

    @Autowired
    private EncoderService encoderService;

    @Autowired
    private RestTemplateService restTemplateService;

    @RequestMapping(value = "/registerForm", method = RequestMethod.GET)
    public ModelAndView getForm() {
        return new ModelAndView("regist/registerForm");
    }

    @ResponseBody
    @RequestMapping(value = "/join", method = RequestMethod.POST)
    public String getJoin(JoinRequest joinRequest) {
        log.info("RegisterPageController : {}",joinRequest);
        joinRequest.setUserPassword(encoderService.getEncodePassword(joinRequest.getUserPassword())); //pw인코딩
        CommonHeader<JoinRequest> header = new CommonHeader<JoinRequest>()
                .setData(joinRequest)
                .setApiName("register")
                .setMessage("회원등록api");

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path("/regist/registerForm").build().toUri();

        ParameterizedTypeReference<CommonHeader> type = new ParameterizedTypeReference<CommonHeader>() {
        };
        try {
            CommonHeader response = restTemplateService.send(uri, HttpMethod.POST, header, type);
            if(response.getResponseCode().equals("OK")){
                return "OK";
            }else{
                return response.getResponseCode();
            }
        } catch (Exception e) {
            log.info(e.getLocalizedMessage());
        }
        return null;
    }
}
