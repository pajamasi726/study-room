package com.study.front.controller;

import com.study.common.dto.ReservationInfoRequest;
import com.study.common.dto.ReservationInfoResponse;
import com.study.front.service.SearchRoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api")
public class SearchRoomController {

    @Autowired
    private SearchRoomService searchRoomService;

    @RequestMapping(value = "/searchRoom",method = RequestMethod.GET)
    public ReservationInfoResponse search(ReservationInfoRequest data) {
        log.info("[front]searchRoom request: {}", data);
        return searchRoomService.search(data);
    }
}
