package com.study.front.controller;

import com.study.common.dto.CafeDto;
import com.study.common.mapper.CafeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api")
public class CafeController {

    @Autowired
    private CafeMapper cafeMapper;

    @ResponseBody
    @RequestMapping("/check")
    public List<CafeDto> check(@RequestParam Map<String,Object> checkParams){
        List<CafeDto> cafeDtoList = cafeMapper.getCafeList(checkParams);
        return cafeDtoList;
    }

}

