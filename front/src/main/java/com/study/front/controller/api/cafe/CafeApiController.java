package com.study.front.controller.api.cafe;

import com.study.common.dto.internal.index.CafeDetailsResponse;
import com.study.common.dto.internal.index.CafeDetailsRoomResponse;
import com.study.front.service.cafe.CafeApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/api/cafe")
public class CafeApiController {

    @Autowired
    private CafeApiService cafeApiService;

    @RequestMapping(value = "/info",method = RequestMethod.GET)
    public CafeDetailsResponse info(@RequestParam Long id, HttpServletRequest httpServletRequest){
        log.info("info id :{} {}",id,httpServletRequest.getRequestURI());
        return cafeApiService.getApiData(id,httpServletRequest.getRequestURI());
    }

    @RequestMapping(value = "/roomList",method = RequestMethod.GET)
    public CafeDetailsRoomResponse roomList(@RequestParam Long id, HttpServletRequest httpServletRequest){
        return cafeApiService.getApiData2(id,httpServletRequest.getRequestURI());
    }

}
