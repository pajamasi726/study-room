package com.study.front.process.holding;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;

public interface HoldingInterface {
    CommonHeader<InternalHoldingResponse> holding(CommonHeader<InternalHoldingRequest> request);
}
