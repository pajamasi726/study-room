package com.study.front.process.holding;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.holding.HoldingRequest;
import com.study.common.dto.external.holding.HoldingResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;

import java.util.Optional;

public abstract class HoldingAbstract implements HoldingInterface{
    public abstract Optional<CommonHeader<InternalHoldingResponse>> validation(CommonHeader<InternalHoldingRequest> request);
    public abstract CommonHeader<InternalHoldingResponse> process(CommonHeader<InternalHoldingRequest> request);
}
