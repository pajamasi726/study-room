package com.study.front.process;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.holding.HoldingRequest;
import com.study.common.dto.external.holding.HoldingResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;
import com.study.common.enumClass.RegisterErrorCode;
import com.study.common.service.RestTemplateService;
import com.study.front.process.holding.HoldingAbstract;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Slf4j
@Service
public class FrontHoldingProcess extends HoldingAbstract {

    @Value("${url.api.server}")
    private String apiServer;

    private final String API_NAME = "holding";

    private final String HOLDING_PATH = "/platform/api/reservation/holding";

    @Autowired
    private RestTemplateService restTemplateService;


    /** Holding 검증 부분 */
    @Override
    public Optional<CommonHeader<InternalHoldingResponse>> validation(CommonHeader<InternalHoldingRequest> request) {
        return Optional.empty();
    }

    /** Holding 요청 부분 */
    @Override
    public CommonHeader<InternalHoldingResponse> process(CommonHeader<InternalHoldingRequest> request) {
        log.info("Request {}",request);
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer)
                .path(HOLDING_PATH)
                .build()
                .toUri();

        ParameterizedTypeReference<CommonHeader<InternalHoldingResponse>> type =
                new ParameterizedTypeReference<CommonHeader<InternalHoldingResponse>>() {};

        try{
            return restTemplateService.send(uri,HttpMethod.POST,request,type);
        }catch (Exception e){
            log.error("[FrontHoldingProcess holding] Process Error : {}",e.getLocalizedMessage());
            return CommonHeader.getResponse(API_NAME, RegisterErrorCode.ERROR, null);
        }
    }

    @Override
    public CommonHeader<InternalHoldingResponse> holding(CommonHeader<InternalHoldingRequest> request) {
        return validation(request).orElseGet(()->process(request));
    }
}
