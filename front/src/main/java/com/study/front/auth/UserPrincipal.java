package com.study.front.auth;

import com.study.common.entity.User;
import com.study.common.enumClass.role.UserRole;
import com.study.common.enumClass.status.UserStatus;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Data
public class UserPrincipal implements UserDetails {
    private String password;
    private String userId;
    private UserStatus status;
    private int loginFailCount;
    private UserRole role;

    public static UserPrincipal of(User user){
        UserPrincipal principal = new UserPrincipal();
        principal.setUserId(user.getUserId());
        principal.setPassword(user.getUserPassword());
        principal.setLoginFailCount(user.getFailCount());
        principal.setStatus(user.getStatus());
        principal.setRole(user.getRole());
        return principal;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> roleSet = new HashSet<>();
        roleSet.add(new SimpleGrantedAuthority(role.name()));
        return roleSet;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userId;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        log.info("loginFailCount={}",loginFailCount);
        if(loginFailCount<5)
            return true;
        else{
            return false;
        }
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status==UserStatus.REGISTERED;
    }
}
