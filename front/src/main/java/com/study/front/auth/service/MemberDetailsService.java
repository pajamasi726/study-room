package com.study.front.auth.service;

import com.study.common.entity.User;
import com.study.common.repository.UserRepository;
import com.study.front.auth.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class MemberDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUserId(username);

        if(!optionalUser.isPresent()){
            log.info("{}을 찾을 수 없습니다.",username);
            throw new UsernameNotFoundException(username);
        }
        log.info("find user : {}",optionalUser.get());
        return UserPrincipal.of(optionalUser.get());
    }
}
