package com.study.front.auth;

import com.study.common.entity.User;
import com.study.front.CustomAuthenticationProvider;
import com.study.front.handler.LoginFailureHandler;
import com.study.front.handler.LoginSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;
    @Autowired
    private LoginFailureHandler loginFailureHandler;
    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.headers().frameOptions().sameOrigin();

        // filter
        http.authorizeRequests()
                .antMatchers("/pages/index").permitAll()
                .antMatchers("/api/index/**").permitAll()
                .antMatchers("/pages/regist/**").permitAll()
                .antMatchers("/pages/login/**").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .anyRequest()       // 그외 모든 요청
                .authenticated();   // 로그인권한

        // login
        http.formLogin()
                .loginPage("/pages/login/loginForm")               // login page html
                .loginProcessingUrl("/pages/login/login")  // post login submit
                .usernameParameter("id")                // from parameter 의 id 부분
                .passwordParameter("pw")                // from parameter 의 pw 부분
                .successHandler(loginSuccessHandler)    // login success 후 할일
                .failureHandler(loginFailureHandler);   // login failure 후 할일

        // logout
        http.logout()
                .logoutUrl("/api/service/logout")                  // post logout submit
                .logoutSuccessUrl("/pages/index")    // 로그아웃시 이동할 페이지
                .invalidateHttpSession(true)    //세션 초기화
        ;

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // 아래의 요청에 대해서는 spring security 제외
        web.ignoring()
                .antMatchers("/js/**")
                .antMatchers("/fonts/**")
                .antMatchers("/css/**")
                .antMatchers("/img/**")
                .antMatchers("/images/**")
        ;
    }
}
