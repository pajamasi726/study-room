package com.study.front.auth.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class EncoderService {
    static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public String getEncodePassword(String password){
        return encoder.encode(password);
    }


}
