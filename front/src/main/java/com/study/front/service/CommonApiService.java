package com.study.front.service;

import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class CommonApiService {

    @Value("${url.api.server}")
    private String apiServer;

    @Autowired
    private RestTemplateService restTemplateService;

    public <T> T getApiData(MultiValueMap<String,String> params, String url, ParameterizedTypeReference<T> type){
        log.info("params = {},url={}",params,url);
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(url).queryParams(params).build().toUri();
        try {
            return restTemplateService.send(uri, HttpMethod.GET,null,type);
        }catch (Exception e){
            return null;
        }
    }

    public <T> T postApiData(Object requestData,String url,ParameterizedTypeReference<T> type){
        log.info("requestData = {},url={}",requestData,url);
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(url).build().toUri();
        try{
            return restTemplateService.send(uri,HttpMethod.POST,requestData,type);
        }catch (Exception e){
            return null;
        }
    }
}
