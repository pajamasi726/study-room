package com.study.front.service.room;

import com.study.common.dto.internal.room.RoomInfoResponse;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class RoomApiService {


    @Value("${url.api.server}")
    private String apiServer;

    private static final String ROOM_INFO_URL = "/api/room/info";

    @Autowired
    private RestTemplateService restTemplateService;
    public RoomInfoResponse getRoomInfo(Long id){
        ParameterizedTypeReference<RoomInfoResponse> type = new ParameterizedTypeReference<RoomInfoResponse>() {
        };

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(ROOM_INFO_URL).queryParam("id",id).build().toUri();

        try{
            return restTemplateService.send(uri, HttpMethod.GET,null,type);
        }catch (Exception e){
            return null;
        }
    }

}
