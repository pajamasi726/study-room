package com.study.front.service.cafe;

import com.study.common.dto.internal.index.CafeDetailsResponse;
import com.study.common.dto.internal.index.CafeDetailsRoomResponse;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class CafeApiService {

    @Value("${url.api.server}")
    private String apiServer;

    @Autowired
    private RestTemplateService restTemplateService;

    public CafeDetailsResponse getApiData(Long id, String url){
        ParameterizedTypeReference<CafeDetailsResponse> type = new ParameterizedTypeReference<CafeDetailsResponse>() {
        };

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(url).queryParam("id",id).build().toUri();

        try{
            return restTemplateService.send(uri, HttpMethod.GET,null,type);
        }catch (Exception e){
            return null;
        }
    }


    public CafeDetailsRoomResponse getApiData2(Long id, String url){
        ParameterizedTypeReference<CafeDetailsRoomResponse> type = new ParameterizedTypeReference<CafeDetailsRoomResponse>() {
        };

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(url).queryParam("id",id).build().toUri();

        try{
            return restTemplateService.send(uri, HttpMethod.GET,null,type);
        }catch (Exception e){
            return null;
        }
    }



}
