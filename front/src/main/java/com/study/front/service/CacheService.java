package com.study.front.service;

import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.entity.Cafe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class CacheService {

    private RedisTemplate<String, String> redisTemplate;
    private ValueOperations<String, String> valueOperations;
    private ListOperations<String, Object> listOperations;

    @Autowired
    public CacheService(RedisTemplate<String, String> redisTemplate,RedisTemplate<String,Object> listOperations) {
        this.redisTemplate = redisTemplate;
        this.valueOperations = redisTemplate.opsForValue();
        this.listOperations = listOperations.opsForList();
    }


    public boolean write(String key, String value) {
        valueOperations.set(key, value);
        return redisTemplate.expire(key, 1, TimeUnit.HOURS);
    }

    public boolean listWrite(String key, CafeListResponse response) {
        listOperations.leftPush(key, response);
        return redisTemplate.expire(key, 5, TimeUnit.MINUTES);
    }

    public String get(String key) {
        return valueOperations.get(key);
    }

    public CafeListResponse getList(String key) {
        CafeListResponse response = (CafeListResponse) listOperations.leftPop(key);
        listWrite(key, response);
        return response;
    }

}
