package com.study.front.service;

import com.study.common.dto.external.holding.HoldingFrontRequest;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.service.RabbitMqProducer;
import com.study.common.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HoldingService {

    private RabbitMqProducer rabbitMqProducer;

    @Autowired
    public HoldingService(RabbitMqProducer rabbitMqProducer) {
        this.rabbitMqProducer = rabbitMqProducer;
    }

    public boolean putMessage(HoldingFrontRequest request) {
        CommonHeader<HoldingFrontRequest> header = new CommonHeader<>();
        header.setData(request);
        header.setApiName("holding");
        String message = JsonUtils.toJson(header);
        log.info("message : {}", message);
        try {
            rabbitMqProducer.put(message);
            return true;
        } catch (Exception e) {
            log.info("[HoldingService] can't putting message in RabbitMQ]");
            return false;
        }
    }

}
