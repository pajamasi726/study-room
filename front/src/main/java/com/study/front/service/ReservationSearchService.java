package com.study.front.service;

import com.study.common.dto.ReservationSearchRequest;
import com.study.common.dto.ReservationSearchResponse;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class ReservationSearchService {

    @Value("http://localhost:9090")
    private String apiServer;

    @Autowired
    private RestTemplateService restTemplateService;

    public ReservationSearchResponse search(ReservationSearchRequest request){
        log.info("[Front] ReservationSearchService request : {}", request);

        // TODO 유빈 POST
        //uri
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer)
                .path("/api/reservation/searchRoom")
                .queryParam("roomCode", request.getRoomCode())
                .queryParam("startDateTime", request.getStartDateTime())
                .queryParam("endDateTime", request.getEndDateTime())
                .build()
                .toUri();

        ParameterizedTypeReference<ReservationSearchResponse> type = new ParameterizedTypeReference<ReservationSearchResponse>() {};

        ReservationSearchResponse response = null;
        try {
            response = restTemplateService.send(uri, HttpMethod.GET, null, type);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
