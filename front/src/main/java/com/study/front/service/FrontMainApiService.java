package com.study.front.service;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.dto.internal.index.topList.TopListResponse;
import com.study.common.dto.internal.search.CafeSearchRequest;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class FrontMainApiService {

    @Value("${url.api.server}")
    private String apiServer;
    private final static String CAFE_LIST_ALL_URL ="/api/index/cafeListAll";
    private final static String CAFE_LIST_URL = "/api/index/cafeList";
    private final static String TOP_LIST_URL ="/api/index/topList";
    private final static String SEARCH_URL = "/api/index/search";

    private CommonApiService commonApiService;
    private RestTemplateService restTemplateService;
    private CacheService cacheService;
    @Autowired
    public FrontMainApiService(RestTemplateService restTemplateService,
                               CommonApiService commonApiService,
                               CacheService cacheService){
        this.restTemplateService = restTemplateService;
        this.commonApiService = commonApiService;
        this.cacheService = cacheService;
    }

    public CafeListResponse cafeListAll(){
        CafeListResponse response = cacheService.getList("cafeListAll");
        if(!Objects.isNull(response)){
            return response;
        }
        Long start,end;
        start = System.currentTimeMillis();
        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(CAFE_LIST_ALL_URL).build().toUri();
        ParameterizedTypeReference<CafeListResponse> type = new ParameterizedTypeReference<CafeListResponse>() {};
        try {
            end = System.currentTimeMillis();
            log.info("검색 시간 : {}초",(end-start)/1000.0);
            CafeListResponse data = restTemplateService.send(uri,HttpMethod.GET,null,type);
            cacheService.listWrite("cafeListAll",data);
            return data;
        } catch (Exception e) {
            log.info("[FrontMainApiService] cafeListAll() Error!!!!! {}",e);
        }
        return new CafeListResponse();
    }
    public CommonHeader<CafeListResponse> cafeList(int page){

        ParameterizedTypeReference<CommonHeader<CafeListResponse>> type = new ParameterizedTypeReference<CommonHeader<CafeListResponse>>() {
        };

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(CAFE_LIST_URL).queryParam("page",page).build().toUri();

        try{
            return restTemplateService.send(uri,HttpMethod.GET,null,type);
        }catch (Exception e){
            return new CommonHeader();
        }
    }

    public TopListResponse topList(){
        ParameterizedTypeReference<TopListResponse> type = new ParameterizedTypeReference<TopListResponse>() {
        };

        URI uri = UriComponentsBuilder.fromHttpUrl(apiServer).path(TOP_LIST_URL).build().toUri();

        try{
            return restTemplateService.send(uri,HttpMethod.GET,null,type);
        }catch (Exception e){
            return new TopListResponse();
        }
    }

    public CafeListResponse search(CafeSearchRequest data){

        LinkedMultiValueMap<String,String> map = new LinkedMultiValueMap<>();

        List<String> list = new ArrayList<>();
        list.add(data.getSearchType());
        //name, {강남}
        map.put("searchType",list);

        List<String> keywordList = new ArrayList<>();
        keywordList.add(data.getWord());
        //name, {강남}
        map.put("word",keywordList);


        //type
        ParameterizedTypeReference<CafeListResponse> type = new ParameterizedTypeReference<CafeListResponse>() {};

        return commonApiService.getApiData(map,SEARCH_URL,type);
    }

}
