package com.study.front.service;

import com.study.common.dto.ReservationInfoRequest;
import com.study.common.dto.ReservationInfoResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.entity.Biller;
import com.study.common.entity.Room;
import com.study.common.repository.RoomRepository;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Slf4j
@Service
public class SearchRoomService {

   // @Value("http://localhost:9090")
   // private String apiServer;

    @Autowired
    private RestTemplateService restTemplateService;

    public ReservationInfoResponse search(ReservationInfoRequest request) {

        log.info("[Front Service request]:{}", request);
        //uri
        URI uri = UriComponentsBuilder.fromHttpUrl("http://localhost:9090")
                .path("/platform/api/searchRoom")
                .queryParam("roomCode", request.getRoomCode())
                .queryParam("dateTime", request.getDateTime())
                .build()
                .toUri();

        ParameterizedTypeReference<ReservationInfoResponse> type = new ParameterizedTypeReference<ReservationInfoResponse>() {
        };

        try {
            ReservationInfoResponse response = restTemplateService.send(uri, HttpMethod.GET, null, type);
            log.info("Front responseCode = {}", response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

