package com.study.front.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.study.common","com.study.front"})
public class AppConfig {

}
