package com.study.front;

import com.study.front.auth.service.MemberDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    private MemberDetailsService memberDetailsService;

    @PostConstruct
    public void init(){
        setHideUserNotFoundExceptions(false);
        setUserDetailsService(memberDetailsService);
        setPasswordEncoder(new BCryptPasswordEncoder());
    }
}
