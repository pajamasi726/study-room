(function ($) {

    $(document).ready(function () {

        console.log("document is ready!!!");
        $("#register").click(function () {
            if (flagId || flagEm || flagPh || flagPw || flagCo) {
                console.log("click event!!!");
                $.ajax({
                    type: "POST",
                    url: "/pages/regist/join",
                    data: {
                        userId: $('#user_id').val(),
                        userPassword: $('#password').val(),
                        email: $('#email').val(),
                        phoneNumber: $('#phone_number').val()
                    },
                    success: function (response) {
                        if (response == "OK") {
                            alert('회원가입에 성공하셨습니다.');
                            window.location.href = "/pages/login/loginForm";
                        } else if(response == "DUPLICATE"){
                            alert('존재하는 아이디입니다');
                            $('#user_id').focus();
                        } else{
                            alert('회원가입에 실패하셨습니다.');
                        }
                        
                    },
                    timeout: 5000
                });

            } else {
                alert('다시 체크해주세요');
                return;
            }

        });
    });

})(jQuery);