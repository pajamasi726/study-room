(function ($) {

    /** #############################
     *  #         Header var        #
     *  ############################# **/
    var headerVar = new Vue({
        el: 'header',
        data: {
            type: "name",
            keyword: null
        },
        methods: {
            loginEvent: function () {
                window.location.href = "/pages/login/loginForm"
            },
            logoutEvent: function () {
                console.log("logout ...")
                $.ajax({
                    type: "POST",
                    url: "/api/service/logout",
                    data: {},
                    success: function () {
                        window.location.href = "/pages/index"
                    },
                    timeout: 1000
                })
            },
            registEvent: function () {
                window.location.href = '/pages/regist/registerForm'
            },
            searchEvent: function () {
                $.ajax({
                    type: "GET",
                    url: "/api/index/search?searchType=" + headerVar.type + "&word=" + headerVar.keyword,
                    success: function (response) {
                        cafeTopList.cafeList = response.cafe_list
                        headerVar.keyword = ""
                        var offset = $('#cafeTopList').offset();
                        $('html,body').animate({scrollTop: offset.top}, 400);
                        return;
                    },
                    timeout: 5000
                })
            },
        }
    })
})(jQuery);