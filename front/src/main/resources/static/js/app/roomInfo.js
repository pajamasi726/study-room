(function ($) {
    'use strict';

    // bootstrap dropdown hover
    var now = new Date();

    var roomInfo = new Vue({
        el: '#room_info',
        data: {
            roomInfo: {},
            personCount: 1,
            start: null,
            end: null,
            datetime: null,
            time: null,
        },
        methods: {
            reservation: function () {
                if(roomInfo.personCount<roomInfo.roomInfo.min_seating || roomInfo.personCount>roomInfo.roomInfo.max_seating){
                    return;
                }
                $.ajax({
                    url: '/api/platform/reservation/holding',
                    type: 'get',
                    data: {
                        datetime: roomInfo.datetime,
                        roomCode: roomInfo.roomInfo.room_code,
                        startAt: roomInfo.start,
                        endAt: roomInfo.end,
                        personCount: roomInfo.personCount
                    },
                    success: function (response) {
                        alert("success : " + response);
                        window.location.reload();
                    },
                    error: function (err) {
                        alert("error" + err);
                    }
                });
            },
            reservationCancel: function () {
                $('#reservation').slideUp('slow');
            }
        }
    })

    function initTime() {
        var dateString = moment(now).format('YYYY-MM-DD');
        var roomCode = roomInfo.roomInfo.room_code;
        console.log(roomCode);
        console.log(dateString);

        $.ajax({
            url: '/platform/api/searchRoom',
            type: 'get',
            data: {
                roomCode: roomCode,
                dateTime: dateString
            },
            success: function (response) {
                var timeTable = [];

                response.hour.forEach(function (item, index, array) {
                    if (item.ok === false) {
                        var start = moment({hour: index}).format('YYYY-MM-DD HH:mm:ss');
                        var end = moment({
                            hour: index + 1,
                        }).format('YYYY-MM-DD HH:mm:ss');
                        timeTable.push({
                            start: start,
                            end: end
                        });
                    }
                });

                $('#calendar').fullCalendar({
                    header: {
                        right: 'today,customPrevButton,customNextButton',
                        left: 'title'
                    },
                    customButtons: {
                        customPrevButton: {
                            text: 'prev',
                            click: function () {
                                updateTime(-1);
                            }
                        },
                        customNextButton: {
                            text: 'next',
                            click: function () {
                                updateTime(1)
                            }
                        }
                    },
                    selectable: true,
                    slotDuration: '00:60:00',
                    defaultView: 'agendaDay',
                    displayEventTime: false,
                    lang: 'ko',
                    events: timeTable,
                    dayClick: function (date) {
                        var val = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        var test = "{'roomCode':'" + roomCode + "', 'start':'" + val + "'}";
                        console.log("dayClick");
                    },
                    select: function (startDate, endDate) {
                        var s = new Date(startDate);
                        var e = new Date(endDate);
                        roomInfo.time = (e-s)/1000/3600;
                        console.log("select click");
                        var start = moment(startDate).format('YYYY-MM-DD HH:mm:ss');
                        var end = moment(endDate).format('YYYY-MM-DD HH:mm:ss');
                        var datetime = moment(startDate).format('YYYY-MM-DD');
                        $('#reservation').slideDown('slow');
                        roomInfo.start = start;
                        roomInfo.end = end;
                        roomInfo.datetime = datetime;
                        roomInfo.roomCode = roomCode;
                        alert(roomInfo.time)
                    }
                });

            },
            error: function () {
            }
        });
    }

    function updateTime(number){
        now.setDate(now.getDate()+number);
        var roomCode = roomInfo.roomInfo.room_code;
        var dateString = moment(now).format('YYYY-MM-DD');
        console.log("date : "+new Date(dateString).getDate())
        $.ajax({
            url: '/platform/api/searchRoom',
            type: 'get',
            data: {
                roomCode: roomCode,
                dateTime: dateString
            },
            success : function (response) {
                var timeTable = [];

                response.hour.forEach(function (item, index, array) {
                    if (item.ok === false) {
                        var start = moment({
                            date : new Date(dateString).getDate(),
                            hour: index
                        }).format('YYYY-MM-DD HH:mm:ss');
                        var end = moment({
                            date : new Date(dateString).getDate(),
                            hour: index + 1,
                        }).format('YYYY-MM-DD HH:mm:ss');
                        timeTable.push({
                            start: start,
                            end: end
                        });
                    }
                });

                if(number>0){
                    $('#calendar').fullCalendar('next');
                    $('#calendar').fullCalendar('renderEvents',timeTable,true);

                    for(var i = 0; i < timeTable.length;i++){
                        console.log(timeTable[i].start+","+timeTable[i].end)
                    }
                }else{
                    $('#calendar').fullCalendar('prev');
                    $('#calendar').fullCalendar('renderEvents',timeTable,true);
                    for(var i = 0; i < timeTable.length;i++){
                        console.log(timeTable[i].start+","+timeTable[i].end)
                    }
                }
            }
        })
    }
    $(document).ready(function () {
        console.log('ready');
        var id = $('#roomId').val();
        console.log(id);
        if (id != undefined) {
            $.get("/api/room/info?id=" + id, function (response) {
                console.log(response);
                roomInfo.roomInfo = response;
                setTimeout(initTime(), 500);
            });
        }
        $('html,body').animate({scrollTop: 0}, 400);

    });


    // loader
    var loader = function () {
        setTimeout(function () {
            if ($('#loader').length > 0) {
                $('#loader').removeClass('show');
            }
        }, 1);
    };
    loader();

    // Stellar
    $(window).stellar();


    $('nav .dropdown').hover(function () {
        var $this = $(this);
        $this.addClass('show');
        $this.find('> a').attr('aria-expanded', true);
        $this.find('.dropdown-menu').addClass('show');
    }, function () {
        var $this = $(this);
        $this.removeClass('show');
        $this.find('> a').attr('aria-expanded', false);
        $this.find('.dropdown-menu').removeClass('show');
    });


    $('#dropdown04').on('show.bs.dropdown', function () {
        console.log('show');
    });


    // home slider
    $('.home-slider').owlCarousel({
        loop: true,
        autoplay: true,
        margin: 10,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: true,
        autoplayHoverPause: true,
        items: 1,
        navText: ["<span class='ion-chevron-left'></span>", "<span class='ion-chevron-right'></span>"],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true
            }
        }
    });

    // owl carousel
    var majorCarousel = $('.js-carousel-1');
    majorCarousel.owlCarousel({
        loop: true,
        autoplay: false,
        stagePadding: 0,
        margin: 10,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false,
        dots: false,
        autoplayHoverPause: false,
        items: 3,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    });

    // cusotm owl navigation events
    $('.custom-next').click(function (event) {
        event.preventDefault();
        // majorCarousel.trigger('owl.next');
        majorCarousel.trigger('next.owl.carousel');

    })
    $('.custom-prev').click(function (event) {
        event.preventDefault();
        // majorCarousel.trigger('owl.prev');
        majorCarousel.trigger('prev.owl.carousel');
    })

    // owl carousel
    var major2Carousel = $('.js-carousel-2');
    major2Carousel.owlCarousel({
        loop: true,
        autoplay: true,
        stagePadding: 7,
        margin: 20,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false,
        autoplayHoverPause: true,
        items: 4,
        navText: ["<span class='ion-chevron-left'></span>", "<span class='ion-chevron-right'></span>"],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    });


    var contentWayPoint = function () {
        var i = 0;
        $('.element-animate').waypoint(function (direction) {

            if (direction === 'down' && !$(this.element).hasClass('element-animated')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function () {

                    $('body .element-animate.item-animate').each(function (k) {
                        var el = $(this);
                        setTimeout(function () {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn element-animated');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft element-animated');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight element-animated');
                            } else {
                                el.addClass('fadeInUp element-animated');
                            }
                            el.removeClass('item-animate');
                        }, k * 100);
                    });

                }, 100);

            }

        }, {offset: '95%'});
    };
    contentWayPoint();


})(jQuery);