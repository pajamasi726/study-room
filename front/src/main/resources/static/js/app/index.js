(function ($) {

    /** #############################
     *  #         Slider           #
     *  ############################# **/
    var homeSlider = new Vue({
        el: '#homeSlider',
        data: {
            cafeList: []
        }
    });

    /** #############################
     *  #         Section           #
     *  ############################# **/
    var cafeTopList = new Vue({
        el: '#cafeTopList',
        data: {
            startIdx: 0,                     // 화면에 보여질 시작 인덱스
            startPage : 0,                   // 하단 페이지 시작 번호
            page : 0 ,                       // 현재 페이지
            cafeList: []                     // 전체 데이터
        },
        methods: {
            requestPage : function(val){
                cafeTopList.startIdx = cafeTopList.startIdx+(val-cafeTopList.page)*9;
                cafeTopList.page = val;
            },
            prevPage: function () {
                if(cafeTopList.startPage-5<0){
                    alert('첫 페이지 입니다.')
                    return
                }
                cafeTopList.startPage = cafeTopList.startPage-5
            },
            nextPage: function () {
                if(cafeTopList.startPage+5>this.getTotalPage){
                    alert('더 이상 넘길 페이지가 존재하지 않습니다.')
                    return
                }
                cafeTopList.startPage = cafeTopList.startPage+5
            },
        },
        computed :{
            getTotalLength : function () {
                return this.cafeList.length;
            },
            getTotalPage : function () {
                var ret = Math.floor(this.cafeList.length/9) +(this.cafeList.length%9==0?0:1);
                console.log("ret="+ret);
                return ret;
            }
        },
        watch : {
            cafeList : {
                handler : function () {
                    console.log(this.cafeList)
                },
                deep : true
            }
        }
    })

    /** #############################
     *  #        Data Init          #
     *  ############################# **/
    $(document).ready(function () {

        $.get("/api/index/topList", function (header) {
            homeSlider.cafeList = header.cafe_list;
            setSlider();
        });

        $.get("/api/index/cafeListAll", function (resp) {
            cafeTopList.cafeList = resp.cafe_list;
        });
    });


    // top 4 list slider
    function setSlider() {
        setTimeout(function () {
            $('.home-slider').owlCarousel({
                loop: true,
                autoplay: true,
                margin: 10,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                nav: true,
                autoplayHoverPause: true,
                items: 1,
                navText: ["<span class='ion-chevron-left'></span>", "<span class='ion-chevron-right'></span>"],
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    600: {
                        items: 1,
                        nav: false
                    },
                    1000: {
                        items: 1,
                        nav: true
                    }
                }
            });
        }, 200);
    }

    'use strict';

    // bootstrap dropdown hover

    // loader
    var loader = function () {
        setTimeout(function () {
            if ($('#loader').length > 0) {
                $('#loader').removeClass('show');
            }
        }, 1);
    };
    loader();


    // Stellar
    $(window).stellar();


    $('nav .dropdown').hover(function () {
        var $this = $(this);
        $this.addClass('show');
        $this.find('> a').attr('aria-expanded', true);
        $this.find('.dropdown-menu').addClass('show');
    }, function () {
        var $this = $(this);
        $this.removeClass('show');
        $this.find('> a').attr('aria-expanded', false);
        $this.find('.dropdown-menu').removeClass('show');
    });


    $('#dropdown04').on('show.bs.dropdown', function () {
        console.log('show');
    });


    // owl carousel
    var majorCarousel = $('.js-carousel-1');
    majorCarousel.owlCarousel({
        loop: true,
        autoplay: false,
        stagePadding: 0,
        margin: 10,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false,
        dots: false,
        autoplayHoverPause: false,
        items: 3,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    });

    // cusotm owl navigation events
    $('.custom-next').click(function (event) {
        event.preventDefault();
        // majorCarousel.trigger('owl.next');
        majorCarousel.trigger('next.owl.carousel');

    })
    $('.custom-prev').click(function (event) {
        event.preventDefault();
        // majorCarousel.trigger('owl.prev');
        majorCarousel.trigger('prev.owl.carousel');
    })

    // owl carousel
    var major2Carousel = $('.js-carousel-2');
    major2Carousel.owlCarousel({
        loop: true,
        autoplay: true,
        stagePadding: 7,
        margin: 20,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false,
        autoplayHoverPause: true,
        items: 4,
        navText: ["<span class='ion-chevron-left'></span>", "<span class='ion-chevron-right'></span>"],
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    });


    var contentWayPoint = function () {
        var i = 0;
        $('.element-animate').waypoint(function (direction) {

            if (direction === 'down' && !$(this.element).hasClass('element-animated')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function () {

                    $('body .element-animate.item-animate').each(function (k) {
                        var el = $(this);
                        setTimeout(function () {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn element-animated');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft element-animated');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight element-animated');
                            } else {
                                el.addClass('fadeInUp element-animated');
                            }
                            el.removeClass('item-animate');
                        }, k * 100);
                    });

                }, 100);

            }

        }, {offset: '95%'});
    };
    contentWayPoint();


})(jQuery);