//전역 변수
var flagId = false;
var flagEm = false;
var flagPh = false;
var flagPw = false;
var flagCo = false;

/*아이디 타입 체크*/
function checkId() {
    var id = document.getElementById('user_id').value

    var patt = /[`~!@#$%^&*|\\\'\";:\/?]/gi;
    var kpatt = /[ㄱ-ㅎ | ㅏ-ㅣ | 가-힣]]/;
    var spatt = /\s/g;
    document.getElementById

    //맞지 않는 타입일 경우
    if ((patt.test(id) || kpatt.test(id) || spatt.test(id)) || id.length < 6) {
        console.log('block')
        document.getElementById('checkIds').style.display = 'none';
        document.getElementById('checkId').style.display = 'block';
        flagId = false;
    } else {
        console.log('none')
        document.getElementById('checkId').style.display = 'none';
        document.getElementById('checkIds').style.display = 'block';
        flagId = true;
        //return;
    }
}

/*이메일 타입 체크*/
function checkEmail() {
    var em = document.getElementById('email').value

    var epatt = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
    var result = em.match(epatt);
    document.getElementById

    //email패턴에 맞지 않는 경우
    if (!result) {
        console.log('block')
        document.getElementById('checkemails').style.display = 'none';
        document.getElementById('checkemail').style.display = 'block';
        flagEm = false;
    } else {
        console.log('none')
        document.getElementById('checkemail').style.display = 'none';
        document.getElementById('checkemails').style.display = 'block';
        flagEm = true;
    }
}

/*핸드폰 타입 체크*/
function checkPhone() {
    var ph = document.getElementById('phone_number').value

    var phpatt = /^\d{2,3}\d{3,4}\d{4}$/;
    var result = ph.match(phpatt);
    document.getElementById

    //phone패턴에 맞지 않는 경우
    if (!result) {
        console.log('block')
        document.getElementById('checkphones').style.display = 'none';
        document.getElementById('checkphone').style.display = 'block';
        flagPh = false;
    } else {
        console.log('none')
        document.getElementById('checkphone').style.display = 'none';
        document.getElementById('checkphones').style.display = 'block';
        flagPh = true;
    }
}

/*비밀번호 타입 체크*/
function checkPassword() {
    var pw = document.getElementById('password').value

    var pwpatt = /^[a-zA-Z0-9]{8,16}$/;
    var result = pw.match(pwpatt);
    document.getElementById

    if (!result) {
        console.log('block')
        document.getElementById('checkpws').style.display = 'none';
        document.getElementById('checkpw').style.display = 'block';
        flagPw = false;
    } else {
        console.log('none')
        document.getElementById('checkpw').style.display = 'none';
        document.getElementById('checkpws').style.display = 'block';
        flagPw = true;
    }
}

/*비밀번호 확인 체크*/
function checkConfirm() {
    var pw = document.getElementById('password').value
    var confirmpw = document.getElementById('confirm').value
    var spatt = /\s/g;
    var result = confirmpw.match(spatt);
    document.getElementById

    //비밀번호가 같은경우
    if (!result && pw === confirmpw && confirmpw.length >= 8) {
        console.log('block')
        document.getElementById('confirmpws').style.display = 'block';
        document.getElementById('confirmpw').style.display = 'none';
        flagCo = true;
    } else {
        console.log('none')
        document.getElementById('confirmpws').style.display = 'none';
        document.getElementById('confirmpw').style.display = 'block';
        flagCo = false;
    }
}

