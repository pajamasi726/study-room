package com.study.api.service;


import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.search.SearchRequest;
import com.study.common.dto.external.search.SearchResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Slf4j
@Service
public class SearchService {

    @Autowired
    @Qualifier("middleRestTemplate")
    private RestTemplate restTemplate;


    public ExternalHeader<SearchResponse> search(ExternalHeader<SearchRequest> request) {

        // uri
        URI uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost:1010")
                .path("/api/platform/reservation/search")
                .build()
                .encode()
                .toUri();

        ParameterizedTypeReference<ExternalHeader<SearchResponse>> type = new ParameterizedTypeReference<ExternalHeader<SearchResponse>>() {};

        //return 할거
        ExternalHeader<SearchResponse> searchResponse = null;
        try {
            searchResponse = send(uri, HttpMethod.POST, request, type);
            log.info("RESPONSE : {}",searchResponse);
        } catch (Exception e) {
            log.error("네트워크 에러 : {}",e.getLocalizedMessage());
            e.printStackTrace();
        }

        return searchResponse;
    }


    public <T> T send(URI uri, HttpMethod httpMethod, Object requestData, ParameterizedTypeReference<T> type) throws Exception {
        RequestEntity<Object> requestEntity = RequestEntity.method(httpMethod, uri).contentType(MediaType.APPLICATION_JSON_UTF8).body(requestData);
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestEntity, type);
        return responseEntity.getBody();
    }
}

