package com.study.api.service.external.search;

import com.study.api.model.ifs.SearchInterface;
import com.study.common.dto.external.search.SearchResponse;
import com.study.common.dto.internal.CommonHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CommonSearchService implements SearchInterface {

    @Override
    public CommonHeader<SearchResponse> search() {
        // todo 파라미터 검증

        // 통신

        // RESPONSE 검증

        return null;
    }

}
