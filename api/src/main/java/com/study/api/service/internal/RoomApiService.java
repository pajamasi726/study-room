package com.study.api.service.internal;

import com.study.common.dto.internal.index.CafeDetailsRoomResponse;
import com.study.common.dto.internal.room.RoomInfoResponse;
import com.study.common.entity.Cafe;
import com.study.common.entity.Room;
import com.study.common.repository.CafeRepository;

import com.study.common.service.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RoomApiService {

    @Autowired
    private CafeRepository cafeRepository;

    @Autowired
    private RoomService roomService;

    public RoomInfoResponse getRoomInfo(Long id){
        Optional<Room> optionalRoom = roomService.findById(id);
        if(optionalRoom.isPresent()){
            Room room = optionalRoom.get();

            RoomInfoResponse response = new RoomInfoResponse().setId(room.getId())
                    .setCategory(room.getCategory())
                    .setDescription(room.getDescription())
                    .setImgUrl_01(room.getImgUrl01())
                    .setImgUrl_02(room.getImgUrl02())
                    .setImgUrl_03(room.getImgUrl03())
                    .setMaxSeating(room.getMaxSeating())
                    .setMinSeating(room.getMinSeating())
                    .setTitle(room.getName())
                    .setRoomCode(room.getRoomCode())
                    .setPrice(room.getPrice())
                    ;
            return response;
        }else{
            return new RoomInfoResponse();
        }
    }

    public CafeDetailsRoomResponse roomList(Long id) {
        Optional<Cafe> optionalCafe = cafeRepository.findById(id);

        if (optionalCafe.isPresent()) {
            Cafe cafe = optionalCafe.get();
            List<Room> roomList = cafe.getRoomList();

            List<CafeDetailsRoomResponse.Room> responseRoomList = roomList.stream().map(r -> {
                CafeDetailsRoomResponse.Room room = new CafeDetailsRoomResponse.Room();
                room.setId(r.getId());
                room.setRoomName(r.getName());
                room.setPrice(r.getPrice());
                room.setImgUrl01(r.getImgUrl01());
                return room;
            }).collect(Collectors.toList());

            log.info("CAFE : {} , ROOM LIST SIZE : {}",cafe.getId(),responseRoomList.size());
            return new CafeDetailsRoomResponse().setRoomList(responseRoomList);
        } else {
            return new CafeDetailsRoomResponse().setRoomList(new ArrayList<>());
        }
    }
}
