package com.study.api.service.external.holdingConfirm;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.holdingConfirm.HoldingConfirmRequest;
import com.study.common.dto.external.holdingConfirm.HoldingConfirmResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.entity.Biller;
import com.study.common.enumClass.holdingConfirm.HoldingConfirmResponseCode;
import com.study.common.repository.BillerRepository;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Slf4j
@Service
public class HoldingConfirmService {

    static String HOLDING_CONFIRM_PATH = "/platform/api/reservation/confirm";
    static String HOLDING_CONFIRM_NAME = "confirm";

    @Autowired
    private BillerRepository billerRepository;
    @Autowired
    private RestTemplateService restTemplateService;



    public CommonHeader<HoldingConfirmResponse> holdingConfirm(CommonHeader<HoldingConfirmRequest> request){
        HoldingConfirmRequest holdingConfirmRequest = request.getData();

        //요청 전 데이터 확인
        Optional<Biller> optionalBiller = billerRepository.findByBillerCode(holdingConfirmRequest.getBillerCode());

        if(optionalBiller.isPresent()){
            Biller biller = optionalBiller.get();
            log.info("[HoldingConfirmService] biller = {}",biller);

            //응답에 따른 프로세스 작동
            return sendHoldingConfirmApi(biller,holdingConfirmRequest);
        }
        return CommonHeader.getResponse(HOLDING_CONFIRM_NAME,HoldingConfirmResponseCode.NOT_FOUNT,null);
    }

    private CommonHeader<HoldingConfirmResponse> sendHoldingConfirmApi(Biller biller,HoldingConfirmRequest holdingConfirmRequest){
        URI uri = UriComponentsBuilder
                .fromHttpUrl(biller.getIpAddress())
                .path(HOLDING_CONFIRM_PATH)
                .encode()
                .build()
                .toUri();
        //type
        ParameterizedTypeReference<ExternalHeader<HoldingConfirmResponse>> type
                = new ParameterizedTypeReference<ExternalHeader<HoldingConfirmResponse>>() {};

        //data
        ExternalHeader<HoldingConfirmRequest> requestData = new ExternalHeader<>();
        requestData.setData(holdingConfirmRequest);
        ExternalHeader<HoldingConfirmResponse> response = null;
        try {
            response = restTemplateService.send(uri, HttpMethod.POST,requestData,type);
            log.info("[HoldingConfirmService] responseCode = {}",response);
            return CommonHeader.getResponse(HOLDING_CONFIRM_NAME,HoldingConfirmResponseCode.OK,response.getData());
        } catch (Exception e) {
            return CommonHeader.getResponse(HOLDING_CONFIRM_NAME, HoldingConfirmResponseCode.ERROR,null);
        }
    }


}
