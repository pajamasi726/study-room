package com.study.api.service;

import com.study.common.dto.ReservationInfoRequest;
import com.study.common.dto.ReservationInfoResponse;
import com.study.common.dto.external.ExternalHeader;
import com.study.common.entity.Biller;
import com.study.common.entity.Room;
import com.study.common.repository.RoomRepository;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class SearchRoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RestTemplateService restTemplateService;

    public ReservationInfoResponse searchRoom(ReservationInfoRequest request) {
        Optional<Room> optionalRoom = roomRepository.findByRoomCode(request.getRoomCode());
        if (optionalRoom.isPresent()) {
            Biller biller = optionalRoom.get().getCafe().getBiller();

            //uri
            URI uri = UriComponentsBuilder.fromHttpUrl(biller.getIpAddress())
                    .path("/platform/api/searchRoom")
                    .build()
                    .toUri();
            //타입
            ParameterizedTypeReference<ExternalHeader<ReservationInfoResponse>> type = new ParameterizedTypeReference<ExternalHeader<ReservationInfoResponse>>() {
            };

            //data -> biller로 보낼때
            ExternalHeader<ReservationInfoRequest> requestData = new ExternalHeader<>();
            requestData.setData(request);

            ExternalHeader<ReservationInfoResponse> header = null;
            try {
                header = restTemplateService.send(uri, HttpMethod.POST, requestData, type);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return header.getData();
        }
        return null;
    }

}
