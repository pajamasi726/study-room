package com.study.api.service.external;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.register.RegisterRequest;
import com.study.common.dto.external.register.RegisterResponse;
import com.study.api.process.ExternalRegisterProcess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RegisterService {

    @Autowired
    private ExternalRegisterProcess externalRegisterProcess;

    public ExternalHeader<RegisterResponse> register(ExternalHeader<RegisterRequest> registerRequest){
        //validation , process, register
        return externalRegisterProcess.register(registerRequest);
    }
}
