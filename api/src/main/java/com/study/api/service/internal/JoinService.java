package com.study.api.service.internal;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.register.JoinRequest;
import com.study.common.entity.User;
import com.study.common.enumClass.role.UserRole;
import com.study.common.enumClass.status.JoinStatus;
import com.study.common.enumClass.status.UserStatus;
import com.study.common.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class JoinService {

    @Autowired
    private UserRepository userRepository;

    public CommonHeader getJoinCheck(CommonHeader<JoinRequest> joinRequest) {
        CommonHeader header = new CommonHeader();
        JoinRequest data = joinRequest.getData();
        if(data.getUserId() != null && data.getUserId().length() >= 2){
            header.setData(data.getUserId());
        }
        if(!checkUserId(data.getUserId())){
           return header.setResponseCode(JoinStatus.DUPLICATE.getDisplayName());
        }
        if(data.getUserPassword() != null && data.getUserPassword().length() >= 6){
            header.setData(data.getUserPassword());
        }
        if(data.getEmail() != null){
            header.setData(data.getEmail());
        }
        if(data.getPhoneNumber() != null){
            header.setData(data.getPhoneNumber());
        }
        User user = new User().setUserId(data.getUserId())
                .setUserPassword(data.getUserPassword())
                .setEmail(data.getEmail())
                .setPhoneNumber(data.getPhoneNumber())
                .setStatus(UserStatus.REGISTERED)
                .setRole(UserRole.ROLE_MEMBER)
                ;
        userRepository.save(user);
        return header.setResponseCode("OK");
    }


    private boolean checkUserId(String userId){
        Optional<User> response = userRepository.findByUserId(userId);
        //이미 가입된 회원
        if(response.isPresent()){
            return false;
        }
        return true;
    }
}
