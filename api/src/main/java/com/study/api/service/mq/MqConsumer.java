package com.study.api.service.mq;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.study.api.service.HoldingService;
import com.study.common.dto.external.holding.HoldingFrontRequest;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class MqConsumer {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    TypeReference<CommonHeader<HoldingFrontRequest>> type = new TypeReference<CommonHeader<HoldingFrontRequest>>(){};

    @Autowired
    private HoldingService holdingService;

    @RabbitListener(queues = "${study.mq.name}")
    public void consumer(String message) throws IOException {
        log.info("message = {}",message);
        CommonHeader msg = objectMapper.readValue(message,CommonHeader.class);
        String api = msg.getApiName();
        switch (api){
            case "holding":
                CommonHeader<HoldingFrontRequest> request = objectMapper.readValue(message,type);
                log.info("request ={}",request);
                CommonHeader<InternalHoldingResponse> response = holdingService.holding(request.getData());
                log.info("response = {}",response);
        }

        log.info("message = {}");
    }
}
