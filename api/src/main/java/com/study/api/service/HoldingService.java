package com.study.api.service;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.holding.HoldingFrontRequest;
import com.study.common.dto.external.holding.HoldingRequest;
import com.study.common.dto.external.holding.HoldingResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;
import com.study.common.entity.*;
import com.study.common.enumClass.HoldingErrorCode;
import com.study.common.enumClass.ReservationConfirmStatus;
import com.study.common.repository.BillerRepository;
import com.study.common.repository.CafeRepository;
import com.study.common.repository.RoomRepository;
import com.study.common.repository.UserRepository;
import com.study.common.service.*;
import com.study.common.util.HeaderUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Slf4j
@Service
public class HoldingService {

    public final static String HOLDING_API_NAME = "holding";
    public final static String HOLDING_API_PATH = "/platform/api/reservation/holding";

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private ReservationConfirmService reservationConfirmService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private ReservationHistoryService reservationHistoryService;



    /********************************받은 요청을 biller에게 쏴주는 부분*******************************************/
    @Transactional
    public CommonHeader<InternalHoldingResponse> holding(HoldingFrontRequest request) {
        Optional<Room> optionalRoom = roomRepository.findByRoomCode(request.getRoomCode());
        if(optionalRoom.isPresent()){
            Room room = optionalRoom.get();
            Cafe cafe = room.getCafe();
            Biller biller = cafe.getBiller();

            // 예약 시간 데이터
            LocalDateTime startAt = LocalDateTime.parse(request.getStartAt(),DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            LocalDateTime endAt = LocalDateTime.parse(request.getEndAt(),DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            Optional<User> user = userRepository.findByUserId(request.getUserId());
            if(!user.isPresent())
                return CommonHeader.getResponse(HOLDING_API_NAME,HoldingErrorCode.NOT_FOUND,null);
            // 예약 대기 데이터 쌓기
            ReservationConfirm reservationConfirm  = reservationConfirmService.insertWaiting(cafe,room,user.get().getId(),startAt,endAt,ReservationConfirmStatus.WAITING);
            log.info("[HoldingService] reservationConfirm id: {}",reservationConfirm.getId());

            // 예약 대기 history 쌓기
            reservationHistoryService.save(reservationConfirm);

            InternalHoldingRequest holdingRequest = new InternalHoldingRequest();
            holdingRequest.setStartDateTime(request.getStartAt());
            holdingRequest.setEndDateTime(request.getEndAt());
            holdingRequest.setBillerCode(biller.getBillerCode());
            holdingRequest.setCafeCode(cafe.getCafeCode());
            holdingRequest.setRoomCode(room.getRoomCode());
            holdingRequest.setPersonCount(request.getPersonCount());
            holdingRequest.setUserId(user.get().getId());
            // 요청 데이터 생성
            ExternalHeader<HoldingRequest> requestData = makeHoldingRequest(holdingRequest,reservationConfirm);
            // API 요청
            ExternalHeader<HoldingResponse> holdingResponse = sendHoldingApi(biller, requestData);

            // 빌러의 responseCode 검증 및 후속 처리
            return validation(holdingResponse,reservationConfirm).orElseGet(()->process(holdingResponse,reservationConfirm));
        }else{
            // error 처리 데이터가 잘못 되었을때
            return CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.NOT_FOUND.getResponseCode(),HoldingErrorCode.NOT_FOUND.getMessage(),new InternalHoldingResponse());
        }
    }

    /** holding api request */
    private ExternalHeader<HoldingResponse> sendHoldingApi(Biller biller, ExternalHeader<HoldingRequest> request) {
        // uri
        URI uri = UriComponentsBuilder.fromHttpUrl(biller.getIpAddress())
                .path(HOLDING_API_PATH)
                .build()
                .encode()
                .toUri();

        // 리턴 타입 지정
        ParameterizedTypeReference<ExternalHeader<HoldingResponse>> type = new ParameterizedTypeReference<ExternalHeader<HoldingResponse>>() {};

        try {
            ExternalHeader<HoldingResponse> holdingResponse = restTemplateService.send(uri, HttpMethod.POST, request, type);
            log.info("[HoldingService] BILLER Holding Response : {}",holdingResponse);

            return holdingResponse;

        } catch (Exception e) {
            log.error("네트워크 에러 : {}",e.getLocalizedMessage());
            // todo 유빈이 숙제 타임아웃일때 어떻게 해야 할까요??
            return HeaderUtil.getHeader(HOLDING_API_NAME, HoldingErrorCode.ERROR.getResponseCode(),HoldingErrorCode.ERROR.getMessage());
        }
    }

    /** Holding Request 데이터 생성 */
    private ExternalHeader<HoldingRequest> makeHoldingRequest(InternalHoldingRequest request, ReservationConfirm reservationConfirm) {
        ExternalHeader<HoldingRequest> header = HeaderUtil.getHeader(HOLDING_API_NAME,"","");

        HoldingRequest body = new HoldingRequest();
        body.setBillerCode(request.getBillerCode())
                .setCafeCode(request.getCafeCode())
                .setRoomCode(request.getRoomCode())
                .setStartDateTime(request.getStartDateTime())
                .setEndDateTime(request.getEndDateTime())
                .setPersonCount(request.getPersonCount())
                .setReservationCode(reservationConfirm.getId().toString());
        header.setData(body);
        return header;
    }

    /*****************************biller에게서 받은 요청을 처리해서 front쪽으로 쏨********************************************/
    public Optional<CommonHeader<InternalHoldingResponse>> validation(ExternalHeader<HoldingResponse> billerResponse, ReservationConfirm reservationConfirm) {
        if (Strings.isEmpty(billerResponse.getResponseCode())) {
            log.info("[HoldingService] responseCode is null");
            CommonHeader commonHeader = CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.ERROR, new InternalHoldingResponse().setResult(false));
            return Optional.ofNullable(commonHeader);
        }

        // 빌러의 응답이 OK가 아닐때
        if("OK".equals(billerResponse.getResponseCode())==false){
            // 예약 불가로 변경
            reservationConfirm.setStatus(ReservationConfirmStatus.IMPOSSIBLE);
            reservationConfirmService.save(reservationConfirm);

            // 히스토리 쌓기
            reservationHistoryService.save(reservationConfirm);

            CommonHeader commonHeader = CommonHeader.getResponse(HOLDING_API_NAME, billerResponse.getResponseCode(), billerResponse.getMessage() , new InternalHoldingResponse().setResult(false));
            log.info("[HoldingService] 예약 불가");
            return Optional.ofNullable(commonHeader);
        }


        if (billerResponse.getData() == null) { //외부적인(전체적인 데이터)가 있는지 없는지 확인하는 부분
            log.info("[HoldingService] data is null");
            CommonHeader commonHeader = CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.ERROR, new InternalHoldingResponse().setResult(false));
            return Optional.ofNullable(commonHeader);
        }

        HoldingResponse response = billerResponse.getData(); // ExternalHeader의 data부분

        if (response.getBillerCode() == null || response.getCafeCode() == null || response.getRoomCode() == null) {
            log.info("[HoldingService] billerCode is null");
            CommonHeader commonHeader = CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.ERROR, new InternalHoldingResponse().setResult(false));
            return Optional.ofNullable(commonHeader);
        }
        if (response.getStartDateTime() == null || response.getEndDateTime() == null) {
            log.info("[HoldingService] startDateTime is null");
            CommonHeader commonHeader = CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.ERROR, new InternalHoldingResponse().setResult(false));
            return Optional.ofNullable(commonHeader);
        }

        return Optional.empty();
    }


    //예약이 제대로 되었는지 디비와 비교, 확인하는 부분
    public CommonHeader<InternalHoldingResponse> process(ExternalHeader<HoldingResponse> billerResponse, ReservationConfirm reservationConfirm) {
        HoldingResponse response = billerResponse.getData();
        //reservation_code도 있는지 없는지 확인
        return reservationConfirmService
                .findById(Long.parseLong(response.getReservationCode()))
                .map(confirm -> {
                    // 정상적인 경우
                    confirm.setAmount(response.getAmount());
                    reservationConfirmService.save(confirm);
                    return CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.OK, new InternalHoldingResponse().setResult(true));
                })
                .orElseGet(()->{
                    // 비정상적인경우
                    reservationConfirm.setStatus(ReservationConfirmStatus.CANCEL);
                    reservationConfirmService.save(reservationConfirm);

                    // 히스토리 쌓기
                    reservationHistoryService.save(reservationConfirm);
                    return CommonHeader.getResponse(HOLDING_API_NAME, HoldingErrorCode.ERROR, new InternalHoldingResponse().setResult(false));
                });
    }
}
