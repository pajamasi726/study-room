package com.study.api.service.external;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.holding.HoldingCancelRequest;
import com.study.common.dto.external.holding.HoldingCancelResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingCancelRequest;
import com.study.common.entity.Cafe;
import com.study.common.entity.ReservationConfirm;
import com.study.common.enumClass.HoldingCancelErrorCode;
import com.study.common.enumClass.ReservationConfirmStatus;
import com.study.common.service.ReservationConfirmService;
import com.study.common.service.ReservationHistoryService;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class HoldingCancelService {

    private String HOLDING_CANCEL_NAME = "HoldingCancel";
    private String HOLDING_CANCEL_PATH = "/platform/api/reservation/cancel";

    @Autowired
    private ReservationConfirmService reservationConfirmService;

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private ReservationHistoryService reservationHistoryService;


    /**
     * Biller에게 요청 하는 부분
     **/
    protected ExternalHeader<HoldingCancelResponse> exchange(CommonHeader<InternalHoldingCancelRequest> request, String ipAddress) {
        //Internal ->external용으로 만들고
        InternalHoldingCancelRequest data = request.getData();

        ExternalHeader<HoldingCancelRequest> externalCancelRequest = makeRequest(data);

        return sendCancelApi(externalCancelRequest, ipAddress);


        //요청 데이터 가공
    }

    /**
     * 요청한 데이터에 대한 처리였는지 비교 확인(reservation_cde , biller_code, cafe_code, room_code)
     **/
    protected Optional<CommonHeader<HoldingCancelResponse>> validate(
            CommonHeader<InternalHoldingCancelRequest> request,
            ExternalHeader<HoldingCancelResponse> response) {
        /* Response Code 확인해서 실패에 대한 케이스면 그냥 내려주기. */
        log.info("[HoldingCancelService] responseCode = {}", response);
        log.info("[HoldingCancelService] responseCode's data = {}", response.getData());

        if (Strings.isEmpty(response.getResponseCode())) {
            return Optional.ofNullable(CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null));
        }

        if (!HoldingCancelErrorCode.OK.name().equals(response.getResponseCode())) {
            return Optional.ofNullable(externalHeaderToCommonHeader(response));
        }

        HoldingCancelResponse holdingCancelResponse = response.getData();
        InternalHoldingCancelRequest holdingCancelRequest = request.getData();
        log.info("[HoldingCancelService] responseCode data = {}", holdingCancelResponse);

        if (!response.getData().getReservationCode().equals(request.getData().getReservationCode())) {
            log.info("[HoldingCancelService] reservation code responseCode={},request={}", response.getData().getReservationCode(), request.getData().getReservationCode());
            return Optional.ofNullable(CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null));
        }
        if (!holdingCancelRequest.getBillerCode().equals(holdingCancelResponse.getBillerCode())) {
            log.info("[HoldingCancelService] biller code responseCode={},request={}", holdingCancelResponse.getBillerCode(), holdingCancelRequest.getBillerCode());
            return Optional.ofNullable(CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null));
        }
        if (!holdingCancelRequest.getCafeCode().equals(holdingCancelResponse.getCafeCode())) {
            log.info("[HoldingcancelService] cafe code responseCode={},request={}", holdingCancelResponse.getCafeCode(), holdingCancelRequest.getCafeCode());
            return Optional.ofNullable(CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null));
        }
        if (!holdingCancelRequest.getRoomCode().equals(holdingCancelResponse.getRoomCode())) {
            log.info("[HoldingCancelService] room code request={},responseCode={}", holdingCancelRequest.getReservationCode(), holdingCancelResponse.getRoomCode());
            return Optional.ofNullable(CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null));
        }

        return Optional.empty();
    }

    @Transactional
    public CommonHeader<HoldingCancelResponse> cancel(CommonHeader<InternalHoldingCancelRequest> request) {

        /************************************** 요청 가능한 데이터인지 확인  [Request 확인]********************************************/
        InternalHoldingCancelRequest data = request.getData();

        //예약 번호인지 확인
        Optional<ReservationConfirm> optionalReservationConfirm = reservationConfirmService.findById(Long.parseLong(data.getReservationCode()));

        if (!optionalReservationConfirm.isPresent()) {
            log.info("[HoldingCancelService] ReservationConfirm not found");
            return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.NOT_FOUND_CONFIRM, null);
        }
        ReservationConfirm confirm = optionalReservationConfirm.get();

        if (!confirm.getCafe().getCafeCode().equals(data.getCafeCode())) {
            return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.NOT_FOUND_CONFIRM, null);
        }
        if (!confirm.getRoom().getRoomCode().equals(data.getRoomCode())) {
            return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.NOT_FOUND_CONFIRM, null);
        }
        //상태 변경 가능한지 확인
        if (!confirm.getStatus().equals(ReservationConfirmStatus.WAITING)) {
            log.info("[HoldingCancelService] status is not waiting");
            return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.NOT_CHANGE, null);
        }

        /************************************** 취소 가능한 예약 건인지 확인************************************/
        Cafe cafe = confirm.getCafe();

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime reservationTime = confirm.getStartAt();

        //취소 가능한 시간 확인 ( 내부적으로는 10분 이내에는 ) 취소 불가
        LocalDateTime cancelImpossibleLimit = reservationTime.minusMinutes(cafe.getCancelImpossible() == null ? (60 * 24) : cafe.getCancelImpossible());
        if (now.isAfter(cancelImpossibleLimit)) {
            // 취소 불가능
            return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.NOT_CHANGE, null);
        }

        //Biller에게 받은 응답
        String ipAddress = cafe.getBiller().getIpAddress();  //ip address
        ExternalHeader<HoldingCancelResponse> response = exchange(request, ipAddress);

        return validate(request, response).orElseGet(() -> process(response));
    }

    /************************************** 유료/ 무료 확인 ********************************************/
    protected CommonHeader<HoldingCancelResponse> process(ExternalHeader<HoldingCancelResponse> response) {
        HoldingCancelResponse data = response.getData();

        Optional<ReservationConfirm> optionalReservationConfirm = reservationConfirmService.findById(Long.parseLong(data.getReservationCode()));
        ReservationConfirm reservationConfirm = optionalReservationConfirm.get();

        Cafe cafe = reservationConfirm.getCafe();

        LocalDateTime reservationTime = reservationConfirm.getStartAt();
        log.info("[HoldingCancelService - process] 예약 시작 시간 = {} ", reservationTime);
        LocalDateTime now = LocalDateTime.now();
        log.info("[HoldingCancelService = process] 현재 시간 = {}", now);

        // 빌러 측 제한 todo 무료/ 유료 취소 값 responseCode -> 무료 일때 내부 정책 확인해서 유료 잡을건지 결정
        LocalDateTime billerSideLimit = reservationTime.minusHours(cafe.getCancelInterval() == null ? (24 * 365) : cafe.getCancelInterval());

        log.info("[HoldingCancelService - process] 빌러 측 제한 시간 = {}", billerSideLimit);
        // 플랫폼 측 제한
        LocalDateTime platformSideLimit = billerSideLimit.minusHours(2);
        log.info("[HoldingCancelService - process] 플랫폼 측 제한 시간 = {}", platformSideLimit);


        if (now.isBefore(platformSideLimit)) {
            // now < platform cancel time [무료/무료]
            reservationConfirm.setStatus(ReservationConfirmStatus.CANCEL);
        } else if (now.isBefore(billerSideLimit)) {
            // platform cancel time < now < biller cancel time [유료/무료]
            reservationConfirm.setStatus(ReservationConfirmStatus.PLATFORM_PAY_CANCEL);
        } else {
            // biller cancel time < now [유료/유료]
            reservationConfirm.setStatus(ReservationConfirmStatus.PAY_CANCEL);
        }
        //상태값 변경 후 저장.
        reservationConfirmService.save(reservationConfirm);
        //히스토리 저장
        reservationHistoryService.save(reservationConfirm);

        return CommonHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.OK, data);
    }

    private ExternalHeader<HoldingCancelResponse> sendCancelApi(ExternalHeader<HoldingCancelRequest> externalCancelRequest, String ipAddress) {
        //restTemplate 이용해서 던지고 받는다
        URI uri = UriComponentsBuilder.newInstance()
                .fromHttpUrl(ipAddress)
                .path(HOLDING_CANCEL_PATH)
                .build()
                .encode()
                .toUri();


        ParameterizedTypeReference<ExternalHeader<HoldingCancelResponse>> type = new ParameterizedTypeReference<ExternalHeader<HoldingCancelResponse>>() {
        };

        ExternalHeader<HoldingCancelResponse> response = null;
        try {
            response = restTemplateService.send(uri, HttpMethod.POST, externalCancelRequest, type);
        } catch (Exception e) {
            response = ExternalHeader.getResponse(HOLDING_CANCEL_NAME, HoldingCancelErrorCode.ERROR, null);
            e.printStackTrace();
        }
        return response;
    }

    public ExternalHeader<HoldingCancelRequest> makeRequest(InternalHoldingCancelRequest internalHoldingCancelRequest) {
        HoldingCancelRequest cancelRequest = new HoldingCancelRequest()
                .setBillerCode(internalHoldingCancelRequest.getBillerCode())
                .setCafeCode(internalHoldingCancelRequest.getCafeCode())
                .setRoomCode(internalHoldingCancelRequest.getRoomCode())
                .setEndDateTime(internalHoldingCancelRequest.getEndDateTime())
                .setStartDateTime(internalHoldingCancelRequest.getStartDateTime())
                .setReservationCode(internalHoldingCancelRequest.getReservationCode());

        ExternalHeader<HoldingCancelRequest> externalCancelRequest = new ExternalHeader<>();
        externalCancelRequest.setData(cancelRequest);
        externalCancelRequest.setApiName(HOLDING_CANCEL_NAME);
        log.info("cancelRequest : {}", cancelRequest);
        return externalCancelRequest;
    }

    public CommonHeader externalHeaderToCommonHeader(ExternalHeader externalHeader) {
        return new CommonHeader().
                setApiName(externalHeader.getApiName())
                .setResponseCode(externalHeader.getResponseCode())
                .setMessage(externalHeader.getMessage())
                .setTransDateTime(externalHeader.getTransDateTime())
                ;
    }
}
