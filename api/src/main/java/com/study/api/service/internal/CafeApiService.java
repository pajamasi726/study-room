package com.study.api.service.internal;

import com.study.common.dto.external.kakaoMap.KakaoMapResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeDetailsRequest;
import com.study.common.dto.internal.index.CafeDetailsResponse;
import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.entity.Cafe;
import com.study.common.enumClass.CafeStatus;
import com.study.common.repository.CafeRepository;
import com.study.common.service.RestTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.action.internal.CollectionRecreateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CafeApiService {

    private String KAKAO_MAP_URL = "https://dapi.kakao.com";
    private String KAKAO_MAP_PATH = "/v2/local/search/address.json";
    private String APP_KEY = "KakaoAK 3f6eae89fdb30f7825ab06250af1959e";

    @Autowired
    private CafeRepository cafeRepository;
    @Autowired
    private RestTemplateService restTemplateService;

    public CafeDetailsResponse cafeList(Long id) {

        Optional<Cafe> response = cafeRepository.findById(id);
        if(response.isPresent()) {
            Cafe cafe = response.get();

            if(Strings.isEmpty(cafe.getLocationX()) && Strings.isEmpty(cafe.getLocationY())) {
                sendKakaoMap(cafe);
                cafeRepository.save(cafe);
            }

            CafeDetailsResponse cafeDetailsResponse = new CafeDetailsResponse()
                    .setId(cafe.getId())
                    .setCafeName(cafe.getBiller().getName()+" "+cafe.getCafeName())
                    .setCategory(cafe.getCategory())
                    .setDescription(cafe.getDescription())
                    .setImgUrl01(cafe.getImgUrl01())
                    .setImgUrl02(cafe.getImgUrl02())
                    .setImgUrl03(cafe.getImgUrl03())
                    .setWebSite(cafe.getWebsite())
                    .setHeart(cafe.getHeart())
                    .setLocationX(cafe.getLocationX())
                    .setLocationY(cafe.getLocationY());
            return  cafeDetailsResponse;

        }else{
            return null;
        }
    }

    public CafeListResponse searchCafeList(String keyword){
        List<Cafe> originCafe = cafeRepository.findByDistrictContaining(keyword);

        List<CafeListResponse.ChagedCafe> list = originCafe.stream().map(cafe -> {
            CafeListResponse.ChagedCafe chagedCafe = new CafeListResponse.ChagedCafe();
            chagedCafe.setId(cafe.getId());
            chagedCafe.setCafeName(cafe.getCafeName());
            chagedCafe.setCategory(cafe.getCategory());
            chagedCafe.setTitle(cafe.getSummary());
            chagedCafe.setDescription(cafe.getDescription());
            chagedCafe.setImgUrl01(cafe.getImgUrl01());
            chagedCafe.setWebSite(cafe.getWebsite());
            return chagedCafe;
        }).collect(Collectors.toList());

        CafeListResponse response = new CafeListResponse();
        response.setCafeList(list);

        return response;
    }

    public void sendKakaoMap(Cafe cafe){
        URI uri = UriComponentsBuilder
                .fromHttpUrl(KAKAO_MAP_URL)
                .path(KAKAO_MAP_PATH)
                .queryParam("query",cafe.getAddress())
                .build()
                .toUri();

        ParameterizedTypeReference<KakaoMapResponse> type = new ParameterizedTypeReference<KakaoMapResponse>() {};

        try {
            KakaoMapResponse response = restTemplateService.send(uri, HttpMethod.GET,"Authorization",APP_KEY,null,type);
            log.info("responseCode={}",response);
            if(response.getAddress().size() > 0){
                cafe.setLocationX(response.getAddress().get(0).getX());
                cafe.setLocationY(response.getAddress().get(0).getY());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
