package com.study.api.service.internal;

import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.dto.internal.index.topList.TopListResponse;
import com.study.common.dto.internal.search.CafeSearchRequest;
import com.study.common.entity.Cafe;
import com.study.common.enumClass.CafeStatus;
import com.study.common.repository.CafeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class IndexApiService {

    @Autowired
    private CafeRepository cafeRepository;

    public CafeListResponse search(CafeSearchRequest data) {
        List<Cafe> cafeList = null;

        switch (data.getSearchType()) {
            case "district":
                log.info("district search keyword : {}", data.getWord());
                cafeList = cafeRepository.findByDistrictContaining(data.getWord());
                break;
            case "name":
                log.info("name search keyword : {}", data.getWord());
                cafeList = cafeRepository.findByCafeNameContaining(data.getWord());
                break;
            case "city":
                log.info("city search keyword : {}", data.getWord());
                cafeList = cafeRepository.findByCityContaining(data.getWord());
                break;
            default:
                cafeList = new ArrayList<>();
                break;
        }
        CafeListResponse responseData = new CafeListResponse();
        responseData.setCafeList(cafeList.stream()
                .filter(cafe -> cafe.getCafeStatus().equals(CafeStatus.REGISTERED))
                .map(cafe -> makeChangedCafe(cafe)).collect(Collectors.toList()));

        return responseData;
    }

    public CommonHeader<CafeListResponse> cafeList(Pageable pageable) {

        List<CafeListResponse.ChagedCafe> cafeList = cafeRepository.findAll(pageable).stream()
                .filter(cafe -> CafeStatus.REGISTERED.equals(cafe.getCafeStatus()))
                .map(cafe -> makeChangedCafe(cafe)).collect(Collectors.toList());

        cafeList.stream().forEach(c -> log.info("{}", c));

        CommonHeader<CafeListResponse> header = new CommonHeader<>();
        CafeListResponse body = new CafeListResponse();
        body.setCafeList(cafeList);
        header.setData(body);
        return header;
    }

    public TopListResponse topList() {
        List<Cafe> cafeList = cafeRepository.findTop4CafeList();
        log.info("cafeList size is {}", cafeList.size());

        if (cafeList.size() < 0)
            return null;

        List<TopListResponse.CafeList> topListResponses = cafeList.stream().map(cafe -> {
            TopListResponse.CafeList response = new TopListResponse.CafeList();
            response.setId(cafe.getId());
            response.setCategory("스터디룸");
            //responseCode.setAddress(cafe.getAddress());
            response.setCafeName(cafe.getBiller().getName() + " " + cafe.getCafeName());
            response.setDescription(cafe.getDescription());
            response.setImgUrl01(cafe.getImgUrl01());
            response.setLikeCount(cafe.getHeart());
            response.setWebSiteUrl(cafe.getWebsite());
            return response;
        }).collect(Collectors.toList());
        return new TopListResponse().setCafeList(topListResponses);
    }

    public CafeListResponse cafeListAll() {
        List<CafeListResponse.ChagedCafe> list = cafeRepository.findAll().stream()
                .filter(cafe -> cafe.getCafeStatus().equals(CafeStatus.REGISTERED))
                .map(cafe -> makeChangedCafe(cafe)).collect(Collectors.toList());
        log.info("[IndexApiService] cafeListAll() -> size={}", list.size());
        return new CafeListResponse().setCafeList(list);
    }

    private CafeListResponse.ChagedCafe makeChangedCafe(Cafe cafe) {
        CafeListResponse.ChagedCafe dto = new CafeListResponse.ChagedCafe();
        dto.setCafeName(cafe.getCafeName())
                .setId(cafe.getId())
                .setImgUrl01(cafe.getImgUrl01())
                .setWebSite(cafe.getWebsite())
                .setTitle(cafe.getDescription())
                .setHeart(cafe.getHeart())
        //.setDescription(cafe.getAddress())
        ;
        return dto;
    }

}
