package com.study.api.service.factory;

import com.study.api.model.CommonStudy;
import com.study.api.model.GMStudy;
import com.study.api.model.JStudy;
import com.study.api.model.STStudy;
import com.study.api.model.ifs.ApiInterface;
import com.study.common.entity.Biller;
import com.study.common.repository.BillerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ApiFactory {

    @Autowired
    private BillerRepository billerRepository;

    @Autowired
    private GMStudy gmStudy;

    @Autowired
    private JStudy jStudy;

    @Autowired
    private CommonStudy commonStudy;

    @Autowired
    private STStudy stStudy;

    public ApiInterface getApiModel(Long billerId){
        // todo id 검증
        Optional<Biller> optionalBiller = billerRepository.findById(billerId);

        return optionalBiller
                .map(biller -> {
                    switch (biller.getName()){

                        case "강남스터디" :
                            return gmStudy;

                        case "종로스터디" :
                            return jStudy;

                        case "STStudy" :
                            return stStudy;

                        default:
                            return commonStudy; // 기본형
                    }
                })
                .orElse(commonStudy);
    }
}
