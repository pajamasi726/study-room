package com.study.api.controller.externalApi.holding;

import com.study.api.service.external.holdingConfirm.HoldingConfirmService;
import com.study.common.dto.external.holdingConfirm.HoldingConfirmRequest;
import com.study.common.dto.external.holdingConfirm.HoldingConfirmResponse;
import com.study.common.dto.internal.CommonHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class HoldingConfirmApiController {

    @Autowired
    private HoldingConfirmService holdingConfirmService;

    @RequestMapping(value = "/confirm",method = RequestMethod.POST)
    public CommonHeader<HoldingConfirmResponse> cancel(@RequestBody CommonHeader<HoldingConfirmRequest> request){
        log.info("[ConfirmApiController] cancel : {}",request);
        return holdingConfirmService.holdingConfirm(request);
    }
}
