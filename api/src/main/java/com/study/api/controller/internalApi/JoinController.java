package com.study.api.controller.internalApi;

import com.study.api.service.internal.JoinService;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.register.JoinRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/regist")
public class JoinController {

    @Autowired
    private JoinService joinService;

    @RequestMapping(value = "/registerForm", method = RequestMethod.POST)
    public CommonHeader join(@RequestBody CommonHeader<JoinRequest> joinRequest){
        log.info("JoinController : {}", joinRequest);
        return joinService.getJoinCheck(joinRequest);
    }

}
