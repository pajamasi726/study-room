package com.study.api.controller;

import com.study.api.service.SearchService;
import com.study.common.dto.ReservationSearchRequest;
import com.study.common.dto.ReservationSearchResponse;
import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.search.SearchRequest;
import com.study.common.dto.external.search.SearchResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/reservation")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ExternalHeader<SearchResponse> search(ExternalHeader<SearchRequest> request){
        //TODO 나머지 유빈 정보 채우기
        // BILLER CODE, ZIP CODE


        log.info("[API] Search={}",request);
        return searchService.search(request);
    }
}
