package com.study.api.controller.internalApi;

import com.study.api.service.internal.CafeApiService;
import com.study.api.service.internal.RoomApiService;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeDetailsRequest;
import com.study.common.dto.internal.index.CafeDetailsResponse;
import com.study.common.dto.internal.index.CafeDetailsRoomResponse;
import com.study.common.dto.internal.index.CafeListResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/cafe")
public class CafeApiController {

    @Autowired
    private CafeApiService cafeApiService;

    @Autowired
    private RoomApiService roomApiService;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CafeDetailsResponse cafeList(@RequestParam Long id){
        return cafeApiService.cafeList(id);
    }

    @RequestMapping(value = "/roomList", method = RequestMethod.GET)
    public CafeDetailsRoomResponse roomList(@RequestParam Long id){
        return roomApiService.roomList(id);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public CafeListResponse searchCafeList(@RequestParam String keyword){
        return cafeApiService.searchCafeList(keyword);
    }
}
