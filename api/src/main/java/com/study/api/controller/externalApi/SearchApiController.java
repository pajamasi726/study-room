package com.study.api.controller.externalApi;

import com.study.api.service.factory.ApiFactory;
import com.study.common.dto.external.search.SearchResponse;
import com.study.common.dto.internal.CommonHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class SearchApiController {

    @Autowired
    private ApiFactory apiFactory;

    @RequestMapping("/search")
    public CommonHeader<SearchResponse> search(Long billerId){
        return apiFactory.getApiModel(billerId).search();
    }
}
