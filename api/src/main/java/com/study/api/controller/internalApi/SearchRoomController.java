package com.study.api.controller.internalApi;

import com.study.api.service.SearchRoomService;
import com.study.common.dto.ReservationInfoRequest;
import com.study.common.dto.ReservationInfoResponse;
import com.study.common.dto.external.ExternalHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/platform/api")
public class SearchRoomController {

    @Autowired
    private SearchRoomService searchRoomService;

    @RequestMapping(value = "/searchRoom",method = RequestMethod.GET)
    public ReservationInfoResponse searchRoom(ReservationInfoRequest request){
        log.info("controller request={}",request);
        return searchRoomService.searchRoom(request);
    }
}
