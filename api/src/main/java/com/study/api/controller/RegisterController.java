package com.study.api.controller;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.register.RegisterRequest;
import com.study.common.dto.external.register.RegisterResponse;
import com.study.api.service.external.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/platform/reservation")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @RequestMapping(value = "register",method = RequestMethod.POST)
    public ExternalHeader<RegisterResponse> register(@RequestBody ExternalHeader<RegisterRequest> registerRequest){
        log.info("[RegisterController] request is : {}",registerRequest);
        return registerService.register(registerRequest);
    }
}
