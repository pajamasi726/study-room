package com.study.api.controller.internalApi;

import com.study.api.service.internal.IndexApiService;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.index.CafeListResponse;
import com.study.common.dto.internal.index.topList.TopListResponse;
import com.study.common.dto.internal.search.CafeSearchRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/api/index")
public class IndexApiController {

    @Autowired
    private IndexApiService indexApiService;

    @RequestMapping(value = "/cafeList",method = RequestMethod.GET)
    public CommonHeader<CafeListResponse> cafeList( @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 9) Pageable pageable) {
        log.info("pageable: {}",pageable.getOffset());
        log.info("pageable : {}",pageable.getPageSize());
        return indexApiService.cafeList(pageable);
    }
    @RequestMapping(value="/cafeListAll",method = RequestMethod.GET)
    public CafeListResponse cafeListAll(){
        return indexApiService.cafeListAll();
    }

    @RequestMapping(value= "/topList")
    public TopListResponse topList(){
        return indexApiService.topList();
    }

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public CafeListResponse search(CafeSearchRequest request){
        log.info(" search request : {}",request);
        return indexApiService.search(request);
    }
}
