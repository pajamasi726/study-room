package com.study.api.controller;

import com.study.api.service.HoldingService;
import com.study.common.dto.external.holding.HoldingFrontRequest;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingRequest;
import com.study.common.dto.internal.holding.InternalHoldingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class ReservationApiController {

    @Autowired
    private HoldingService holdingService;

    /** Front 로 부터 들어오는 예약 처리 Holding */
    @ResponseBody
    @RequestMapping("/holding")
    public InternalHoldingResponse holding(@RequestBody HoldingFrontRequest request){
        log.info("[ReservationApiController] holding : {}",request);
        return holdingService.holding(request).getData();
    }

}
