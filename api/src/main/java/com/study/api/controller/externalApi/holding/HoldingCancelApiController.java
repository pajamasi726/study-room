package com.study.api.controller.externalApi.holding;

import com.study.api.service.external.HoldingCancelService;
import com.study.common.dto.external.holding.HoldingCancelResponse;
import com.study.common.dto.internal.CommonHeader;
import com.study.common.dto.internal.holding.InternalHoldingCancelRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class HoldingCancelApiController {

    @Autowired
    private HoldingCancelService holdingCancelService;

    @RequestMapping(value = "/cancel",method = RequestMethod.POST)
    public CommonHeader<HoldingCancelResponse> cancel(@RequestBody CommonHeader<InternalHoldingCancelRequest> request){
        log.info("[HoldingApiController] cancel : {}",request);
        return holdingCancelService.cancel(request);
    }
}
