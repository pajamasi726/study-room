package com.study.api.model.ifs;

import com.study.common.dto.external.search.SearchResponse;
import com.study.common.dto.internal.CommonHeader;

public interface ApiInterface {

    CommonHeader<SearchResponse> search();
    CommonHeader confirm();
}
