package com.study.api.model;

import com.study.api.model.ifs.ApiInterface;
import com.study.api.model.ifs.SearchInterface;
import com.study.api.service.external.search.SearchPhoneNumberService;
import com.study.common.dto.external.search.SearchResponse;
import com.study.common.dto.internal.CommonHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class JStudy implements ApiInterface {

    private SearchInterface searchInterface;

    @Autowired
    private SearchPhoneNumberService searchPhoneNumberService;

    @PostConstruct
    public void init(){
        searchInterface = searchPhoneNumberService;
    }

    @Override
    public CommonHeader<SearchResponse> search() {
        return searchInterface.search();
    }

    @Override
    public CommonHeader confirm() {
        return null;
    }
}
