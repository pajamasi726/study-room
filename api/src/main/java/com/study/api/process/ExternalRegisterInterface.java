package com.study.api.process;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.register.RegisterRequest;
import com.study.common.dto.external.register.RegisterResponse;

public interface ExternalRegisterInterface {
    ExternalHeader<RegisterResponse> register(ExternalHeader<RegisterRequest> request);
}
