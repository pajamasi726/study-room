package com.study.api.process;

import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.register.RegisterRequest;
import com.study.common.dto.external.register.RegisterResponse;

import java.util.Optional;

public abstract class ExternalRegisterAbstract implements ExternalRegisterInterface{

    abstract protected Optional<ExternalHeader<RegisterResponse>> validation(ExternalHeader<RegisterRequest> registerRequest);
    abstract protected ExternalHeader<RegisterResponse> process(ExternalHeader<RegisterRequest> registerRequest);

}
