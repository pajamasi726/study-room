package com.study.api.process;


import com.study.api.util.RegisterRequestValidator;
import com.study.common.dto.external.ExternalHeader;
import com.study.common.dto.external.kakaoMap.KakaoMapResponse;
import com.study.common.dto.external.register.RegisterRequest;
import com.study.common.dto.external.register.RegisterResponse;
import com.study.common.enumClass.BillerStatus;
import com.study.common.enumClass.CafeStatus;
import com.study.common.enumClass.RegisterErrorCode;
import com.study.common.entity.Biller;
import com.study.common.entity.Cafe;
import com.study.common.entity.Room;
import com.study.common.service.BillerService;
import com.study.common.service.CafeService;
import com.study.common.service.RestTemplateService;
import com.study.common.service.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import javax.swing.text.html.Option;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ExternalRegisterProcess extends ExternalRegisterAbstract {

    @Autowired
    private BillerService billerService;
    @Autowired
    private CafeService cafeService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private RestTemplateService restTemplateService;

    private String API_NAME = "register";


    @Override
    protected Optional<ExternalHeader<RegisterResponse>> validation(ExternalHeader<RegisterRequest> request) {

        //data 유무 확인
        if (request.getData() == null) {
            log.info("[ExternalRegisterProcess] request data is null");
            return Optional.ofNullable(ExternalHeader.getResponse(API_NAME, RegisterErrorCode.INAPPROPRIATE, null));
        }

        //data 검사.
        RegisterRequest data = request.getData();
        log.info("[ExternalRegisterProcess] request's data is {}", request.getData());

        List<String> errors = new RegisterRequestValidator().validate(data);

        if (errors.size() > 0) {
            errors.stream().forEach(log::info);
            log.info("[ExternalRegisterProcess] data contain error");
            return Optional.ofNullable(ExternalHeader.getResponse(API_NAME, RegisterErrorCode.INAPPROPRIATE, null));
        }

        //검증 결과 이상이 없다는 것을 의미
        return Optional.empty();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ExternalHeader<RegisterResponse> register(ExternalHeader<RegisterRequest> request) {
        //valdation에서 리턴값이 없을 경우(=이상이 없을 경우) process 수행
        return validation(request).orElseGet(() -> process(request));

    }


    @Override
    protected ExternalHeader<RegisterResponse> process(ExternalHeader<RegisterRequest> request) {

        /********************데이터 가공*********************/
        RegisterRequest data = request.getData();

        //데이터 가공
        List<Cafe> cafeList = data.getCafeList().stream().map(biller_cafe -> {
            List<Room> roomList = biller_cafe.getRoomList().stream().map(billerRoom ->
                    new Room().setName(billerRoom.getRoomName())
                            .setMinSeating(billerRoom.getMinSeating())
                            .setMaxSeating(billerRoom.getMaxSeating())
                            .setPrice(billerRoom.getPrice())
                            .setImgUrl01(billerRoom.getImgUrl01())
                            .setImgUrl02(billerRoom.getImgUrl02())
                            .setImgUrl03(billerRoom.getImgUrl03())
                            .setDescription(billerRoom.getDescription())
            ).collect(Collectors.toList());
            return new Cafe()
                    .setCafeStatus(CafeStatus.PRE_REGISTERED)
                    .setCafeName(biller_cafe.getCafeName())
                    .setRoomList(roomList)
                    .setCity(biller_cafe.getCity())
                    .setDistrict(biller_cafe.getDistrict())
                    .setTown(biller_cafe.getTown())
                    .setCity(biller_cafe.getCity())
                    .setDescription(biller_cafe.getDescription())
                    .setWebsite(biller_cafe.getWebsite())
                    .setSummary(biller_cafe.getSummary())
                    .setOpenAt(biller_cafe.getOpenAt())
                    .setCloseAt(biller_cafe.getCloseAt())
                    .setPrecaution(biller_cafe.getPrecaution())
                    .setFacilities(biller_cafe.getFacilities())
                    ;


        }).collect(Collectors.toList());


        //cafeList.stream().forEach(cafe -> sendKakaoMap(cafe));

        /********************데이터 등록*********************/

        Biller biller = new Biller().setBillerStatus(BillerStatus.PRE_REGISTERED).setName(data.getBillerName()).setIpAddress(data.getIpAddress());
        biller = billerService.register(biller);

        for(Cafe cafe : cafeList){
            //카페 등록
            cafeService.register(cafe.setBiller(biller));
            for(Room room : cafe.getRoomList()){
                //룸 등록
                roomService.register(room.setCafe(cafe));
            }
        }
        /********************RESPONSE 가공*********************/
        RegisterResponse response = new RegisterResponse();
        response.setBillerCode(biller.getBillerCode());
        log.info("BillerService.register.responseCode => billerCode:{}", biller.getBillerCode());
        response.setCafeList(cafeList.stream().
                map(cafe ->
                        {
                            RegisterResponse.BillerCafe billerCafe = new RegisterResponse.BillerCafe();
                            billerCafe.setCafeCode(cafe.getCafeCode());
                            billerCafe.setCafeName(cafe.getCafeName());
                            billerCafe.setStudyRoom(cafe.getRoomList().stream().map(room ->
                                    {
                                        RegisterResponse.BillerRoom billerRoom = new RegisterResponse.BillerRoom();
                                        billerRoom.setRoomName(room.getName());
                                        billerRoom.setRoomCode(room.getRoomCode());
                                        return billerRoom;
                                    }
                            ).collect(Collectors.toList()));
                            return billerCafe;
                        }
                ).

                collect(Collectors.toList()));

        return ExternalHeader.getResponse(API_NAME, RegisterErrorCode.OK, response);

    }
}
