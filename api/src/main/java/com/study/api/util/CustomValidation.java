package com.study.api.util;

import com.study.common.dto.external.register.RegisterRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.validation.Validation;
import java.util.List;

@Component
public class CustomValidation implements Validator {

    private SpringValidatorAdapter validator;

    public CustomValidation() {
        this.validator = new SpringValidatorAdapter(Validation.buildDefaultValidatorFactory().getValidator());
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
//        RegisterRequest request = (RegisterRequest) target;
//        List<RegisterRequest.BillerRoom> billerRooms = request.getRoomList();
//        for (Object obj : billerRooms) {
//            validator.validate(obj, errors);
//        }
//
//        if(billerRooms.size()!=
//                billerRooms.stream().map(a->a.getRoomName().trim()).distinct().count()){
//            errors.reject("room_number","룸번호가 유니크하지 않습니다.");
//        }

    }
}
