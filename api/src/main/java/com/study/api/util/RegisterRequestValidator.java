package com.study.api.util;

import com.study.common.dto.external.register.RegisterRequest;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;


@Slf4j
public class RegisterRequestValidator {
    private boolean isNotBlank(String target){
        if(target==null || target.trim().isEmpty()){
            return false;
        }
        return true;
    }
    public List<String> validate(RegisterRequest data){
        List<String> errors = new ArrayList<>();

        if(!isNotBlank(data.getBillerName())){
            log.info("[RegisterRequestValidator] -> billerName :{}",data.getBillerName());
            errors.add("biller_name");
            return errors;
        }

        List<RegisterRequest.BillerCafe> cafeList = data.getCafeList();
        Long zipCount = cafeList.stream().map(a->a.getCafeName().trim()).distinct().count();
        if(cafeList.size()!=zipCount){
            log.info("[RegisterRequestValidator]->zip name is not unique");
            errors.add("zip_name");
            return errors;
        }

        //지점 및 방 검사
        for(RegisterRequest.BillerCafe cafe : cafeList){

            if(!isNotBlank(cafe.getCafeName())){
                log.info("[RegisterRequestValidator]->cafeName is null");
                errors.add("zip_name");
                return errors;
            }

            //방 이름이 유니크한지 검사
            List<RegisterRequest.BillerRoom> billerRooms = cafe.getRoomList();

            Long count = billerRooms.stream().map(a->a.getRoomName().trim()).distinct().count();

            if(billerRooms.size()!=count){
                log.info("[RegisterRequestValidator]->room name is not unique");
                errors.add("room_name");
                return errors;
            }
            //방 내용 검사
            billerRooms.stream().forEach(room->{
                if(!isNotBlank(room.getDescription())){
                    log.info("[RegisterRequestValidator] -> rooms.description :{}",room.getDescription());
                    errors.add("description");
                }
                if(!isNotBlank(room.getImgUrl01())){
                    log.info("[RegisterRequestValidator] -> rooms.img_url_01 :{}",room.getImgUrl01());
                    errors.add("img_url_01");
                }
                if(room.getPrice()<=0){
                    log.info("[RegisterRequestValidator] -> rooms.room_name :{}",room.getRoomName());
                    errors.add("price");
                }
                if(room.getMinSeating()<=0){
                    log.info("[RegisterRequestValidator] -> rooms.min_seating :{}",room.getMinSeating());
                    errors.add("min_seating");
                }
                if(room.getMaxSeating()<=0){
                    log.info("[RegisterRequestValidator] -> rooms.max_seating :{}",room.getMaxSeating());
                    errors.add("max_seating");
                }
            });
        }


        return errors;
    }
}
