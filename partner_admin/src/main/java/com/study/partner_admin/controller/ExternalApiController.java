package com.study.partner_admin.controller;

import com.study.common.entity.Biller;
import com.study.common.entity.BillerUser;
import com.study.common.repository.BillerRepository;
import com.study.common.repository.BillerUserRepository;
import com.study.partner_admin.service.PartnerBillerUserSerivce;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/external")
public class ExternalApiController {

    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    private BillerUserRepository billerUserRepository;

    @Autowired
    private BillerRepository billerRepository;

    @Autowired
    private PartnerBillerUserSerivce partnerBillerUserSerivce;

    @RequestMapping(value = "billerUserCreate",method = RequestMethod.GET)
    public String billerUserCreate(@RequestParam String userName, @RequestParam String password){
        // todo default 제거
        Optional<Biller> optionalBiller = billerRepository.findById(1L);

        if(optionalBiller.isPresent()){
            BillerUser billerUser = new BillerUser();
            billerUser.create(optionalBiller.get(),userName,partnerBillerUserSerivce.getEncodePassword(password));
            billerUserRepository.save(billerUser);
            return "OK";
        }
        return "FAIL";
    }

}
