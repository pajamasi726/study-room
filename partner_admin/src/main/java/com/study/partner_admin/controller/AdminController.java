package com.study.partner_admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/admin")
public class AdminController {
    @RequestMapping("/main")
    public ModelAndView main(){

        log.info("admin mai init");
        return new ModelAndView("/admin/main");
    }
}
