package com.study.partner_admin.controller;

import com.study.partner_admin.auth.BillerUserPricipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/internal")
public class InternalController {

    @RequestMapping("/welcome")
    public ModelAndView welcome(@AuthenticationPrincipal BillerUserPricipal billerUserPricipal){
        log.info("welcome init 로그인한 사용자 : {}",billerUserPricipal);
        return new ModelAndView("/internal/welcome");
    }
}
