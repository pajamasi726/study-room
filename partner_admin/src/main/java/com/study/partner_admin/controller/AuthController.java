package com.study.partner_admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/auth")
public class AuthController {

    @RequestMapping("/login")
    public ModelAndView login(){
        log.info("pages login init");
        return new ModelAndView("/service/login"); // resource/templates/service/login.html
    }

    @RequestMapping("/logout")
    public ModelAndView logout(){
        log.info("pages logout init");
        return new ModelAndView("/service/logout"); // resource/templates/service/login.html
    }
}
