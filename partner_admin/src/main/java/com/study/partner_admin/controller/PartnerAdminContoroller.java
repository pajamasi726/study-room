package com.study.partner_admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/partner")
public class PartnerAdminContoroller {

    //@Secured(value={"ROLE_PARTNER", "ROLE_SUPER"})
    @RequestMapping("/main")
    public ModelAndView main(){
        log.info("partner main init");
        return new ModelAndView("/partner/main");
    }
}

