package com.study.partner_admin.controller;

import com.study.partner_admin.auth.BillerUserPricipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/pages/error")
public class ErrorController {

    @RequestMapping("/reject")
    public ModelAndView reject(@AuthenticationPrincipal BillerUserPricipal billerUserPricipal){
        log.info("잘못접근:{}", billerUserPricipal);
        return new ModelAndView("/error/reject");
    }
}
