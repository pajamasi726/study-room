package com.study.partner_admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Partner_adminApplication {

    public static void main(String[] args) {
        SpringApplication.run(Partner_adminApplication.class,args);
    }
}
