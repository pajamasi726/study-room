package com.study.partner_admin.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PartnerBillerUserSerivce {

    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public String getEncodePassword(String password){
        return encoder.encode(password);
    }
}
