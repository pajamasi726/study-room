package com.study.partner_admin.config;

import com.study.common.enumClass.BillerUserRole;
import com.study.partner_admin.auth.LoginFailureHandler;
import com.study.partner_admin.auth.LoginSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity //(securedEnabled = true)

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    private LoginFailureHandler loginFailureHandler;

    @Autowired
    private CustomeAuthenticationProvide customeAuthenticationProvide;

    // http에 관한보안 설정
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.headers().frameOptions().sameOrigin();

        // filter
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/pages/external/**").permitAll()
                .antMatchers("/api/external/**").permitAll()
                .antMatchers("/pages/service/login").permitAll()         // login
                .antMatchers("/pages/admin/**").hasRole(BillerUserRole.ROLE_ADMIN.getRole())   // ROLE_ADMIN
                .antMatchers("/pages/partner/**").hasAnyRole(BillerUserRole.ROLE_PARTNER.getRole(), BillerUserRole.ROLE_SUPER.getRole())
                .anyRequest()       // 그외 모든 요청
                .authenticated();   // 로그인권한

        // login
        http.formLogin()
                .loginPage("/pages/service/login")               // login page html
                .loginProcessingUrl("/api/service/login")  // post login submit
                .usernameParameter("id")                // from parameter 의 id 부분
                .passwordParameter("pw")                // from parameter 의 pw 부분
                .successHandler(loginSuccessHandler)    // login success 후 할일
                .failureHandler(loginFailureHandler);   // login failure 후 할일

        // logout
        http.logout()
                .logoutUrl("/api/service/logout")                  // post logout submit
                .logoutSuccessUrl("/")    // 로그아웃시 이동할 페이지
                .invalidateHttpSession(true)    //세션 초기화
                ;

        http.exceptionHandling().accessDeniedPage("/pages/exception/reject");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {

        // 아래의 요청에 대해서는 spring security 제외
        web.ignoring()
                .antMatchers("/js/**")
                .antMatchers("/css/**")
                .antMatchers("/img/**")
                .antMatchers("/images/**")
                ;
    }

    // 계정 관련 보안설정
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
        .authenticationProvider(customeAuthenticationProvide);

    }
}
