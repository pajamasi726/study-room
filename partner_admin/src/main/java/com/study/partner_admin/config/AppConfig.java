package com.study.partner_admin.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.study.common","com.study.partner_admin"})
public class AppConfig {

}
