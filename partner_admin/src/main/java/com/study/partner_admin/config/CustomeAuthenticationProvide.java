package com.study.partner_admin.config;

import com.study.partner_admin.auth.BillerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CustomeAuthenticationProvide  extends DaoAuthenticationProvider {
    @Autowired
    private BillerUserDetailService billerUserDetailService;

    @PostConstruct
    public void init(){
        setHideUserNotFoundExceptions(false);
        setUserDetailsService(billerUserDetailService);
        setPasswordEncoder(new BCryptPasswordEncoder());

    }
}
