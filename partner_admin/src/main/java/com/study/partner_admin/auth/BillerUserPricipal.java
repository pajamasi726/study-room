package com.study.partner_admin.auth;

import com.study.common.enumClass.BillerUserRole;
import com.study.common.enumClass.BillerUserStatus;
import com.study.common.entity.BillerUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
public class BillerUserPricipal implements UserDetails {

    private String userName;
    private String password;
    private BillerUserRole role;
    private BillerUserStatus status;
    private int loginFailCount;
    private LocalDateTime passwordUpdatedAt;

    public static BillerUserPricipal of(BillerUser billerUser){
        BillerUserPricipal billerUserPricipal = new BillerUserPricipal();
        billerUserPricipal.setUserName(billerUser.getUserName());
        billerUserPricipal.setPassword(billerUser.getPassword());
        billerUserPricipal.setRole(billerUser.getRole());
        billerUserPricipal.setStatus(billerUser.getStatus());
        billerUserPricipal.setLoginFailCount(billerUser.getLoginFailCount());
        billerUserPricipal.setPasswordUpdatedAt(billerUser.getPasswordUpdatedAt());
        return billerUserPricipal;
    }

    // 계정의 권한 정보
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> roleSet = new HashSet<>();
        roleSet.add(new SimpleGrantedAuthority(role.name())); // ROLE_ PREFIX 포함 role
        return roleSet;
    }

    // 계정의 패스워드 정보
    @Override
    public String getPassword() {
        return this.password;
    }

    // 계정의 유저 id 정보
    @Override
    public String getUsername() {
        return this.userName;
    }

    // 계정이 만료되지 않은 조건 지정
    @Override
    public boolean isAccountNonExpired() {
        return this.status.equals(BillerUserStatus.EXPIRED) == false;
    }

    // 계정이 잠금이 되지 않은 조건 지정
    @Override
    public boolean isAccountNonLocked() {
        return this.status.equals(BillerUserStatus.SUSPEND) == false;
    }

    // 비밀번호가 만료되지 않은 조건 지정
    @Override
    public boolean isCredentialsNonExpired() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime limit = passwordUpdatedAt.plusDays(5);
        return loginFailCount < 5 || (now.isBefore(limit)); // 비밀번호 5회 이하이고, 비밀번호 변경 90일 이내 일때
    }

    // 계정의 활성화 여부
    @Override
    public boolean isEnabled() {
        return true;
    }
}
