package com.study.partner_admin.auth;

import com.study.common.entity.BillerUser;
import com.study.common.repository.BillerUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@Slf4j
@Service
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private BillerUserRepository billerUserRepository;

    @PostConstruct
    public void init(){
        // 로그인후 이동할 페이지
        setDefaultTargetUrl("/pages/internal/welcome");

        // 파라미터 이용?
        setAlwaysUseDefaultTargetUrl(false);

        // ?customParameter=https://www.naver.com
        setTargetUrlParameter("customParameter");
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // todo login success 후 해야 할일 정의
        String userName = authentication.getName(); // 로그인에 성공한 사람의 id
        log.info("로그인 성공 : {}",userName);
        BillerUser billerUser = billerUserRepository.findByUserName(userName);

        // 마지막 로그인 일자 업데이트
        billerUser.setLastAccessAt(LocalDateTime.now());
        billerUser.setLoginFailCount(0);
        billerUserRepository.save(billerUser);


        super.onAuthenticationSuccess(request,response,authentication);
    }
}
