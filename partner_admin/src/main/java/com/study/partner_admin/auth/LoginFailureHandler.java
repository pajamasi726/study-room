package com.study.partner_admin.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Service
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("로그인 실패 exception : {}, message : {}",exception.getClass(), exception.getMessage());
            super.onAuthenticationFailure(request,response,exception);
        // 로그인 실패시 적용할 로직

        if(exception instanceof UsernameNotFoundException){
            //유저 없음
        }else if(exception instanceof BadCredentialsException){
            //비밀번호가 틀렸을 때 후속 작업
        }

        super.onAuthenticationFailure(request, response, exception);
    }
}
