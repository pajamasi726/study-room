package com.study.partner_admin.auth;

import com.study.common.entity.BillerUser;
import com.study.common.repository.BillerUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BillerUserDetailService implements UserDetailsService {

    @Autowired
    private BillerUserRepository billerUserRepository;

    // login post submit 이 일어날대 id == username 에 해당되는 부분 호출
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // todo db에서 username 사용자 찾기
        BillerUser billerUser = billerUserRepository.findByUserName(username);

        if(billerUser == null){
            throw new UsernameNotFoundException(username); // login failure handler 로 이동
        }

        // 로그인 성공

        // TODO 전처리 과정

        return BillerUserPricipal.of(billerUser);
    }
}
