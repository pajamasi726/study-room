package com.study.partner_admin.service;

import com.study.common.dto.Description;
import com.study.common.entity.RoomTime;
import com.study.common.repository.RoomTimeRepository;
import com.study.partner_admin.PartnerAdminTestApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class RoomTimeServiceTest extends PartnerAdminTestApplication {

    @Autowired
    @Qualifier("middleRestTemplate")
    private RestTemplate restTemplate;

    @Autowired
    private RoomTimeRepository roomTimeRepository;

    @Test
    public void create(){
        RoomTime roomTime = new RoomTime();

        Description description = new Description();
        description.setTitle("test1");
        description.setDescription("디스크립션");
        roomTime.setDescription(description);
        roomTime.setRoomId(1L);
        roomTimeRepository.save(roomTime);
    }
    @Test
    public void roomTimeTest(){
        List<String> frontResult = new ArrayList<>();
        List<String> apiResult = new ArrayList<>();
        List<String> adminResult = new ArrayList<>();

        String frontUrl = "http://localhost:8080/api/reservation/roomTime";
        String apiUrl = "http://localhost:9090/api/reservation/roomTime";
        String adminUrl = "http://localhost:6060/api/reservation/roomTime";

        int tryCount = 25;

        CompletableFuture.runAsync(() -> {
            for(int i = 0 ; i< tryCount; i++){
                String result = call(frontUrl);
                frontResult.add(result);
            }
        });

        CompletableFuture.runAsync(() -> {
            for(int i = 0 ; i< tryCount; i++){
                String result = call(adminUrl);
                adminResult.add(result);
            }
        });


        CompletableFuture.runAsync(() -> {
            for(int i = 0 ; i< tryCount; i++){
                String result = call(apiUrl);
                apiResult.add(result);
            }
        });

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("프론트서버 결과");
        print(frontResult);

        log.info("어드민서버 결과");
        print(adminResult);

        log.info("API서버 결과");
        print(apiResult);


        log.info("전체 결과");
        printTotal(frontResult,adminResult,apiResult);
    }

    public String call(String url){
        return restTemplate.getForObject(url,String.class);
    }

    // 1대별 출력
    public void print(List<String> result){
        int time01 = 0;
        int time02 = 0;
        int time03 = 0;
        int time04 = 0;
        int time05 = 0;
        int other = 0;

        for(String s : result){
            switch (s){
                case "TIME01" :
                    time01++;
                    break;

                case "TIME02" :
                    time02++;
                    break;

                case "TIME03" :
                    time03++;
                    break;

                case "TIME04" :
                    time04++;
                    break;

                case "TIME05" :
                    time05++;
                    break;

                case "RESERVATION FULL" :
                    other++;
                    break;
            }
        }

        int total = time01+time02+time03+time04+time05+other;
        log.info("결과 time01 : {} , time02 : {}, time03 : {} , time04 : {} , time05 : {}, 예약만료 : {} , total : {}",time01,time02,time03,time04,time05,other,total);
    }

    // 3대의 결과 출력
    public void printTotal(List<String> front ,List<String> admin ,List<String> api){
        int time01 = 0;
        int time02 = 0;
        int time03 = 0;
        int time04 = 0;
        int time05 = 0;
        int other = 0;

        for(String s : front){
            switch (s){
                case "TIME01" :
                    time01++;
                    break;

                case "TIME02" :
                    time02++;
                    break;

                case "TIME03" :
                    time03++;
                    break;

                case "TIME04" :
                    time04++;
                    break;

                case "TIME05" :
                    time05++;
                    break;

                case "RESERVATION FULL" :
                    other++;
                    break;
            }
        }

        for(String s : admin){
            switch (s){
                case "TIME01" :
                    time01++;
                    break;

                case "TIME02" :
                    time02++;
                    break;

                case "TIME03" :
                    time03++;
                    break;

                case "TIME04" :
                    time04++;
                    break;

                case "TIME05" :
                    time05++;
                    break;

                case "RESERVATION FULL" :
                    other++;
                break;
            }
        }

        for(String s : api){
            switch (s){
                case "TIME01" :
                    time01++;
                    break;

                case "TIME02" :
                    time02++;
                    break;

                case "TIME03" :
                    time03++;
                    break;

                case "TIME04" :
                    time04++;
                    break;

                case "TIME05" :
                    time05++;
                    break;

                case "RESERVATION FULL" :
                    other++;
                    break;
            }
        }

        int total = time01+time02+time03+time04+time05+other;
        log.info("서버 3대의 결과 time01 : {} , time02 : {}, time03 : {} , time04 : {} , time05 : {}, 예약만료 : {} , total : {}",time01,time02,time03,time04,time05,other,total);


    }
}
