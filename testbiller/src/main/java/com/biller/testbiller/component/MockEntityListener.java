package com.biller.testbiller.component;

import com.biller.testbiller.entity.BaseEntity;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Component
public class MockEntityListener {
    @PrePersist
    public void created(BaseEntity baseEntity){
        baseEntity.setCreatedAt(LocalDateTime.now());
        baseEntity.setCreatedBy("mockbiller");
    }

    @PreUpdate
    public void updated(BaseEntity baseEntity){
        baseEntity.setUpdatedAt(LocalDateTime.now());
        baseEntity.setUpdatedBy("mockbiller");
    }
}
