package com.biller.testbiller.model.timeTable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Hour {

    @JsonProperty("use_hour")
    private boolean useHour = true; // 디폴트 사용가능

    @JsonIgnore
    public boolean isUseAble(){
        return useHour;
    }
}
