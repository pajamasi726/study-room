package com.biller.testbiller.model.enumeration.confirm;

import com.biller.testbiller.model.enumeration.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  ConfirmCode implements BaseResponseCode {

    OK(0,"OK","확정되었습니다.","예약확정"),
    IMPOSSIBLE(0,"IMPOSSIBLE","예약불가능","예약불가능"),
    ERROR(2, "ERROR", "기타에러", "기타에러"),
            ;

    private Integer id;
    private String responseCode;
    private String message;
    private String description;
}
