package com.biller.testbiller.model.enumeration.reservation;

import com.biller.testbiller.model.enumeration.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MockBillerReservationConfirmStatus implements BaseEnum {

    WAIT(0, "대기"),
    IMPOSSIBLE(1, "불가"),
    CONFIRM(2,"확정"),
    CANCEL(3,"무료 취소"),
    PAY_CANCEL(4,"유료 취소")
    ;
    private Integer id;
    private String displayName;
}
