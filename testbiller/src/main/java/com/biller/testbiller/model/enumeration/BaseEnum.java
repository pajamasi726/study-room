package com.biller.testbiller.model.enumeration;

public interface BaseEnum {
    Integer getId();
    String getDisplayName();
}
