package com.biller.testbiller.model.enumeration.holding;

import com.biller.testbiller.model.enumeration.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HoldingErrorCode implements BaseResponseCode {
    OK(0,"OK","정상적으로 예약되었습니다.","예약성공"),
    NOT_FOUND(1,"NOT_FOUND","해당 정보를 찾을 수 없습니다.","검색 불가능"),
    ERROR(2, "ERROR", "기타에러", "기타에러"),
    IMPOSSIBLE_RESERVATION(3,"IMPOSSIBLE_RESERVATION","예약이 불가능합니다.","예약 불가능")
    ;

    private Integer id;
    private String responseCode;
    private String message;
    private String description;
}
