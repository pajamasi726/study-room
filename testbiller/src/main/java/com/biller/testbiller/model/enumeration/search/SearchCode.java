package com.biller.testbiller.model.enumeration.search;

import com.biller.testbiller.model.enumeration.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SearchCode implements BaseResponseCode {

    OK(0,"OK","정상","예약가능"),
    IMPOSSIBLE(1,"IMPOSSIBLE","예약 불가능","예약 불가능"),
    ERROR(2,"ERROR","에러","기타 오류"),
    NOT_FOUND(3,"NOT_FOUND","방을 찾을수 없습니다.","정보 없음. 잘못된 요청"),
    ;

    private Integer id;
    private String responseCode;
    private String message;
    private String description;
}
