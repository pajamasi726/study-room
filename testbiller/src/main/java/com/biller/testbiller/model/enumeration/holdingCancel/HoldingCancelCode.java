package com.biller.testbiller.model.enumeration.holdingCancel;

import com.biller.testbiller.model.enumeration.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  HoldingCancelCode  implements BaseResponseCode {

        OK(0,"OK","정상적으로 취소되었습니다.","예약취소"),
        NOT_FOUND(1,"NOT_FOUND","해당 정보를 찾을 수 없습니다.","검색 불가능"),
        ERROR(2, "ERROR", "기타에러", "기타에러"),
        IMPOSSIBLE(3,"IMPOSSIBLE","상태변경불가","상태변경불가")
        ;

        private Integer id;
        private String responseCode;
        private String message;
        private String description;

}
