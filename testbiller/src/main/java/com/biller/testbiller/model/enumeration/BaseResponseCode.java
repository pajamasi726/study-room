package com.biller.testbiller.model.enumeration;

public interface BaseResponseCode {
    Integer getId();
    String getResponseCode();
    String getMessage();
    String getDescription();
}
