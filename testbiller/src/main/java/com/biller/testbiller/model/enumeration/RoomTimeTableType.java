package com.biller.testbiller.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoomTimeTableType {
    HOUR(0,"1시간 단위")
    ;

    private int id;
    private String description;
}
