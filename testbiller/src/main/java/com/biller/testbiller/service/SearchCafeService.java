package com.biller.testbiller.service;

import com.biller.testbiller.dto.external.SearchCreatedResponse;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.repository.MockBillerCafeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SearchCafeService {

    @Autowired
    private MockBillerCafeRepository mockBillerCafeRepository;

    public SearchCreatedResponse search(String start, String end) {
        List<SearchCreatedResponse.ChangedMockBillerCafe> list = null;

        if (start != null && end != null) {

            LocalDateTime starts = LocalDateTime.parse(start, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            LocalDateTime ends = LocalDateTime.parse(end, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            List<MockBillerCafe> mockBillerCafeList = mockBillerCafeRepository.findByCreatedAtBetween(starts, ends);

            list = mockBillerCafeList.stream().map(mockBillerCafe -> {
                SearchCreatedResponse.ChangedMockBillerCafe changedMockBillerCafe = new SearchCreatedResponse.ChangedMockBillerCafe();
                changedMockBillerCafe.setCafeName(mockBillerCafe.getCafeName());
                return changedMockBillerCafe;
            }).collect(Collectors.toList());
            return new SearchCreatedResponse().setChangedMockBillerCafe(list);
        } else {
            return new SearchCreatedResponse().setChangedMockBillerCafe(list);
        }

    }

}
