package com.biller.testbiller.service;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.ReservationInfoRequest;
import com.biller.testbiller.dto.external.ReservationInfoResponse;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.enumeration.RoomTimeTableType;
import com.biller.testbiller.repository.MockBillerRoomRepository;
import com.biller.testbiller.repository.MockBillerRoomTimeTableRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class SearchRoomService {

    @Autowired
    private MockBillerRoomTimeTableRepository mockBillerRoomTimeTableRepository;

    @Autowired
    private MockBillerRoomRepository mockBillerRoomRepository;

    public ExternalHeader<ReservationInfoResponse> searchRoomtable(ExternalHeader<ReservationInfoRequest> request) {

        String roomCode = request.getData().getRoomCode();
        //string -> localdate
        log.info("date={}",request.getData().getDateTime());
        LocalDate date = LocalDate.parse(request.getData().getDateTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Optional<MockBillerRoom> optionalMockBillerRoom = mockBillerRoomRepository.findByRoomCode(roomCode);

        if (optionalMockBillerRoom.isPresent()) {
            Optional<MockBillerRoomTimeTable> optionalMockBillerRoomTimeTable = mockBillerRoomTimeTableRepository.findByMockBillerRoomAndDay(optionalMockBillerRoom.get(), date);


            MockBillerRoomTimeTable mockBillerRoomTimeTable=null;

            if (!optionalMockBillerRoomTimeTable.isPresent()) {
                mockBillerRoomTimeTable = MockBillerRoomTimeTable.of(optionalMockBillerRoom.get());
                mockBillerRoomTimeTable.setDay(date);
                mockBillerRoomTimeTableRepository.save(mockBillerRoomTimeTable);
            } else {
                mockBillerRoomTimeTable = optionalMockBillerRoomTimeTable.get();
            }
            List<ReservationInfoResponse.Hour> list = new ArrayList<>();
            ReservationInfoResponse.Hour hour;

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(1);
            hour.setOk(mockBillerRoomTimeTable.getHour_01().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(2);
            hour.setOk(mockBillerRoomTimeTable.getHour_02().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(3);
            hour.setOk(mockBillerRoomTimeTable.getHour_03().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(4);
            hour.setOk(mockBillerRoomTimeTable.getHour_04().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(5);
            hour.setOk(mockBillerRoomTimeTable.getHour_05().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(6);
            hour.setOk(mockBillerRoomTimeTable.getHour_06().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(7);
            hour.setOk(mockBillerRoomTimeTable.getHour_07().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(8);
            hour.setOk(mockBillerRoomTimeTable.getHour_08().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(9);
            hour.setOk(mockBillerRoomTimeTable.getHour_09().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(10);
            hour.setOk(mockBillerRoomTimeTable.getHour_10().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(11);
            hour.setOk(mockBillerRoomTimeTable.getHour_11().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(12);
            hour.setOk(mockBillerRoomTimeTable.getHour_12().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(13);
            hour.setOk(mockBillerRoomTimeTable.getHour_13().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(14);
            hour.setOk(mockBillerRoomTimeTable.getHour_14().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(15);
            hour.setOk(mockBillerRoomTimeTable.getHour_15().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(16);
            hour.setOk(mockBillerRoomTimeTable.getHour_16().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(17);
            hour.setOk(mockBillerRoomTimeTable.getHour_17().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(18);
            hour.setOk(mockBillerRoomTimeTable.getHour_18().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(19);
            hour.setOk(mockBillerRoomTimeTable.getHour_19().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(20);
            hour.setOk(mockBillerRoomTimeTable.getHour_20().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(21);
            hour.setOk(mockBillerRoomTimeTable.getHour_21().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(22);
            hour.setOk(mockBillerRoomTimeTable.getHour_22().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(23);
            hour.setOk(mockBillerRoomTimeTable.getHour_23().isUseAble() == false ? false : true);
            list.add(hour);

            hour = new ReservationInfoResponse.Hour();
            hour.setTime(24);
            hour.setOk(mockBillerRoomTimeTable.getHour_24().isUseAble() == false ? false : true);
            list.add(hour);


            ReservationInfoResponse response = new ReservationInfoResponse();
            response.setList(list);

            ExternalHeader<ReservationInfoResponse> externalHeader = new ExternalHeader<>();
            externalHeader.setData(response);

            return externalHeader;
        }

        return new ExternalHeader<ReservationInfoResponse>().

                setMessage("없는 방입니다.");
    }


}
