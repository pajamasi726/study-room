package com.biller.testbiller.service.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelRequest;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelResponse;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerReservationConfirm;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.enumeration.holdingCancel.HoldingCancelCode;
import com.biller.testbiller.model.enumeration.reservation.MockBillerReservationConfirmStatus;
import com.biller.testbiller.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.annotation.ExceptionProxy;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
@Slf4j
public class HoldingCancelService {

    @Autowired
    private MockBillerCafeService cafeService;

    @Autowired
    private MockBillerRoomService roomService;

    @Autowired
    private MockBillerReservationConfirmService confirmService;

    @Autowired
    private RoomTimeTableLogicService roomTimeTableLogicService;

    @Autowired
    private MockBillerRoomTimeTableService mockBillerRoomTimeTableService;


    private static final String API_NAME = "holdingCancel";

    public ExternalHeader<HoldingCancelResponse> cancel(ExternalHeader<HoldingCancelRequest> request) {
        // todo 유빈 MockBillerCafe에 취소 가능한 시간 컬럼 추가 후, 불가능한 경우 추가 해주세요
        // MockBillerCafe에 cancelImpossible부분 추가

        log.info("[HoldingCancelService] request data ={}", request.getData());
        if (request.getData().getReservationCode() == null || request.getData().getCafeCode() == null || request.getData().getRoomCode() == null) {
            ExternalHeader holdingCancelResponse = ExternalHeader.getHeader(HoldingCancelCode.ERROR, API_NAME, null);
            return holdingCancelResponse;
        }

        Optional<MockBillerCafe> cafe = cafeService.getCafeByCafeCode(request.getData().getCafeCode());
        Optional<MockBillerRoom> room = roomService.findByRoomCode(request.getData().getRoomCode());
        Optional<MockBillerReservationConfirm> confirm = confirmService.findByReservationCode(request.getData().getReservationCode());

        if (!cafe.isPresent() || !room.isPresent() || !confirm.isPresent()) {
            ExternalHeader holdingCancelResponse = ExternalHeader.getHeader(HoldingCancelCode.NOT_FOUND, API_NAME, null);
            return holdingCancelResponse;
        }

        MockBillerReservationConfirm mockBillerReservationConfirm = confirm.get();

        //mockbillerreservationconfirm이 wait인 경우
        //mockbillercafe에 cancelinterval 추가, 불가능한 경우 추가
        if (MockBillerReservationConfirmStatus.WAIT.equals(mockBillerReservationConfirm.getMockBillerReservationConfirmStatus())) {

            Optional<MockBillerCafe> mockBillerCafe = cafeService.getCafeByCafeCode(request.getData().getCafeCode());
            MockBillerReservationConfirm reservationConfirm = confirm.get();

            LocalDateTime now = LocalDateTime.now();
            LocalDateTime mockBillerReservationTime = reservationConfirm.getStartAt(); //예약이 확정된 곳의 startAt 시간을 가져온다.

            //먼저 취소가 가능한지 확인하는 부분(10분전) (-> startAt - 취소가능한 시간)
            LocalDateTime cancelImpossibleLimit = mockBillerReservationTime.minusMinutes(mockBillerCafe.get().getCancelImpossible() == null ? (60 * 24) : mockBillerCafe.get().getCancelImpossible());

            if (now.isAfter(cancelImpossibleLimit)) {
                //취소 아예 불가능 부분(스터디 시작하기 10분이 안남은 경우)
                return ExternalHeader.getHeader(HoldingCancelCode.IMPOSSIBLE, API_NAME, null);
            } else {
                //취소가 가능하지만,
                //취소가 무료인지 유료인지 확인하는 부분
                LocalDateTime cancelPayLimit = mockBillerReservationTime.minusHours(mockBillerCafe.get().getCancelInterval() == null ? (24 * 365) : mockBillerCafe.get().getCancelInterval());
                if (now.isAfter(cancelPayLimit)) {
                    //유료 취소부분
                    mockBillerReservationConfirm.setMockBillerReservationConfirmStatus(MockBillerReservationConfirmStatus.PAY_CANCEL);

                } else {
                    //무료 취소부분
                    mockBillerReservationConfirm.setMockBillerReservationConfirmStatus(MockBillerReservationConfirmStatus.CANCEL);
                }

                //예약 가능한 상태로 바꿔 주는 부분
                LocalDateTime time = LocalDateTime.parse(request.getData().getStartDateTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                LocalDateTime endTime = LocalDateTime.parse(request.getData().getEndDateTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                LocalDate day = LocalDate.of(time.getYear(), time.getMonth(), time.getDayOfMonth());
                Optional<MockBillerRoomTimeTable> optional = mockBillerRoomTimeTableService.findByMockBillerRoomAndDay(room.get(), day);
                if (optional.isPresent()) {
                    try {
                        roomTimeTableLogicService.setUseable(optional.get(), time, endTime, true);
                    } catch (Exception e) {
                        return ExternalHeader.getHeader(HoldingCancelCode.ERROR, API_NAME, null);
                    }
                }
                //응답부분에 data 세팅해주는 부분
                HoldingCancelResponse response = new HoldingCancelResponse();
                response.setBillerCode(request.getData().getBillerCode());
                response.setCafeCode(cafe.get().getCafeCode());
                response.setRoomCode(room.get().getRoomCode());
                response.setReservationCode(confirm.get().getReservationCode());

                ExternalHeader<HoldingCancelResponse> header = ExternalHeader.getHeader(HoldingCancelCode.OK, API_NAME, response);
                return header;
            }
        } else {
            return ExternalHeader.getHeader(HoldingCancelCode.IMPOSSIBLE, API_NAME, null);
        }
    }
}