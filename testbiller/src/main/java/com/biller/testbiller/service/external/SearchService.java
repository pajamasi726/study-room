package com.biller.testbiller.service.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.search.SearchRequest;
import com.biller.testbiller.dto.external.search.SearchResponse;
import com.biller.testbiller.process.search.MockBillerSearchProcess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SearchService {

    @Autowired
    private MockBillerSearchProcess mockBillerSearchProcess;

    public ExternalHeader<SearchResponse> search(ExternalHeader<SearchRequest> searchRequest){
        return mockBillerSearchProcess.search(searchRequest);
    }


}
