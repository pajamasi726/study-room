package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerReservationConfirm;
import com.biller.testbiller.entity.MockBillerReservationHistory;
import com.biller.testbiller.repository.MockBillerReservationHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MockBillerReservationHistoryService {

    @Autowired
    private MockBillerReservationHistoryRepository mockBillerReservationHistoryRepository;

    public void save(MockBillerReservationConfirm confirm) {
        MockBillerReservationHistory history = new MockBillerReservationHistory()
                .setAmount(confirm.getAmount())
                .setConfirmedAt(confirm.getConfirmedAt())
                .setDay(confirm.getDay())
                .setStartAt(confirm.getStartAt())
                .setEndAt(confirm.getEndAt())
                .setMockBillerRoom(confirm.getMockBillerRoom())
                .setReservationCode(confirm.getReservationCode())
                .setMockBillerReservationConfirmStatus(confirm.getMockBillerReservationConfirmStatus())
                .setRequestedAt(confirm.getRequestedAt());
        mockBillerReservationHistoryRepository.save(history);
    }
}
