package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBiller;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.repository.MockBillerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class MockBillerService {

    @Autowired
    private MockBillerRepository mockBillerRepository;

    @Transactional
    public Optional<MockBiller> getMockBiller(String billerCode){
        return mockBillerRepository.findTopByBillerCode(billerCode);
    }

    @Transactional
    public Optional<MockBillerCafe> getBillerCafeByZipCode(MockBiller mockBiller, String zipCode){
        return mockBiller.getMockBillerCafeList().stream()
                .filter(cafe -> cafe.getCafeCode().equals(zipCode))
                .findFirst();
    }

}
