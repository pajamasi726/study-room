package com.biller.testbiller.service.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.confirm.ConfirmRequest;
import com.biller.testbiller.dto.external.confirm.ConfirmResponse;
import com.biller.testbiller.entity.MockBiller;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerReservationConfirm;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.model.enumeration.confirm.ConfirmCode;
import com.biller.testbiller.model.enumeration.reservation.MockBillerReservationConfirmStatus;
import com.biller.testbiller.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class ConfirmService {

    @Autowired
    private  MockBillerService mockBillerService;

    @Autowired
    private MockBillerCafeService cafeService;

    @Autowired
    private MockBillerRoomService roomService;

    @Autowired
    private MockBillerReservationConfirmService confirmService;

    private static final String API_NAME = "confirm";

    public ExternalHeader<ConfirmResponse> confirm(ExternalHeader<ConfirmRequest> request) {

        //값이 없는 경우
        log.info("[ConfirmService] request data ={}", request.getData());
        if (request.getData().getReservationCode() == null || request.getData().getCafeCode() == null || request.getData().getRoomCode() == null) {
            ExternalHeader confirmResponse = ExternalHeader.getHeader(ConfirmCode.ERROR, API_NAME, null);
            return confirmResponse;
        }
        Optional<MockBiller> optionalBiller = mockBillerService.getMockBiller(request.getData().getBillerCode());
        Optional<MockBillerCafe> optionalCafe = cafeService.getCafeByCafeCode(request.getData().getCafeCode());
        Optional<MockBillerRoom> optionalRoom = roomService.findByRoomCode(request.getData().getRoomCode());
        Optional<MockBillerReservationConfirm> optionalConfirm = confirmService.findByReservationCode(request.getData().getReservationCode());

        if(!(optionalBiller.isPresent()||optionalCafe.isPresent() || optionalRoom.isPresent() || optionalConfirm.isPresent())){
            return ExternalHeader.getHeader(ConfirmCode.IMPOSSIBLE, API_NAME, null);
        }
        MockBiller mockBiller = optionalBiller.get();
        MockBillerCafe mockBillerCafe = optionalCafe.get();
        MockBillerRoom mockBillerRoom = optionalRoom.get();
        MockBillerReservationConfirm mockBillerReservationConfirm = optionalConfirm.get();

        //요청으로 온 reservationcode와 확정된 reservationcode가 같지 않은 경우
        if (!mockBillerReservationConfirm.getReservationCode().equals(request.getData().getReservationCode())) {
            return ExternalHeader.getHeader(ConfirmCode.IMPOSSIBLE, API_NAME, null);
        }

        //상태가 WAIT으로 온 경우
        if (MockBillerReservationConfirmStatus.WAIT.equals(mockBillerReservationConfirm.getMockBillerReservationConfirmStatus())) {
            mockBillerReservationConfirm.setMockBillerReservationConfirmStatus(MockBillerReservationConfirmStatus.CONFIRM);
            confirmService.save(mockBillerReservationConfirm);
            return makeResponse(mockBiller, mockBillerCafe, mockBillerRoom, mockBillerReservationConfirm);
        } else {
            return ExternalHeader.getHeader(ConfirmCode.ERROR, API_NAME, null);
        }

    }
    private ExternalHeader<ConfirmResponse> makeResponse(
            MockBiller mockBiller,
            MockBillerCafe mockBillerCafe,
            MockBillerRoom mockBillerRoom,
            MockBillerReservationConfirm mockBillerReservationConfirm){

        ConfirmResponse response = new ConfirmResponse();
        response.setBillerCode(mockBiller.getBillerCode());
        response.setCafeCode(mockBillerCafe.getCafeCode());
        response.setRoomCode(mockBillerRoom.getRoomCode());
        response.setReservationCode(mockBillerReservationConfirm.getReservationCode());

        return ExternalHeader.getHeader(ConfirmCode.OK,API_NAME,response);
    }
}