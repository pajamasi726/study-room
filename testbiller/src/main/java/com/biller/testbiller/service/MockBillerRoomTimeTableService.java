package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.repository.MockBillerRoomTimeTableRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class MockBillerRoomTimeTableService {

    @Autowired
    private MockBillerRoomTimeTableRepository mockBillerRoomTimeTableRepository;

    @Transactional
    public MockBillerRoomTimeTable create(MockBillerRoomTimeTable mockBillerTimeTable){
        return mockBillerRoomTimeTableRepository.save(mockBillerTimeTable);
    }

    public void save(MockBillerRoomTimeTable mockBillerRoomTimeTable){
        mockBillerRoomTimeTableRepository.save(mockBillerRoomTimeTable);
    }

    public Optional<MockBillerRoomTimeTable> findByDay(LocalDate localDate){
        return mockBillerRoomTimeTableRepository.findTopByDay(localDate);
    }

    public Optional<MockBillerRoomTimeTable> findByDay(LocalDateTime localDateTime){
        LocalDate localDate = LocalDate.of(localDateTime.getYear(), localDateTime.getMonth(), localDateTime.getDayOfMonth());
        return findByDay(localDate);
    }

    public Optional<MockBillerRoomTimeTable> findByMockBillerRoomAndDay(MockBillerRoom mockBillerRoom, LocalDate day){
        return mockBillerRoomTimeTableRepository.findByMockBillerRoomAndDay(mockBillerRoom, day);
    }
}
