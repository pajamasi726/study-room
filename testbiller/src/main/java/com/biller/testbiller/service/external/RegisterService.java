package com.biller.testbiller.service.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.register.RegisterRequest;
import com.biller.testbiller.dto.external.register.RegisterResponse;
import com.biller.testbiller.entity.MockBiller;
import com.biller.testbiller.process.register.MockBillerRegisterProcess;
import com.biller.testbiller.repository.MockBillerRepository;
import com.biller.testbiller.service.RestTemplateService;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Slf4j
@Service
public class RegisterService {


    @Value("${url.platform.api}")
    private String API_SERVER;
    private static final String registerApiUrl = "/api/platform/reservation/register";

    @Autowired
    private MockBillerRepository mockBillerRepository;

    @Autowired
    private MockBillerRegisterProcess mockBillerRegisterProcess;

    @Autowired
    private RestTemplateService restTemplateService;

    @Transactional
    public void register(String billerName){
        MockBiller mockBiller = mockBillerRepository.findByBillerName(billerName);

        ExternalHeader<RegisterRequest> header = mockBillerRegisterProcess.makeRegisterData(mockBiller);
        log.info("Register Request : {}",header);
        URI url = UriComponentsBuilder.fromUriString(API_SERVER)
                .path(registerApiUrl)
                .build()
                .toUri();

        ParameterizedTypeReference<ExternalHeader<RegisterResponse>> type =
                new ParameterizedTypeReference<ExternalHeader<RegisterResponse>>() {};

        try{
            ExternalHeader<RegisterResponse> response = restTemplateService.send(url,HttpMethod.POST,header,type);
            mockBillerRegisterProcess.registerResponse(mockBiller, response);
        }catch (Exception e){
            log.error("REST TEMPLATE ERROR : {}",e.getLocalizedMessage());
        }
    }
}
