package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.timeTable.Hour;
import com.biller.testbiller.repository.MockBillerRoomTimeTableRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class RoomTimeTableLogicService {

    @Autowired
    private MockBillerRoomTimeTableRepository mockBillerRoomTimeTableRepository;

    public boolean isUseAble(MockBillerRoomTimeTable mockBillerRoomTimeTable, LocalDateTime start, LocalDateTime end) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<Integer> searchTime = getUseHourList(start, end);

        for(int time : searchTime){
            String number = (time<10) ? "0"+time : ""+time;      //01  10
            String getMethodName = "getHour_"+number;

            Method getMethod = mockBillerRoomTimeTable.getClass().getDeclaredMethod(getMethodName,new Class[]{});
            Hour hour = (Hour)getMethod.invoke(mockBillerRoomTimeTable);
            if(hour.isUseAble()==false)
                return false;
        }
        return true;
    }

    public void setUseable(MockBillerRoomTimeTable mockBillerRoomTimeTable, LocalDateTime start, LocalDateTime end,boolean bool) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        log.info("[RoomTimeTableLogicService] mockBillerRoomTimeTable :{}",mockBillerRoomTimeTable);
        List<Integer> searchTime = getUseHourList(start, end);

        for (int time : searchTime) {
            String number = (time<10) ? "0"+time : ""+time;      //01  10
            String setMethodName = "setHour_"+number;
            Method setMethod = mockBillerRoomTimeTable.getClass().getDeclaredMethod(setMethodName,new Class[]{Hour.class});
            setMethod.invoke(mockBillerRoomTimeTable,new Hour(bool));
        }
        mockBillerRoomTimeTableRepository.save(mockBillerRoomTimeTable);
        log.info("[RoomTimeTableLogicService] mockBillerRoomTimeTable :{}",mockBillerRoomTimeTable);
    }

    public List<Integer> getUseHourList(LocalDateTime start, LocalDateTime end) {
        List<Integer> searchTime = new ArrayList<>();

        int startTime = start.getHour(); // ex 13
        int endTime = end.getHour();    // ex 16

        for (int i = startTime; i < endTime; i++) {
            // 13, 14 ,15
            searchTime.add(i+1);
        }

        searchTime.stream().forEach(time -> log.info("검색해야할 시간대 : {}", time));
        return searchTime;
    }
}
