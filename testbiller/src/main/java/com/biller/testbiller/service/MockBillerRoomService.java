package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.repository.MockBillerRoomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class MockBillerRoomService {

    @Autowired
    private MockBillerRoomRepository mockBillerRoomRepository;

    @Transactional
    public MockBillerRoom create(MockBillerRoom mockBillerRoom){
        return mockBillerRoomRepository.save(mockBillerRoom);
    }

    public Optional<MockBillerRoom> getRoomByRoomCode(MockBillerCafe mockBillerCafe, String roomCode){
        return mockBillerCafe.getMockBillerRoomList().stream()
                .filter(room -> room.getRoomCode().equals(roomCode))
                .findFirst();
    }

    public Optional<MockBillerRoom> findByRoomCode(String roomCode){
        return mockBillerRoomRepository.findByRoomCode(roomCode);
    }

}
