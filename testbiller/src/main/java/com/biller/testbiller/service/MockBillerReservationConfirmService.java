package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerReservationConfirm;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.model.enumeration.reservation.MockBillerReservationConfirmStatus;
import com.biller.testbiller.repository.MockBillerReservationConfirmRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class MockBillerReservationConfirmService {
    @Autowired
    private MockBillerReservationConfirmRepository mockBillerReservationConfirmRepository;

    @Autowired
    private MockBillerReservationHistoryService mockBillerReservationHistoryService;

    public void save(MockBillerReservationConfirm mockBillerReservationConfirm){
        mockBillerReservationConfirmRepository.save(mockBillerReservationConfirm);
    }

    public Optional<MockBillerReservationConfirm> findByReservationCode(String reservationCode){
        return mockBillerReservationConfirmRepository.findByReservationCode(reservationCode);
    }

    public MockBillerReservationConfirm save(
            String reservationCode, LocalDateTime startTime, LocalDateTime endTime, MockBillerRoom mockBillerRoom,int personCount){

        int useTime = endTime.getHour() - startTime.getHour();
        MockBillerReservationConfirm mockBillerReservationConfirm
                = new MockBillerReservationConfirm()
                .setReservationCode(reservationCode)
                .setDay(LocalDate.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth()))//예약 날짜
                .setMockBillerRoom(mockBillerRoom)// 룸 아이디
                .setStartAt(startTime).setEndAt(endTime)//시간
                .setAmount(mockBillerRoom.getPrice() * personCount * useTime)//가격
                .setMockBillerReservationConfirmStatus(MockBillerReservationConfirmStatus.WAIT)//상태
                .setRequestedAt(LocalDateTime.now());//거래일시
        save(mockBillerReservationConfirm);
        log.info("[MockBillerReservationProcess] mockBillerReservationConfirm = {}",mockBillerReservationConfirm);


        //히스토리 저장
        mockBillerReservationHistoryService.save(mockBillerReservationConfirm);
        return mockBillerReservationConfirm;
    }
}
