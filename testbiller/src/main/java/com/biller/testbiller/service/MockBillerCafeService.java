package com.biller.testbiller.service;

import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.repository.MockBillerCafeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Slf4j
@Service
public class MockBillerCafeService {
    @Autowired
    private MockBillerCafeRepository mockBillerCafeRepository;

    public Optional<MockBillerCafe> getCafeByCafeCode(String cafeCode){
        return mockBillerCafeRepository.findByCafeCode(cafeCode);
    }
}
