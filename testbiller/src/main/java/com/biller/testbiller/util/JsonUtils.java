package com.biller.testbiller.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@Data
public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String toJson(Object object){
        try {
            String json = objectMapper.writeValueAsString(object);
            log.info("[JsonUtils] toJson : {}",json);
            return json;
        } catch (JsonProcessingException e) {
            log.error("{}",e);
            return "";
        }
    }

    public static <T> Optional<T> toObject(String value, Class<T> clazz){
        try {
            return Optional.ofNullable(objectMapper.readValue(value,clazz));
        } catch (IOException e) {
            log.error("{}",e);
            return Optional.empty();
        }
    }

}
