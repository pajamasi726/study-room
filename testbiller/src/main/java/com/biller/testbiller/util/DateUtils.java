package com.biller.testbiller.util;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class DateUtils {

    public static String API_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
