package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface MockBillerRoomTimeTableRepository extends JpaRepository<MockBillerRoomTimeTable,Long> {
    Optional<MockBillerRoomTimeTable> findTopByDay(LocalDate day);
    Optional<MockBillerRoomTimeTable> findByMockBillerRoomAndDay(MockBillerRoom mockBillerRoom, LocalDate day);
    //Long id (X) 객체로 검색

}
