package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBillerReservationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MockBillerReservationHistoryRepository extends JpaRepository<MockBillerReservationHistory,Long> {
    
}
