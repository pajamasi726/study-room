package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MockBillerRoomRepository extends JpaRepository<MockBillerRoom,Long> {

    Optional<MockBillerRoom> findByRoomCode(String roomCode);
    Optional<MockBillerRoom> findByName(String roomName);
    Optional<MockBillerRoom> findByMockBillerCafeAndName(MockBillerCafe mockBillerCafe,String roomName);
}
