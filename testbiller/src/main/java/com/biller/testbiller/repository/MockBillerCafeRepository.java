package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface MockBillerCafeRepository extends JpaRepository<MockBillerCafe,Long> {
    Optional<MockBillerCafe> findByCafeCode(String cafeCode);
    Optional<MockBillerCafe> findByCafeName(String cafeName);
    List<MockBillerCafe> findByCreatedAtBetween(LocalDateTime start, LocalDateTime end);
}
