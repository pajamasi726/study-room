package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBillerReservationConfirm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MockBillerReservationConfirmRepository extends JpaRepository<MockBillerReservationConfirm,Long> {
    Optional<MockBillerReservationConfirm> findByReservationCode(String reservationCode);
}
