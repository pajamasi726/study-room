package com.biller.testbiller.repository;

import com.biller.testbiller.entity.MockBiller;
import com.biller.testbiller.entity.MockBillerRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MockBillerRepository extends JpaRepository<MockBiller,Long> {
    Optional<MockBiller> findTopByBillerCode(String billerCode);
    MockBiller findByBillerName(String billerName);
}
