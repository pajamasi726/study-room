package com.biller.testbiller.process.search;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.search.SearchRequest;
import com.biller.testbiller.dto.external.search.SearchResponse;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.enumeration.search.SearchCode;
import com.biller.testbiller.service.MockBillerRoomService;
import com.biller.testbiller.service.MockBillerRoomTimeTableService;
import com.biller.testbiller.service.MockBillerService;
import com.biller.testbiller.service.RoomTimeTableLogicService;
import com.biller.testbiller.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class MockBillerSearchProcess {

    @Autowired
    private MockBillerService mockBillerService;

    @Autowired
    private MockBillerRoomService mockBillerRoomService;

    @Autowired
    private MockBillerRoomTimeTableService mockBillerRoomTimeTableService;

    @Autowired
    private RoomTimeTableLogicService roomTimeTableLogicService;

    private static final String SEARCH_API = "search";


    /** api call */
    @Transactional
    public ExternalHeader<SearchResponse> search(ExternalHeader<SearchRequest> request){
        return validation(request).orElseGet(()->process(request));
    }

    /** search request validation */
    protected Optional<ExternalHeader<SearchResponse>> validation(ExternalHeader<SearchRequest> request){
        SearchRequest searchRequest = request.getData();

        if(Objects.isNull(searchRequest)){
            log.error("[MockBillerSearchProcess] search request null : {}",request);
            ExternalHeader externalHeader = getHeader(SEARCH_API,SearchCode.ERROR);
            return Optional.of(externalHeader);
        }

        if(Strings.isEmpty(searchRequest.getBillerCode()) || Strings.isEmpty(searchRequest.getZipCode()) || Strings.isEmpty(searchRequest.getRoomCode())){
            log.error("[MockBillerSearchProcess] search request billerCode or cafeCode or roomCode is null : {}",request);
            ExternalHeader externalHeader = getHeader(SEARCH_API,SearchCode.ERROR);
            return Optional.of(externalHeader);
        }

        if(Strings.isEmpty(searchRequest.getStartDateTime()) || Strings.isEmpty(searchRequest.getEndDateTime())){
            log.error("[MockBillerSearchProcess] search request time is null : {}",request);
            ExternalHeader externalHeader = getHeader(SEARCH_API,SearchCode.ERROR);
            return Optional.of(externalHeader);
        }

        return Optional.empty();
    }

    /** api process */
    protected ExternalHeader<SearchResponse> process(ExternalHeader<SearchRequest> request){
        SearchRequest searchRequest = request.getData();

        String billerCode = searchRequest.getBillerCode();
        String zipCode = searchRequest.getZipCode();
        String roomCode = searchRequest.getRoomCode();

        LocalDateTime startTime = LocalDateTime.parse(searchRequest.getStartDateTime(),DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));
        LocalDateTime endTime = LocalDateTime.parse(searchRequest.getEndDateTime(),DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));

        // 카페찾기
        Optional<MockBillerCafe> mockBillerCafe = mockBillerService.getMockBiller(billerCode)
                .map(mockBiller -> mockBillerService.getBillerCafeByZipCode(mockBiller, zipCode))
                .orElseGet(()->Optional.empty());

        // 해당되는 방 찾기
        Optional<MockBillerRoom> mockBillerRoom = mockBillerCafe
                .map(cafe -> mockBillerRoomService.getRoomByRoomCode(cafe,roomCode))
                .orElseGet(()->Optional.empty());

        // 해당되는 방의 타임 찾기
        Optional<MockBillerRoomTimeTable> mockBillerRoomTimeTable = mockBillerRoom
                .map(room -> mockBillerRoomTimeTableService.findByDay(startTime))
                .orElseGet(()->Optional.empty());


        // 시간 테이블에서 사용가능한지 찾기
        if(mockBillerRoomTimeTable.isPresent()){
            boolean isUseAble = false;
            try {
                isUseAble = roomTimeTableLogicService.isUseAble(mockBillerRoomTimeTable.get(), startTime, endTime);
            } catch (Exception e) {
                log.info("roomTimeTableLogicService.isUseAble() is err");
            }

            if(isUseAble){
                // 사용가능
                ExternalHeader externalHeader = getHeader(SEARCH_API, SearchCode.OK);
                SearchResponse searchResponse = new SearchResponse();
                searchResponse.setBillerCode(billerCode).setZipCode(zipCode).setRoomCode(roomCode);
                return externalHeader.setData(searchResponse);

            }else{
                // 사용불가
                ExternalHeader externalHeader = getHeader(SEARCH_API, SearchCode.IMPOSSIBLE);
                return externalHeader;
            }


        }else{
            // 방이 없을때는 새로 insert 해준다
            MockBillerRoomTimeTable newTimeTable = MockBillerRoomTimeTable.of(mockBillerRoom.get());
            mockBillerRoomTimeTableService.create(newTimeTable);

            //응답은 OK
            ExternalHeader externalHeader = getHeader(SEARCH_API, SearchCode.OK);
            SearchResponse searchResponse = new SearchResponse();
            searchResponse.setBillerCode(billerCode).setZipCode(zipCode).setRoomCode(roomCode);
            return externalHeader.setData(searchResponse);
        }
    }


    protected ExternalHeader<SearchResponse> getHeader(String apiName, SearchCode searchCode){
        ExternalHeader<SearchResponse> externalHeader = new ExternalHeader();
        externalHeader.setApiName(apiName)
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS")))
                .setResponseCode(searchCode.getResponseCode())
                .setMessage(searchCode.getMessage())
        ;
        return externalHeader;
    }

}
