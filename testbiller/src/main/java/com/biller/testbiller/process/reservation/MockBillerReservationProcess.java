package com.biller.testbiller.process.reservation;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.reservation.ReservationRequest;
import com.biller.testbiller.dto.external.reservation.ReservationResponse;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerReservationConfirm;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.enumeration.holding.HoldingErrorCode;
import com.biller.testbiller.model.enumeration.reservation.MockBillerReservationConfirmStatus;
import com.biller.testbiller.service.*;
import com.biller.testbiller.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class MockBillerReservationProcess {
    @Autowired
    private MockBillerReservationConfirmService mockBillerReservationConfirmService;
    @Autowired
    private MockBillerReservationHistoryService mockBillerReservationHistoryService;
    @Autowired
    private RoomTimeTableLogicService roomTimeTableLogicService;
    @Autowired
    private MockBillerRoomService roomService;
    @Autowired
    private MockBillerCafeService cafeService;
    @Autowired
    private MockBillerRoomTimeTableService mockBillerRoomTimeTableService;

    private String RESERVATION_API = "holding";

    protected Optional<ExternalHeader<ReservationResponse>> validate(ExternalHeader<ReservationRequest> request) {
        log.info("api name = {} trans date time = {} ", request.getApiName(), request.getTransDateTime());

        ReservationRequest data = request.getData();
        log.info("[MockBillerReservationProcess] request data = {}", data);

        //데이터가 null일 경우
        if (Objects.isNull(data)) {
            log.info("[MockBillerReservationProcess] data object is null");
            ExternalHeader externalHeader = new ExternalHeader();
            return Optional.ofNullable(externalHeader.getHeader(HoldingErrorCode.ERROR, RESERVATION_API, null));
        }

        LocalDateTime startTime = LocalDateTime.parse(data.getStartDateTime(), DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));
        LocalDateTime endTime = LocalDateTime.parse(data.getEndDateTime(), DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));


        //데이터 이상값 확인
        boolean impossible = false;
        if (data.getPersonCount() == 0) {
            log.info("[MockBillerReservationProcess] Person count is zero");
            impossible = true;
        }
        if (Strings.isEmpty(data.getCafeCode()) || Strings.isEmpty(data.getRoomCode())) {
            log.info("[MockBillerReservationProcess] Cafe code or Room code is empty");
            impossible = true;
        }
        if (startTime.getHour() > 24 && endTime.getHour() > 24 && startTime.getHour() >= endTime.getHour()) {
            log.info("[MockBillerReservationProcess] start time is between 0 and 24");
            impossible = true;
        }
        if (Strings.isEmpty(data.getReservationCode())) {
            log.info("[MockBillerReservationProcess] reservation code is  null");
            impossible = true;
        }
        if (impossible) {
            ExternalHeader externalHeader = new ExternalHeader();
            return Optional.ofNullable(externalHeader.getHeader(HoldingErrorCode.ERROR, RESERVATION_API, null));
        }

        return Optional.empty();
    }

    //트랜잭션
    @Transactional
    public ExternalHeader<ReservationResponse> holding(ExternalHeader<ReservationRequest> request) {
        return validate(request).orElseGet(() -> process(request));
    }

    protected ExternalHeader<ReservationResponse> process(ExternalHeader<ReservationRequest> request) {
        ReservationRequest data = request.getData();

        //카페 검색
        Optional<MockBillerCafe> optinalMockBillerCafe = cafeService.getCafeByCafeCode(data.getCafeCode());
        log.info("[MockBillerReservationProcess] mockBillerCafe : {}", optinalMockBillerCafe);
        if (!optinalMockBillerCafe.isPresent()) {
            return ExternalHeader.getHeader(HoldingErrorCode.NOT_FOUND, RESERVATION_API, null);
        }
        MockBillerCafe mockBillerCafe = optinalMockBillerCafe.get();

        //룸 검색
        Optional<MockBillerRoom> optionalMockBillerRoom = roomService.getRoomByRoomCode(mockBillerCafe, data.getRoomCode());
        log.info("[MockBillerReservationProcess] mockBillerRoom : {}", optionalMockBillerRoom);
        if (!optionalMockBillerRoom.isPresent()) {
            return ExternalHeader.getHeader(HoldingErrorCode.NOT_FOUND, RESERVATION_API, null);
        }
        MockBillerRoom mockBillerRoom = optionalMockBillerRoom.get();

        //해당 시간, 날짜 예약 가능
        LocalDateTime startTime = LocalDateTime.parse(data.getStartDateTime(), DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));
        LocalDateTime endTime = LocalDateTime.parse(data.getEndDateTime(), DateTimeFormatter.ofPattern(DateUtils.API_DATE_FORMAT));

        //시간 테이블 검사
        LocalDate startDay = LocalDate.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth());
        Optional<MockBillerRoomTimeTable> mockBillerRoomTimeTable = mockBillerRoomTimeTableService.findByMockBillerRoomAndDay(mockBillerRoom,startDay);


        MockBillerRoomTimeTable timeTable;

        //해당 튜플이 존재하지 않을 때 -> 예약 건이 하나도 없을 때
        if (!mockBillerRoomTimeTable.isPresent()) {
            timeTable = MockBillerRoomTimeTable.of(mockBillerRoom);
            timeTable.setDay(LocalDate.of(startTime.getYear(), startTime.getMonth(), startTime.getDayOfMonth()));
            mockBillerRoomTimeTableService.save(timeTable);
        } else {
            timeTable = mockBillerRoomTimeTable.get();
        }
        log.info("[MockBillerReservationProcess] timeTable = {}", timeTable);

        boolean isUseAble = false;
        try {
            isUseAble = roomTimeTableLogicService.isUseAble(timeTable, startTime, endTime);
        } catch (Exception e) {
            log.info("[MockBillerReservationProcess] roomTimeTableLogicService isUseAble error");
        }
        log.info("[MockBillerReservationProcess] isUseAble = {}", isUseAble);
        ///사용 가능할 때
        if (isUseAble) {
            //시간 업데이트
            try {
                roomTimeTableLogicService.setUseable(timeTable, startTime, endTime, false);
            } catch (Exception e) {
                log.info("[MockBillerReservationProcess] roomTimeTableLogicService setUseable error");
                //예약 실패
                return ExternalHeader.getHeader(HoldingErrorCode.ERROR, RESERVATION_API, null);
            }
            //예약 내용 저장
            MockBillerReservationConfirm mockBillerReservationConfirm =
                    mockBillerReservationConfirmService.save(data.getReservationCode(), startTime, endTime, mockBillerRoom, data.getPersonCount());

            //response 정제
            return makeReservationResponse(mockBillerCafe, mockBillerRoom, mockBillerReservationConfirm);
        } else {
            log.info("[MockBillerReservationProcess] reservation impossible");
            ExternalHeader externalHeader = new ExternalHeader();
            return externalHeader.getHeader(HoldingErrorCode.IMPOSSIBLE_RESERVATION, RESERVATION_API, null);
        }

    }

    public ExternalHeader<ReservationResponse> makeReservationResponse(
            MockBillerCafe mockBillerCafe, MockBillerRoom mockBillerRoom, MockBillerReservationConfirm mockBillerReservationConfirm) {
        //response 정제
        ReservationResponse reservationResponse = new ReservationResponse()
                .setBillerCode(mockBillerCafe.getMockBiller().getBillerCode())
                .setCafeCode(mockBillerCafe.getCafeCode())
                .setRoomCode(mockBillerRoom.getRoomCode())
                .setAmount(mockBillerReservationConfirm.getAmount())
                .setEndDateTime(mockBillerReservationConfirm.getEndAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .setStartDateTime(mockBillerReservationConfirm.getStartAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .setReservationCode(mockBillerReservationConfirm.getReservationCode());
        log.info("[MockBillerReservationProcess] response = {}", reservationResponse);
        ExternalHeader externalHeader = new ExternalHeader();

        return externalHeader.getHeader(HoldingErrorCode.OK, RESERVATION_API, reservationResponse);
    }


}
