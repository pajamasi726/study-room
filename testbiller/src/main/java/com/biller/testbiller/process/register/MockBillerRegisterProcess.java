package com.biller.testbiller.process.register;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.register.RegisterRequest;
import com.biller.testbiller.dto.external.register.RegisterResponse;
import com.biller.testbiller.dto.external.search.SearchResponse;
import com.biller.testbiller.entity.MockBiller;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.model.enumeration.search.SearchCode;
import com.biller.testbiller.repository.MockBillerCafeRepository;
import com.biller.testbiller.repository.MockBillerRepository;
import com.biller.testbiller.repository.MockBillerRoomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MockBillerRegisterProcess {

    @Autowired
    private MockBillerRepository mockBillerRepository;

    @Autowired
    private MockBillerCafeRepository mockBillerCafeRepository;

    @Autowired
    private MockBillerRoomRepository mockBillerRoomRepository;

    private final String API_REGISTER_NAME = "register";

    @Transactional
    public ExternalHeader<RegisterRequest> makeRegisterData(MockBiller mockBiller){

        List<RegisterRequest.Cafe> cafeList = mockBiller.getMockBillerCafeList()
                .stream()
                .map(mockBillerCafe -> {
                    // cafe name 설정
                    RegisterRequest.Cafe cafe = new RegisterRequest.Cafe()
                            .setCafeName(mockBillerCafe.getCafeName())
                            .setDescription(mockBillerCafe.getDescription())
                            .setSummary(mockBillerCafe.getSummary())
                            .setOpenAt(mockBillerCafe.getOpenAt())
                            .setCloseAt(mockBillerCafe.getCloseAt())
                            .setPrecaution(mockBillerCafe.getPrecaution())
                            .setFacilities(mockBillerCafe.getFacilities())
                            .setCity(mockBillerCafe.getCity())
                            .setTown(mockBillerCafe.getTown())
                            .setDistrict(mockBillerCafe.getDistrict())
                            ;
                    // billerRoom 설정
                    List<MockBillerRoom> mockBillerRoomList = mockBillerCafe.getMockBillerRoomList();
                    List<RegisterRequest.Room> roomList = mockBillerRoomList.stream()
                            .map(mockBillerRoom -> {
                                RegisterRequest.Room room = new RegisterRequest.Room();
                                room.setRoomName(mockBillerRoom.getName());
                                room.setDescription(mockBillerRoom.getDescription());
                                room.setMinSeating(mockBillerRoom.getMinSeating());
                                room.setMaxSeating(mockBillerRoom.getMaxSeating());
                                room.setPrice(mockBillerRoom.getPrice());
                                room.setImgUrl01(mockBillerRoom.getImgUrl_01());
                                return room;
                            }).collect(Collectors.toList());

                    cafe.setRoomList(roomList);
                    return cafe;
                }).collect(Collectors.toList());

        String billerName = mockBiller.getBillerName();

        ExternalHeader externalHeader = getHeader(API_REGISTER_NAME,SearchCode.OK);
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setBillerName(billerName).setCafeList(cafeList).setIpAddress(mockBiller.getIpAddress());
        return externalHeader.setData(registerRequest);
    }

    @Transactional
    public void registerResponse(MockBiller mockBiller, ExternalHeader<RegisterResponse> response){
        mockBiller.setBillerCode(response.getData().getBillerCode());
        mockBillerRepository.save(mockBiller);
        log.info("cafe size : {}",response.getData().getCafeList().size());

        response.getData().getCafeList().stream().forEach(cafe -> {
            log.info("cafe name={},size={}",cafe.getCafeName(),cafe.getStudyRoom().size());
        });

        response.getData().getCafeList().stream()

                .forEach(cafe -> {
                    MockBillerCafe mockBillerCafe = mockBillerCafeRepository.findByCafeName(cafe.getCafeName()).get();
                    mockBillerCafe.setCafeCode(cafe.getCafeCode());


                    cafe.getStudyRoom().stream().forEach(studyRoom -> {
                        MockBillerRoom mockBillerRoom = mockBillerRoomRepository.findByMockBillerCafeAndName(mockBillerCafe,studyRoom.getRoomName()).get();
                        mockBillerRoom.setRoomCode(studyRoom.getRoomCode());
                        mockBillerRoomRepository.save(mockBillerRoom);
                    });

                    mockBillerCafeRepository.save(mockBillerCafe);
                });
    }


    protected ExternalHeader<SearchResponse> getHeader(String apiName, SearchCode searchCode){
        ExternalHeader<SearchResponse> externalHeader = new ExternalHeader();
        externalHeader.setApiName(apiName)
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS")))
                .setResponseCode(searchCode.getResponseCode())
                .setMessage(searchCode.getMessage())
        ;
        return externalHeader;
    }
}
