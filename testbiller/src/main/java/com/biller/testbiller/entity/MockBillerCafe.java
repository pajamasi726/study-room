package com.biller.testbiller.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.List;

@Slf4j
@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = "mockBillerRoomList")
public class MockBillerCafe extends BaseEntity {
    @ManyToOne
    private MockBiller mockBiller;
    @Column(name = "cafe_code", length = 20 , nullable = false)
    private String cafeCode;
    @Column(name = "cafe_name", length = 20, nullable = false)
    private String cafeName;
    private Integer cancelInterval; // 취소 가능한 시간
    private Integer cancelImpossible; // 취소 불가능한 분단위 시간
    private String description;
    private String summary;
    private Integer openAt;
    private Integer closeAt;
    private String precaution;
    @Column(name="img_url_01")
    private String imgUrl_01;
    @Column(name="img_url_02")
    private String imgUrl_02;
    @Column(name="img_url_03")
    private String imgUrl_03;
    private String facilities;
    private String town;
    private String city;
    private String district;

    @OneToMany(mappedBy = "mockBillerCafe",fetch = FetchType.LAZY)
    List<MockBillerRoom> mockBillerRoomList;
}
