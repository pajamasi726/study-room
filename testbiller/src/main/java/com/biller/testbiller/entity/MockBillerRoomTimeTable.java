package com.biller.testbiller.entity;

import com.biller.testbiller.converter.HourConverter;
import com.biller.testbiller.model.enumeration.RoomTimeTableType;
import com.biller.testbiller.model.timeTable.Hour;
import com.biller.testbiller.util.JsonUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.time.LocalDate;

@Slf4j
@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = {"mockBillerRoom"})
public class MockBillerRoomTimeTable extends BaseEntity{

    @ManyToOne
    private MockBillerRoom mockBillerRoom;

    @Column(name = "day")
    private LocalDate day;

    @Column(name = "type", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private RoomTimeTableType roomTimeTableType;

    @Convert(converter = HourConverter.class)
    private Hour Hour_01;

    @Convert(converter = HourConverter.class)
    private Hour Hour_02;

    @Convert(converter = HourConverter.class)
    private Hour Hour_03;

    @Convert(converter = HourConverter.class)
    private Hour Hour_04;

    @Convert(converter = HourConverter.class)
    private Hour Hour_05;

    @Convert(converter = HourConverter.class)
    private Hour Hour_06;

    @Convert(converter = HourConverter.class)
    private Hour Hour_07;

    @Convert(converter = HourConverter.class)
    private Hour Hour_08;

    @Convert(converter = HourConverter.class)
    private Hour Hour_09;

    @Convert(converter = HourConverter.class)
    private Hour Hour_10;

    @Convert(converter = HourConverter.class)
    private Hour Hour_11;

    @Convert(converter = HourConverter.class)
    private Hour Hour_12;

    @Convert(converter = HourConverter.class)
    private Hour Hour_13;

    @Convert(converter = HourConverter.class)
    private Hour Hour_14;

    @Convert(converter = HourConverter.class)
    private Hour Hour_15;

    @Convert(converter = HourConverter.class)
    private Hour Hour_16;

    @Convert(converter = HourConverter.class)
    private Hour Hour_17;

    @Convert(converter = HourConverter.class)
    private Hour Hour_18;

    @Convert(converter = HourConverter.class)
    private Hour Hour_19;

    @Convert(converter = HourConverter.class)
    private Hour Hour_20;

    @Convert(converter = HourConverter.class)
    private Hour Hour_21;

    @Convert(converter = HourConverter.class)
    private Hour Hour_22;

    @Convert(converter = HourConverter.class)
    private Hour Hour_23;

    @Convert(converter = HourConverter.class)
    private Hour Hour_24;


    public static MockBillerRoomTimeTable of(MockBillerRoom mockBillerRoom){
        MockBillerRoomTimeTable mockBillerRoomTimeTable = new MockBillerRoomTimeTable();
        mockBillerRoomTimeTable.setMockBillerRoom(mockBillerRoom);
        mockBillerRoomTimeTable.setRoomTimeTableType(RoomTimeTableType.HOUR);
        mockBillerRoomTimeTable.setHour_01(new Hour());
        mockBillerRoomTimeTable.setHour_02(new Hour());
        mockBillerRoomTimeTable.setHour_03(new Hour());
        mockBillerRoomTimeTable.setHour_04(new Hour());
        mockBillerRoomTimeTable.setHour_05(new Hour());
        mockBillerRoomTimeTable.setHour_06(new Hour());
        mockBillerRoomTimeTable.setHour_07(new Hour());
        mockBillerRoomTimeTable.setHour_08(new Hour());
        mockBillerRoomTimeTable.setHour_09(new Hour());
        mockBillerRoomTimeTable.setHour_10(new Hour());

        mockBillerRoomTimeTable.setHour_10(new Hour());
        mockBillerRoomTimeTable.setHour_11(new Hour());
        mockBillerRoomTimeTable.setHour_12(new Hour());
        mockBillerRoomTimeTable.setHour_13(new Hour());
        mockBillerRoomTimeTable.setHour_14(new Hour());
        mockBillerRoomTimeTable.setHour_15(new Hour());
        mockBillerRoomTimeTable.setHour_16(new Hour());
        mockBillerRoomTimeTable.setHour_17(new Hour());
        mockBillerRoomTimeTable.setHour_18(new Hour());
        mockBillerRoomTimeTable.setHour_19(new Hour());
        mockBillerRoomTimeTable.setHour_20(new Hour());

        mockBillerRoomTimeTable.setHour_21(new Hour());
        mockBillerRoomTimeTable.setHour_22(new Hour());
        mockBillerRoomTimeTable.setHour_23(new Hour());
        mockBillerRoomTimeTable.setHour_24(new Hour());

        return mockBillerRoomTimeTable;
    }


}
