package com.biller.testbiller.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.List;

@Slf4j
@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = {"mockBillerCafe","mockBillerRoomTimeTableList"})
public class MockBillerRoom extends BaseEntity {

    @ManyToOne
    private MockBillerCafe mockBillerCafe;

    @Column(name = "name", length = 20 , nullable = false)
    private String name;

    @Column(name = "room_code", length = 20, nullable = false)
    private String roomCode;

    @Column(name = "min_seating", length = 11, nullable = false)
    private int minSeating;

    @Column(name = "max_seating", length = 11, nullable = false)
    private int maxSeating;

    private Integer price;
    private String imgUrl_01;
    private String imgUrl_02;
    private String imgUrl_03;
    private String description;

    @OneToMany(mappedBy = "mockBillerRoom",fetch = FetchType.LAZY)
    private List<MockBillerRoomTimeTable> mockBillerRoomTimeTableList;
}
