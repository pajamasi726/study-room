package com.biller.testbiller.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.List;

@Slf4j
@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = "mockBillerCafeList")
public class MockBiller extends BaseEntity {

    @Column(name = "biller_name", length = 20 , nullable = false)
    private String billerName;

    @Column(name = "biller_code", length = 20, nullable = false)
    private String billerCode;

    private String ipAddress;

    @OneToMany(mappedBy = "mockBiller", fetch = FetchType.LAZY)
    List<MockBillerCafe> mockBillerCafeList;



}
