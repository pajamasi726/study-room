package com.biller.testbiller.entity;

import com.biller.testbiller.model.enumeration.reservation.MockBillerReservationConfirmStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain = true)
public class MockBillerReservationConfirm extends BaseEntity {

    @ManyToOne
    private MockBillerRoom mockBillerRoom;  //룸 id

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private MockBillerReservationConfirmStatus mockBillerReservationConfirmStatus;

    private LocalDate day;              //예약 날짜
    private LocalDateTime startAt;      //시작 시간
    private LocalDateTime endAt;        //끝나는 시간
    private Integer amount;             //금액
    private String payName;             //예약자 명
    private String payBy;               //결제 수단
    private LocalDateTime requestedAt;  //요청 받은 시간
    private LocalDateTime confirmedAt;  //확정 시간
    private String reservationCode;

}
