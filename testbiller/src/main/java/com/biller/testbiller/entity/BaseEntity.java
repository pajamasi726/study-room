package com.biller.testbiller.entity;

import com.biller.testbiller.component.MockEntityListener;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@EntityListeners(MockEntityListener.class)
@MappedSuperclass
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 5573794313913814146L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String updatedBy;


    
}
