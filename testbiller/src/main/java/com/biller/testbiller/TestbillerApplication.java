package com.biller.testbiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestbillerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestbillerApplication.class, args);
    }
}
