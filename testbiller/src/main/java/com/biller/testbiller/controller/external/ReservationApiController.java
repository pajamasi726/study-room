package com.biller.testbiller.controller.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.search.SearchRequest;
import com.biller.testbiller.dto.external.search.SearchResponse;
import com.biller.testbiller.service.external.SearchService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class ReservationApiController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ExternalHeader<SearchResponse> search(@RequestBody ExternalHeader<SearchRequest> searchRequest){
        log.info("검색 요청!!! : {}",searchRequest);
        return searchService.search(searchRequest);
    }

}
