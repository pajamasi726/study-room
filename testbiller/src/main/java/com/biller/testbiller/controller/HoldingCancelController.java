package com.biller.testbiller.controller;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelRequest;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelResponse;
import com.biller.testbiller.service.external.HoldingCancelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class HoldingCancelController {

    @Autowired
    private HoldingCancelService holdingCancelService;

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public ExternalHeader<HoldingCancelResponse> holdingCancel(@RequestBody ExternalHeader<HoldingCancelRequest> request){
        log.info("[HoldingCancelController] cancel method : request = {}",request);
        return holdingCancelService.cancel(request);
    }

}
