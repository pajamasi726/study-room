package com.biller.testbiller.controller;

import com.biller.testbiller.dto.external.SearchCreatedResponse;
import com.biller.testbiller.service.SearchCafeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/api")
public class SearchCafeController {

    @Autowired
    private SearchCafeService searchCafeService;

    @RequestMapping(value = "/cafeList", method = RequestMethod.GET)
    public SearchCreatedResponse search(String start, String end){
        return searchCafeService.search(start, end);
    }


}
