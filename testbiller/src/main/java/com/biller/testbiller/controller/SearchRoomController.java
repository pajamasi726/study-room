package com.biller.testbiller.controller;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.ReservationInfoRequest;
import com.biller.testbiller.dto.external.ReservationInfoResponse;
import com.biller.testbiller.service.SearchRoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api")
public class SearchRoomController {

    @Autowired
    private SearchRoomService searchRoomService;

    @RequestMapping(value = "/searchRoom", method = RequestMethod.POST)
    public ExternalHeader<ReservationInfoResponse> searchRoom(@RequestBody ExternalHeader<ReservationInfoRequest> request){
        log.info("[TestBiller]search={}",request);
        return searchRoomService.searchRoomtable(request);
    }
}
