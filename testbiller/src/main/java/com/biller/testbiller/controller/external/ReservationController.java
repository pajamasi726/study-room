package com.biller.testbiller.controller.external;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.reservation.ReservationRequest;
import com.biller.testbiller.dto.external.reservation.ReservationResponse;
import com.biller.testbiller.process.reservation.MockBillerReservationProcess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/platform/api/reservation")
public class ReservationController {

    @Autowired
    private MockBillerReservationProcess mockBillerReservationProcess;

    @RequestMapping(value = "/holding",method = RequestMethod.POST)
    public ExternalHeader<ReservationResponse> holding(@RequestBody ExternalHeader<ReservationRequest> request){
        log.info("MOCKBILLER REQUEST : {}",request);
        return mockBillerReservationProcess.holding(request);
    }
}
