package com.biller.testbiller.controller;

import com.biller.testbiller.dto.external.ExternalHeader;
import com.biller.testbiller.dto.external.confirm.ConfirmRequest;
import com.biller.testbiller.dto.external.confirm.ConfirmResponse;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelRequest;
import com.biller.testbiller.dto.external.holdingCancel.HoldingCancelResponse;
import com.biller.testbiller.service.external.ConfirmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/confirm")
public class ConfirmController {

    @Autowired
    private ConfirmService confirmService;

    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public ExternalHeader<ConfirmResponse> confirm(@RequestBody ExternalHeader<ConfirmRequest> request){
        log.info("[ConfirmController] confirm method : request = {}",request);
        return confirmService.confirm(request);
    }
}
