package com.biller.testbiller.dto.external.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegisterResponse {

    @JsonProperty("biller_code")
    private String billerCode;

    @JsonProperty("cafe_list")
    private List<Cafe> cafeList;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Cafe{
        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("cafe_code")
        private String cafeCode;

        @JsonProperty("room_list")
        private List<StudyRoom> studyRoom;
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class StudyRoom {

        @JsonProperty("room_name")
        private String roomName;

        @JsonProperty("room_code")
        private String roomCode;

    }

}
