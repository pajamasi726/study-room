package com.biller.testbiller.dto.external.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegisterRequest {

    @JsonProperty("biller_name")
    private String billerName;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("cafe_list")
    private List<Cafe> cafeList;


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Cafe{
        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("room_list")
        private List<Room> roomList;

        @JsonProperty("city")
        private String city;

        @JsonProperty("district")
        private String district;

        @JsonProperty("town")
        private String town;

        @JsonProperty("website")
        private String website;

        @JsonProperty("description")
        private String description;

        @JsonProperty("summary")
        private String summary;

        @JsonProperty("open_at")
        private Integer openAt;

        @JsonProperty("close_at")
        private Integer closeAt;

        @JsonProperty("precaution")
        private String precaution;

        @JsonProperty("facilities")
        private String facilities;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Room{

        @JsonProperty("room_name")
        private String roomName;

        @JsonProperty("min_seating")
        private int minSeating;

        @JsonProperty("max_seating")
        private int maxSeating;

        @JsonProperty("description")
        private String description;

        @JsonProperty("img_url_01")
        private String imgUrl01;

        @JsonProperty("img_url_02")
        private String imgUrl02;

        @JsonProperty("img_url_03")
        private String imgUrl03;

        @JsonProperty("price")
        private int price;

    }
}
