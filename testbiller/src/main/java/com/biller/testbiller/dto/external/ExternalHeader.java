package com.biller.testbiller.dto.external;

import com.biller.testbiller.model.enumeration.BaseResponseCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExternalHeader<T> {

    @JsonProperty("api_name")
    private String apiName;

    @JsonProperty("trans_date_time")
    private String transDateTime;

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    T data;

    public static <T> ExternalHeader<T> getHeader(BaseResponseCode code, String apiName, T data){
        ExternalHeader<T> header = new ExternalHeader()
                .setApiName(apiName)
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .setMessage(code.getMessage())
                .setResponseCode(code.getResponseCode())
                .setData(data);
        return header;
    }
}
