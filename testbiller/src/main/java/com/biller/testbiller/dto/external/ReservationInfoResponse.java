package com.biller.testbiller.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReservationInfoResponse {

    @JsonProperty("hour")
    private List<Hour> list;

    @Data
    public static class Hour{

        @JsonProperty("time")
        int time;

        @JsonProperty("ok")
        boolean ok;

    }
}
