package com.biller.testbiller.dto.external;

import com.biller.testbiller.entity.MockBillerCafe;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchCreatedResponse {

    @JsonProperty("cafe_list")
    private List<ChangedMockBillerCafe> changedMockBillerCafe;

    @Data
    public static class ChangedMockBillerCafe{
        @JsonProperty("cafe_name")
        private String cafeName;
    }
}
