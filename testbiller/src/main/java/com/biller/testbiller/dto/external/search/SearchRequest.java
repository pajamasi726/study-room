package com.biller.testbiller.dto.external.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchRequest {

    @JsonProperty("biller_code")
    String billerCode;

    @JsonProperty("zip_code")
    String zipCode;

    @JsonProperty("room_code")
    String roomCode;

    @JsonProperty("start_date_time")
    String startDateTime;

    @JsonProperty("end_date_time")
    String endDateTime;

    @JsonProperty("person_count")
    Integer personCount;
}
