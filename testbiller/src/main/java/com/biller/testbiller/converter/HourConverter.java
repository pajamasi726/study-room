package com.biller.testbiller.converter;

import com.biller.testbiller.model.timeTable.Hour;
import com.biller.testbiller.util.JsonUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

@Convert
public class HourConverter implements AttributeConverter<Hour,String> {

    @Override
    public String convertToDatabaseColumn(Hour attribute) {
        return JsonUtils.toJson(attribute);
    }

    @Override
    public Hour convertToEntityAttribute(String dbData) {
        return JsonUtils.toObject(dbData,Hour.class).get();
    }
}
