package com.biller.testbiller.reflection;

import com.biller.testbiller.TestbillerApplication;
import com.biller.testbiller.TestbillerApplicationTests;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.model.timeTable.Hour;
import com.biller.testbiller.repository.MockBillerRoomTimeTableRepository;
import com.biller.testbiller.service.MockBillerRoomTimeTableService;
import com.biller.testbiller.service.RoomTimeTableLogicService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestbillerApplication.class})
public class ReflectionTest extends TestbillerApplicationTests {
    @Autowired
    private MockBillerRoomTimeTableRepository mockBillerRoomTimeTableRepository;
    @Autowired
    private RoomTimeTableLogicService roomTimeTableLogicService;

    @Test
    public void 조회테스트() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        MockBillerRoomTimeTable mockBillerRoomTimeTable = mockBillerRoomTimeTableRepository.findById(6L).get();
        LocalDateTime start = LocalDateTime.of(2018,12,3,1,00,00);
        LocalDateTime end = LocalDateTime.of(2018,12,3,3,00,00);
        boolean result = roomTimeTableLogicService.isUseAble(mockBillerRoomTimeTable,start,end);
        log.info("result = {}",result);
    }
    @Test
    public void 세팅테스트() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        MockBillerRoomTimeTable mockBillerRoomTimeTable = mockBillerRoomTimeTableRepository.findById(6L).get();
        LocalDateTime start = LocalDateTime.of(2018,12,3,1,00,00);
        LocalDateTime end = LocalDateTime.of(2018,12,3,3,00,00);
        roomTimeTableLogicService.setUseable(mockBillerRoomTimeTable,start,end,false);
        log.info("hour1={}",mockBillerRoomTimeTable.getHour_01().isUseAble());
        log.info("hour2={}",mockBillerRoomTimeTable.getHour_02().isUseAble());
        log.info("hour3={}",mockBillerRoomTimeTable.getHour_03().isUseAble());
    }
    @Test
    public void test() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Optional<MockBillerRoomTimeTable> optional = mockBillerRoomTimeTableRepository.findById(6L);
        if(optional.isPresent()){
            MockBillerRoomTimeTable mockBillerRoomTimeTable = optional.get();
            List<Integer> list = new ArrayList<>();
            list.add(1);
            list.add(2);
            list.add(3);

            for(int time : list){

                String number = (time<10) ? "0"+time : ""+time;      //01  10
                String getMethodName = "getHour_"+number;

                Method getMethod = mockBillerRoomTimeTable.getClass().getDeclaredMethod(getMethodName,new Class[]{});
                Hour hour = (Hour)getMethod.invoke(mockBillerRoomTimeTable);
                log.info("hour{}={}",number,hour.isUseAble());
            }
        }
    }
    @Test
    public void reflectionTest2() throws InvocationTargetException, IllegalAccessException {
        Optional<MockBillerRoomTimeTable> optional = mockBillerRoomTimeTableRepository.findById(3L);
        if(optional.isPresent()){
            MockBillerRoomTimeTable roomTimeTable = optional.get();
            log.info("roomTimeTable = {}",roomTimeTable);


            Method methodlist[] = roomTimeTable.getClass().getDeclaredMethods();

            for(Method method :  methodlist){
                log.info("{}",method);
            }

            int e = 1;
            boolean bool = false;

            String number = (e<10) ? "0"+e : ""+e;      //01  10
            String setMethodName = "setHour_"+number;

            for(Method method : methodlist){
                if(setMethodName.equals(method.getName())){
                    method.invoke(roomTimeTable,new Hour(bool));
                }
            }
            log.info("change :{}",roomTimeTable);
            mockBillerRoomTimeTableRepository.save(roomTimeTable);
        }
    }
    @Test
    public void reflectionTest() throws InvocationTargetException, IllegalAccessException {
        MockBillerRoomTimeTable roomTimeTable = new MockBillerRoomTimeTable();
        Hour hour = new Hour();
        roomTimeTable.setHour_01(hour);


        // method 리스트
        Method methodlist[] = roomTimeTable.getClass().getDeclaredMethods();

        // 로그용
        log.info("BEFORE RoomTimeTable : {}", roomTimeTable.getHour_01());
        roomTimeTable.getHour_01().setUseHour(false);
        log.info("AFTER RoomTimeTable : {}", roomTimeTable.getHour_01());
        // 로그용


        Method getMethod = null;
        Method setMethod = null;


        // 1 -> 01
        // 2 -> 02
        // 13 -> 13
        String getMethodName = "getHour_01";
        String setMethodName = "setHour_01";

        for (Method method : methodlist) {
            if (getMethodName.equals(method.getName())) {
                getMethod = method;
            }
            if (setMethodName.equals(method.getName())) {
                setMethod = method;
            }
        }

        log.info("getInvoke : {}", getMethod.invoke(roomTimeTable));

        // set 실행
        // Method . invoke << 메소드 실행 get.set // 첫번째 매개변수 해당 메소드를 가지고 있는 객체 즉 class

        // 값을 가져와서 변경
        Hour hour01 = (Hour) getMethod.invoke(roomTimeTable);
        hour01.setUseHour(true);


        // set실행 // 첫번째는 class, 두번째 매개변수 부터는 파라미터
        // setHout(int a, int b , int c){
        //
        // }
        // 실횅 일반  : sethour(1,2,3);
        // 실행 리플렉션 : setHethod.invoke(roomTimeTable,1,2,3);
        // roomTimeTable.setHour_01(hour);

        setMethod.invoke(roomTimeTable,hour01);
        log.info("최종 결과  RoomTimeTable : {}",roomTimeTable.getHour_01());


        // todo 1 , 2, ,3
        // if (int = 1) hour_01 = setHour();
        // 1 -> hour_01 -> list 돌려서 get, set Method 찾고 - > set
    }

}
