package com.biller.testbiller;


import com.biller.testbiller.dto.external.register.RegisterRequest;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.repository.MockBillerCafeRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestbillerApplication.class)
public class Test{

    @Autowired
    private MockBillerCafeRepository mockBillerCafeRepository;

    @org.junit.Test
    public void test(){
        LocalDateTime start = LocalDateTime.of(2018,01,01,00,00,00);
        LocalDateTime end = LocalDateTime.of(2019,01,01,00,00,00);
        //String start = "2018-01-01 00:00:00";
        //String end = "2019-01-01 00:00:00";

        List<MockBillerCafe> mockBillerCafes = mockBillerCafeRepository.findByCreatedAtBetween(start, end);
        log.info("size:{}",mockBillerCafes.size());

    }

}
