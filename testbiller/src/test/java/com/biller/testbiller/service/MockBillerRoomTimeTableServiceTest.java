package com.biller.testbiller.service;

import com.biller.testbiller.TestbillerApplicationTests;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.repository.MockBillerRoomRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class MockBillerRoomTimeTableServiceTest extends TestbillerApplicationTests {

    @Autowired
    private MockBillerRoomTimeTableService mockBillerRoomTimeTableService;

    @Autowired
    private MockBillerRoomRepository mockBillerRoomRepository;

    @Test
    public void createTest(){
        Optional<MockBillerRoom> mockBillerRoom = mockBillerRoomRepository.findById(2L);

        mockBillerRoom.ifPresent(room->{
            MockBillerRoomTimeTable mockBillerRoomTimeTable = MockBillerRoomTimeTable.of(room);
            mockBillerRoomTimeTableService.create(mockBillerRoomTimeTable);
        });
    }

    @Test
    public void readTest(){
    }
}
