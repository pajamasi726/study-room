package com.biller.testbiller.service;

import com.biller.testbiller.TestbillerApplicationTests;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.entity.MockBillerRoom;
import com.biller.testbiller.repository.MockBillerCafeRepository;
import com.biller.testbiller.repository.MockBillerRoomRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class QueryTest extends TestbillerApplicationTests {

    @Autowired
    private MockBillerRoomRepository repository;
    @Autowired
    private MockBillerCafeRepository mockBillerCafeRepository;

    @Test
    public void test(){

        MockBillerCafe cafe = mockBillerCafeRepository.findById(6L).get();
        MockBillerRoom room = repository.findByMockBillerCafeAndName(cafe,"101호").get();
        log.info("room={}",room);
    }
}
