package com.biller.testbiller.service;

import com.biller.testbiller.config.RestTemplateConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestTemplateConfig.class})
public class RegisterTimeOutTest {

    @Autowired
    @Qualifier("shortRestTemplate")
    private RestTemplate restTemplate;

    @Test
    public void 요청시간이_지나_타임아웃이_될때() throws URISyntaxException {
        URI uri = new URI("http://localhost:9090/test/timeout");
        ResponseEntity<String> response;

        int count = 0;

        while(count < 5){
            try {
                response = restTemplate.getForEntity(uri, String.class);
                if(response.getBody().equals("OK"))
                    break;
                log.info("response :{}",response);
            }catch(ResourceAccessException e){
                count++;
                uri = new URI("http://localhost:9090/test/timeout2");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        log.info("count = {}",count);
    }
}
