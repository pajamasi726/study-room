package com.biller.testbiller.service;

import com.biller.testbiller.TestbillerApplicationTests;
import com.biller.testbiller.entity.MockBillerRoomTimeTable;
import com.biller.testbiller.repository.MockBillerRoomTimeTableRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
public class RoomTimeTableLogicServiceTest extends TestbillerApplicationTests {

    @Autowired
    private RoomTimeTableLogicService roomTimeTableLogicService;

    @Autowired
    private MockBillerRoomTimeTableRepository mockBillerRoomTimeTableRepository;

    @Test
    public void useTimeTest(){
        LocalDateTime start = LocalDateTime.of(2018,11,17,13,00,00);
        LocalDateTime end = LocalDateTime.of(2018,11,17,16,00,00);
        List<Integer> useTime = roomTimeTableLogicService.getUseHourList(start,end);
    }

    @Test
    public void isUseAble() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        MockBillerRoomTimeTable mockBillerRoomTimeTable = mockBillerRoomTimeTableRepository.findById(1L).get();
        LocalDateTime start = LocalDateTime.of(2018,11,17,13,00,00);
        LocalDateTime end = LocalDateTime.of(2018,11,17,16,00,00);
        boolean isUseAble = roomTimeTableLogicService.isUseAble(mockBillerRoomTimeTable,start,end);
        log.info("예약 가능 현황 : {}",isUseAble);
    }

}

