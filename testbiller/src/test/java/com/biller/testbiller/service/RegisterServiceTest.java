package com.biller.testbiller.service;

import com.biller.testbiller.TestbillerApplicationTests;
import com.biller.testbiller.service.external.RegisterService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RegisterServiceTest extends TestbillerApplicationTests {

    @Autowired
    private RegisterService registerService;

    /** 빌러 등록 테스트 입니다 */
    @Test
    public void register(){
        registerService.register("하늘사랑스터디");
    }
}
