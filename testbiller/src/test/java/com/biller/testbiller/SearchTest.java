package com.biller.testbiller;

import com.biller.testbiller.dto.external.SearchCreatedResponse;
import com.biller.testbiller.entity.MockBillerCafe;
import com.biller.testbiller.service.SearchCafeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestbillerApplication.class)
public class SearchTest {

    @Autowired
    private SearchCafeService searchCafeService;


    @Test
    public void test(){
        String start = "2018-01-01 00:00:00";
        String end = "2019-01-01 00:00:00";

        SearchCreatedResponse response = searchCafeService.search(start, end);
        log.info("response:{}",response);
        List<SearchCreatedResponse.ChangedMockBillerCafe> list = response.getChangedMockBillerCafe();
        log.info("[list] : {}",list.size());
    }

}
