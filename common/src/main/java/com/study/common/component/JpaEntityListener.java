package com.study.common.component;

import com.study.common.entity.BaseEntity;
import com.study.common.service.PropertiesService;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Component // new JpaEntityListener(); spring context
public class JpaEntityListener {

    @PrePersist
    private void created(BaseEntity baseEntity){
        PropertiesService propertiesService = SpringApplicationContext.getBean(PropertiesService.class);
        baseEntity.setCreatedAt(LocalDateTime.now());
        baseEntity.setCreatedBy(propertiesService.moduleName());
    }

    @PreUpdate
    private void updated(BaseEntity baseEntity){
        PropertiesService propertiesService = SpringApplicationContext.getBean(PropertiesService.class);
        baseEntity.setUpdatedAt(LocalDateTime.now());
        baseEntity.setUpdatedBy(propertiesService.moduleName());
    }
}
