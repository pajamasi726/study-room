package com.study.common.component;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringApplicationContext implements ApplicationContextAware {

        private static ApplicationContext applicationContext;

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext = applicationContext;
        }

        /**
         * class타입으로 bean을 가져온다.
         * @param clazz
         * @return
         */
        public static <T> T getBean(Class<T> clazz) {
            return applicationContext.getBean(clazz);
        }

        /**
         * 이름으로 bean을 가져온다.
         * @param beanName
         * @return
         */
        public static <T> T getBean(String beanName, Class<T> clazz)  {
            return clazz.cast(applicationContext.getBean(beanName));
        }
}
