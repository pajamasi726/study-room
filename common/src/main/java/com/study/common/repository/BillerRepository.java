package com.study.common.repository;

import com.study.common.entity.Biller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillerRepository extends JpaRepository<Biller, Long> {
    Optional<Biller> findByName(String name);
    Optional<Biller> findByBillerCode(String billerCode);
}
