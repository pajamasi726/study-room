package com.study.common.repository;

import com.study.common.entity.Biller;
import com.study.common.entity.Cafe;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CafeRepository extends JpaRepository<Cafe, Long> {


    Optional<Cafe> findByCafeNameAndBiller(String cafeName, Biller biller);
    Optional<Cafe> findByCafeCode(String cafeCode);
    @Query(value="select cafe_code from cafe order by id desc limit 1;",nativeQuery = true)
    String findCafeCode();
    //@Query(value = "select * from cafe where district like '%'?0'%' ", nativeQuery = true)
    //List<Cafe> abc(String keyword);
    List<Cafe> findByDistrictContaining(String keyword);
    @Query(value = "select * from cafe order by heart desc, created_at desc limit 0,4;",nativeQuery = true)
    List<Cafe> findTop4CafeList();
    List<Cafe> findByCafeNameContaining(String keyword);
    List<Cafe> findByCityContaining(String keyword);

}
