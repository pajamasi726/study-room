package com.study.common.repository;

import com.study.common.entity.Cafe;
import com.study.common.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.OptionalInt;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    @Query(value="select room_code from room order by id desc limit 1",nativeQuery = true)
    String findRoomCode();

    Optional<Room> findByNameAndCafe(String roomName, Cafe cafe);
    Optional<Room> findByRoomCode(String roomCode);
}
