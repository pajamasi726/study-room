package com.study.common.repository;

import com.study.common.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    //JpaRepository CRUD쿼리를 저장하고 있는 곳
    Optional<User> findByUserId(String userId);

}
