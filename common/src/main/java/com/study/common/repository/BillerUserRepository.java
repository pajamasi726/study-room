package com.study.common.repository;

import com.study.common.entity.BillerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillerUserRepository extends JpaRepository<BillerUser,Long> {
    BillerUser findByUserName(String userName);
}
