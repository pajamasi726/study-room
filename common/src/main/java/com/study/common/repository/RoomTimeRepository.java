package com.study.common.repository;

import com.study.common.entity.RoomTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomTimeRepository extends JpaRepository<RoomTime,Long> {
    Optional<RoomTime> findByRoomId(Long roomId);
}
