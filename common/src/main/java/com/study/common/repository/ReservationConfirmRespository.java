package com.study.common.repository;

import com.study.common.entity.ReservationConfirm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReservationConfirmRespository extends JpaRepository<ReservationConfirm, Long> {


}
