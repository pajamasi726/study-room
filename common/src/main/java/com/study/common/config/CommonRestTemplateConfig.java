package com.study.common.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommonRestTemplateConfig {

    @Bean(name = "middleRestTemplate")
    public RestTemplate middleConnect(){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(3 *1000); // 연결 대기 시간
        factory.setReadTimeout(3 * 1000);   // 연결 후 데이터 응답 대기 시간
        factory.setHttpClient(HttpClientBuilder.create()
                .setMaxConnTotal(500)       // 서버의 최대 커넷션 수
                .setMaxConnPerRoute(100)    // 한 도메인에 대해서 처리 할수 있는 숫자
                .build()
        );
        return new RestTemplate(factory);
    }

    @Bean(name = "longRestTemplate")
    public RestTemplate longConnect(){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(3 *1000); // 연결 대기 시간
        factory.setReadTimeout(10 * 1000);   // 연결 후 데이터 응답 대기 시간
        factory.setHttpClient(HttpClientBuilder.create()
                .setMaxConnTotal(500)       // 서버의 최대 커넷션 수
                .setMaxConnPerRoute(100)    // 한 도메인에 대해서 처리 할수 있는 숫자
                .build()
        );
        return new RestTemplate(factory);
    }

}
