package com.study.common.config;

import com.study.common.ifs.Circle;
import com.study.common.ifs.Rectangle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public Circle getCircle(){
        return new Circle();
    }

    @Bean
    public Rectangle getRectangle(Circle circle){
        return new Rectangle();
    }
}
