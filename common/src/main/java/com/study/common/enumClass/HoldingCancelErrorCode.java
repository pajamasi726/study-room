package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HoldingCancelErrorCode implements BaseResponseCode{

    OK(0, "OK","취소성공"),
    IMPOSSIBLE_RESERVATION(1, "IMPOSSIBLE","취소 불가능"),
    ERROR(2,"ERROR","기타 오류 발생"),
    NOT_FOUND_CONFIRM(3,"NOT_FOUND","예약 건을 찾을 수 없습니다."),
    NOT_CHANGE(4,"NOT_CHANGE","변경할 수 없는 상태입니다."),
    TIMEOUT(5,"TIMEOUT","응답시간을 초과했습니다.")
    ;

    private Integer id;
    private String responseCode;
    private String message;

}
