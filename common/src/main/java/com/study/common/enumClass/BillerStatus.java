package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BillerStatus implements BaseEnum{

    PRE_REGISTERED(0, "등록대기"),
    REGISTERED(1, "등록상태"),
    UNREGISTERED(2,"해지상태"),
    ;

    private Integer id;
    private String displayName;
}
