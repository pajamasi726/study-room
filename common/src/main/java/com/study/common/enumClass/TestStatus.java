package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TestStatus implements BaseEnum{

    REGISTERED(0, "등록상태"),
    UNREGISTERED(1,"해지상태"),
    ;

    private Integer id;
    private String displayName;
}
