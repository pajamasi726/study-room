package com.study.common.enumClass.status;

import com.study.common.enumClass.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserStatus implements BaseEnum {
    REGISTERED(1,"등록 완료");
    private Integer id;
    private String displayName;
}
