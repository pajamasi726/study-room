package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RegisterErrorCode implements BaseResponseCode{

    OK(0,"OK","등록되었습니다"),
    ALREADY_REGISTERD(1,"ALREADY_REGISTERD","이미 등록되어 있습니다"),
    INAPPROPRIATE(2,"INAPPROPRIATE","부적절한 데이터가 있습니다"),
    ERROR(3,"ERROR","기타 에러가 발생했습니다");

    private Integer id;
    private String responseCode;
    private String message;
}
