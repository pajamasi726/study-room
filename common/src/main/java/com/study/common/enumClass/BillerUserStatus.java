package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BillerUserStatus implements BaseEnum{

    PRE_REGISTERED(0, "등록대기"),
    REGISTERED(1, "등록상태"),
    UNREGISTERED(2,"해지상태"),
    SUSPEND(3,"계정중지"),
    EXPIRED(4,"계정만료")
    ;

    private Integer id;
    private String displayName;
}
