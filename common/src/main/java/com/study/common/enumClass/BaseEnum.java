package com.study.common.enumClass;

public interface BaseEnum {
    Integer getId();
    String getDisplayName();
}
