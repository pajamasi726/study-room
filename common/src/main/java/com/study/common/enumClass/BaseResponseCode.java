package com.study.common.enumClass;

public interface BaseResponseCode {
    public String getMessage();
    public String getResponseCode();
    public Integer getId();
}
