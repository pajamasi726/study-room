package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ReservationErrorCode implements BaseResponseCode{

    OK(0, "OK","예약성공"),
    IMPOSSIBLE_RESERVATION(1, "IMPOSSIBLE","예약이 불가능"),
    ERROR(2,"ERROR","기타 오류 발생");

    private Integer id;
    private String responseCode;
    private String message;
}
