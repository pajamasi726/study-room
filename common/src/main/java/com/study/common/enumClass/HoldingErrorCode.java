package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HoldingErrorCode implements BaseResponseCode {


    OK(0, "OK", "정상적으로 예약되었습니다.", "예약 성공"),
    NOT_FOUND(1, "NOT_FOUND", "해당 정보를 찾을 수 없습니다.", "해당 정보를 찾을 수 없습니다."),
    ERROR(2, "ERROR", "기타에러", "기타 오류"),
    IMPOSSIBLE(3, "IMPOSSIBLE", "예약이 불가능합니다.", "예약이 불가능");

    private Integer id;
    private String responseCode;
    private String message;
    private String description;

}
