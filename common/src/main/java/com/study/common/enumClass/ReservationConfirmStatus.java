package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ReservationConfirmStatus implements BaseEnum {

    WAITING(0, "홀딩대기"),
    CONFIRM(1, "홀딩확정"),
    IMPOSSIBLE(2, "접근불가"),
    CANCEL(3,"무료취소"),
    PAY_CANCEL(4,"유료취소"),
    PLATFORM_PAY_CANCEL(5,"플랫폼 정책 유료캔슬")
    ;

    private Integer id;
    private String displayName;

}

