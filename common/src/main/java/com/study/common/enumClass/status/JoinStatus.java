package com.study.common.enumClass.status;

import com.study.common.enumClass.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JoinStatus implements BaseEnum {

    OK(0,"OK"),
    FAIL(1,"FAIL"),
    DUPLICATE(2,"DUPLICATE")
    ;

    private Integer id;
    private String displayName;
}
