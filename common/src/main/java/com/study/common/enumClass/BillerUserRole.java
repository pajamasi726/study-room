package com.study.common.enumClass;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BillerUserRole implements BaseEnum{

    ROLE_ADMIN(0, "내부어드민","ADMIN"),
    ROLE_PARTNER(1, "외부어드민","PARTNER"),
    ROLE_SUPER(2,"관리자","SUPER"),
    ;

    private Integer id;
    private String displayName;
    private String role;
}
