package com.study.common.enumClass.holdingConfirm;

import com.study.common.enumClass.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HoldingConfirmResponseCode implements BaseResponseCode {

    OK(1,"OK","주문 확정되었습니다","주문 확정"),
    NOT_FOUNT(3,"NOT_FOUND","찾을 수 없는 데이터입니다.","데이터 오류"),
    ERROR(2,"ERROR","기타 오류가 발생하였습니다.","기타 오류"),
    ;

    private Integer id;
    private String responseCode;
    private String message;
    private String description;
}
