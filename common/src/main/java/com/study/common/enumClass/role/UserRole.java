package com.study.common.enumClass.role;


import com.study.common.enumClass.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserRole implements BaseEnum {

    ROLE_MEMBER(1,"일반 회원","MEMBER");

    private Integer id;
    private String displayName;
    private String role;
}
