package com.study.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;


@Slf4j
public class JsonUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String toJson(Object object){
        try {
            String json = objectMapper.writeValueAsString(object);
            log.info("Object Mapper Json :{}",json);
            return json;
        }catch(JsonProcessingException e){
            log.info("ObjectMapper Error : {}",e.getLocalizedMessage());
            return null;
        }
    }

    public static <T> Optional<T> toObject(String json, Class<T> clazz){
        try {
            return Optional.ofNullable(objectMapper.readValue(json,clazz));
        } catch (IOException e) {
            log.info("ObjectMapper Error : {}",e.getLocalizedMessage());
            return Optional.empty();
        }
    }
}
