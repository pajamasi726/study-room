package com.study.common.util;

import com.study.common.dto.external.ExternalHeader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HeaderUtil {

    /** 공통 header 생성 부분 */
    public static ExternalHeader getHeader(String apiName, String responseCode, String message){
        return new ExternalHeader()
                .setApiName(apiName)
                .setResponseCode(responseCode)
                .setMessage(message)
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                ;
    }

}
