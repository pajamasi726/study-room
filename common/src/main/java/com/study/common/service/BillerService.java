package com.study.common.service;

import com.study.common.entity.Biller;
import com.study.common.repository.BillerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class BillerService {

    @Autowired
    private BillerRepository billerRepository;

    public List<Biller> getList() {
        return billerRepository.findAll();
    }

    public String getNextBillerCode() {
        String lastCode = null;
        List<Biller> billerList=billerRepository.findAll();

        if(billerList.size() > 0){
            lastCode = billerList.get(billerList.size()-1).getBillerCode();
        }

        //최초 코드가 없을 때.
        if (lastCode == null || lastCode.isEmpty()) {
            return "B0001";
        }

        //존재하는 코드 있을 시
        char en = lastCode.charAt(0);   //앞에 영문 부분
        int number = Integer.parseInt(lastCode.substring(1, lastCode.length())); //영문 뒷 부분의 숫자
        String num = null;

        //뒤에 숫자가 9999일 때는
        if (number == 9999) {
            en += 1;        //알파벳을 하나 올린다.
            num = "0001";  //숫자를 0001로 초기화
        } else {
            number += 1;
            num = "0000" + number;
            num = num.substring(num.length() - 4, num.length());  //뒤에 4자리만 가져온다
        }
        log.info("en:{},num:{},return:{}", en, num, "" + en + num);
        return "" + en + num;
    }
    public Optional<Biller> findByName(String name){
        return billerRepository.findByName(name);
    }
    public Biller save(Biller biller){
        billerRepository.save(biller);
        return biller;
    }

    public Biller register(Biller biller){
        log.info("[BillerService] register biller : {}",biller);
        Optional<Biller> search = findByName(biller.getName());

        if(search.isPresent()){
            log.info("[BillerService] 기존 빌러 :{}",search.get());
            return search.get();
        }
        biller.setBillerCode(getNextBillerCode());
        save(biller);
        log.info("[BillerService] 신규 등록 :{}",biller);
        return biller;
    }
}
