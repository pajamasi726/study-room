package com.study.common.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service("propertiesService")
public class PropertiesService {

    @Value("${module.name}")
    private String moduleName;

    public String moduleName(){
        return this.moduleName;
    }
}
