package com.study.common.service;

import com.study.common.entity.Cafe;
import com.study.common.entity.ReservationConfirm;
import com.study.common.entity.ReservationHistory;
import com.study.common.entity.Room;
import com.study.common.enumClass.ReservationConfirmStatus;
import com.study.common.repository.ReservationConfirmRespository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class ReservationConfirmService {

    @Autowired
    private ReservationConfirmRespository confirmRespository;

    public ReservationConfirm insertWaiting(Cafe cafe, Room room, Long UserId, LocalDateTime startAt, LocalDateTime endAt, ReservationConfirmStatus status){
        ReservationConfirm reservationConfirm = new ReservationConfirm();
        log.info("startAt : {},endAt : {}",startAt,endAt);
        reservationConfirm.setCafe(cafe);
        reservationConfirm.setRoom(room);
        reservationConfirm.setUserId(UserId);
        reservationConfirm.setStatus(status);
        reservationConfirm.setDay(LocalDate.of(startAt.getYear(),startAt.getMonth(),startAt.getDayOfMonth()));
        reservationConfirm.setStartAt(startAt);
        reservationConfirm.setEndAt(endAt);
        reservationConfirm.setRequested_at(LocalDateTime.now());

        log.info("reservationConfirm = {}",reservationConfirm);
        return save(reservationConfirm);
    }

    public ReservationConfirm save(ReservationConfirm confirm){
        return confirmRespository.save(confirm);
    }

    public Optional<ReservationConfirm> findById(Long id){
        return confirmRespository.findById(id);
    }

}
