package com.study.common.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitMqProducer {

    @Value("${study.mq.name}")
    private String studyRoomTopic;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void put(String message){
        rabbitTemplate.convertAndSend(studyRoomTopic,message);
    }
}
