package com.study.common.service;


import com.study.common.entity.ReservationConfirm;
import com.study.common.entity.ReservationHistory;
import com.study.common.repository.ReservationHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReservationHistoryService {
    @Autowired
    private ReservationHistoryRepository historyRepository;

    public void save(ReservationHistory history){
        historyRepository.save(history);
    }

    public void save(ReservationConfirm confirm){
        ReservationHistory history = new ReservationHistory()
                .setAmount(confirm.getAmount())
                .setCafe(confirm.getCafe())
                .setRoom(confirm.getRoom())
                .setConfirmed_at(confirm.getConfirmed_at())
                .setDay(confirm.getDay())
                .setEndAt(confirm.getEndAt())
                .setPayBy(confirm.getPayBy())
                .setPersonCount(confirm.getPersonCount())
                .setRequested_at(confirm.getRequested_at())
                .setStartAt(confirm.getStartAt())
                .setStatus(confirm.getStatus())
                .setUserId(confirm.getUserId())
                .setPayName(confirm.getPayName());
        save(history);
    }
}
