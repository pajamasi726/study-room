package com.study.common.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpHead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@Service
public class RestTemplateService {

    @Autowired
    @Qualifier("middleRestTemplate")
    private RestTemplate restTemplate;


    public <T> T send(URI uri,
                       HttpMethod httpMethod,
                       Object requestData,
                       ParameterizedTypeReference<T> type) throws Exception{
        // 요청 데이터
        RequestEntity<Object> requestEntity = RequestEntity.method(httpMethod,uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(requestData);


        // 응답 데이터
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestEntity, type);
        log.info("restTemplate response : {} , {}",responseEntity.getStatusCode(),responseEntity.getBody());
        return responseEntity.getBody();
    }

    public <T> T send(URI uri,
                      HttpMethod httpMethod,
                      String headerKey,
                      String headerValue,
                      Object requestData,
                      ParameterizedTypeReference<T> type) throws Exception{
        // 요청 데이터
        RequestEntity<Object> requestEntity = RequestEntity.method(httpMethod,uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(headerKey,headerValue)
                .body(requestData);

        // 응답 데이터
        ResponseEntity<T> responseEntity = restTemplate.exchange(requestEntity, type);
        log.info("restTemplate response : {} , {}",responseEntity.getStatusCode(),responseEntity.getBody());
        return responseEntity.getBody();
    }



}
