package com.study.common.service.example;

import com.study.common.ifs.Circle;
import com.study.common.ifs.DrawInterface;
import com.study.common.ifs.Rectangle;
import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class FacadeService {

    @Autowired
    private Circle circle;

    @Autowired
    private Rectangle rectangle;

    public DrawInterface getCircle(){
        return circle;
    }

    public DrawInterface getRectangle(){
        return rectangle;
    }
}
