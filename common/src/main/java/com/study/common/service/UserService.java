package com.study.common.service;

import com.study.common.entity.User;
import com.study.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getList() {
        return userRepository.findAll();
    }
}
