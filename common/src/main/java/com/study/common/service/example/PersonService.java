package com.study.common.service.example;

import com.study.common.ifs.AUDI;
import com.study.common.ifs.BMW;
import com.study.common.ifs.Normal;
import com.study.common.ifs.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PersonService {

    @Autowired
    private CarServiceFactory carServiceFactory;

    public void person(){

        // 전략
        Person person = new Person()
                .setCarInterface(new BMW())
                .setPersonalityInterface(new Normal());
        person.print();


        // factory
        carServiceFactory.getPerson(person).print();

        Person person2 = new Person()
                .setCarInterface(new AUDI())
                .setPersonalityInterface(new Normal());
        person2.print();

        carServiceFactory.getPerson(person2).print();

    }
}
