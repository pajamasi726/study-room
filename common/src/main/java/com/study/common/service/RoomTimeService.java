package com.study.common.service;

import com.study.common.repository.RoomTimeRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class RoomTimeService {

    @Autowired
    private RoomTimeRepository roomTimeRepository;

    public String roomReservation(Long roomId){
        return roomTimeRepository.findByRoomId(roomId)
                .map(time ->{

                    if(time.getTime01() > 0){
                        time.setTime01(time.getTime01()-1);
                        roomTimeRepository.save(time);
                        return "TIME01";
                    }

                    if(time.getTime02() > 0){
                        time.setTime02(time.getTime02()-1);
                        roomTimeRepository.save(time);
                        return "TIME02";
                    }

                    if(time.getTime03() > 0){
                        time.setTime03(time.getTime03()-1);
                        roomTimeRepository.save(time);
                        return "TIME03";
                    }

                    if(time.getTime04() > 0){
                        time.setTime04(time.getTime04()-1);
                        roomTimeRepository.save(time);
                        return "TIME04";
                    }

                    if(time.getTime05() > 0){
                        time.setTime05(time.getTime05()-1);
                        roomTimeRepository.save(time);
                        return "TIME05";
                    }

                    return "RESERVATION FULL";
                }).orElseGet(()->"ERROR");
    }
}
