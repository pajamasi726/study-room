package com.study.common.service;

import com.study.common.entity.Biller;
import com.study.common.entity.Cafe;
import com.study.common.repository.CafeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class CafeService {
    @Autowired
    private CafeRepository cafeRepository;
    @Autowired
    private RoomService roomService;


    public void save(Cafe cafe) {
        cafeRepository.save(cafe);
    }

    public Optional<Cafe> findByCafeNameAndBiller(String cafeName, Biller biller) {
        return cafeRepository.findByCafeNameAndBiller(cafeName, biller);
    }

    public void register(Cafe cafe){
        log.info("[CafeService] register cafe : {}",cafe);
        Optional<Cafe> optional = cafeRepository.findByCafeNameAndBiller(cafe.getCafeName(),cafe.getBiller());

        if(optional.isPresent()) {
            log.info("기존 카페 : {}",optional.get().getCafeName());
            cafe.setCafeCode(optional.get().getCafeCode());//기존 코드 세팅
        }else{
            cafe.setCafeCode(getCafeCode());//신규 코드 세팅
            cafeRepository.save(cafe);
            log.info("신규 카페 : {}",cafe.getCafeName());
        }
    }



    public String getCafeCode() {

        String lastCode = null;
        List<Cafe> cafes = cafeRepository.findAll();

        if(cafes.size()>0){
            lastCode = cafes.get(cafes.size()-1).getCafeCode();
        }

        //최초 코드가 없을 때.
        if (lastCode == null || lastCode.isEmpty()) {
            return "BZ0001";
        }

        char en = lastCode.charAt(0);   //앞에 영문 부분 -> ex 'B'
        int number = Integer.parseInt(lastCode.substring(2, lastCode.length())); //영문 뒷 부분의 숫자 -> ex 1
        String num = null; // 4자리의 숫자를 만들기 위해 ex -> 0111

        log.info("lastCode:{} ,en:{},number:{}", lastCode, en, number);

        if (number == 9999) {
            en += 1;
            num = "0001";
        } else {
            number += 1;
            num = "0000" + number;
            num = num.substring(num.length() - 4, num.length());  //뒤에 4자리만 가져온다
        }
        log.info("en:{},num:{},return:{}", en, num, "" + en + 'Z' + num);
        return "" + en + 'Z' + num;
    }

    public Cafe findByCafeCode(String cafeCode){
        return cafeRepository.findByCafeCode(cafeCode).orElse(new Cafe());
    }
}
