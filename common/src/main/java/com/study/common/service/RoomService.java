package com.study.common.service;


import com.study.common.entity.Room;
import com.study.common.repository.RoomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Slf4j
@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public Optional<Room> findById(Long id){
        return roomRepository.findById(id);
    }

    public List<Room> getList() {
        return roomRepository.findAll();
    }

    public void save(Room room) {
        roomRepository.save(room);
    }

    public void register(Room room){
        log.info("[RoomService] register room : {}",room);
        Optional<Room> search = roomRepository.findByNameAndCafe(room.getName(),room.getCafe());
        if(!search.isPresent()){
            room.setRoomCode(getRoomCode());
            roomRepository.save(room);
            log.info("[RoomService] 신규 룸 등록 : {}",room.getName());
        }else{
            log.info("[RoomService] 이미 존재하는 방입니다.",room.getName());
        }
    }
    public String getRoomCode() {
        String lastCode = null;
        List<Room> rooms = roomRepository.findAll();

        if(rooms.size()>0){
            lastCode=rooms.get(rooms.size()-1).getRoomCode();
        }
        //최초 코드가 없을 때.
        if (lastCode == null || lastCode.isEmpty()) {
            return "BR0001";
        }

        char en = lastCode.charAt(0);   //앞에 영문 부분 -> ex 'B'
        int number = Integer.parseInt(lastCode.substring(2, lastCode.length())); //영문 뒷 부분의 숫자 -> ex 1
        String num = null; // 4자리의 숫자를 만들기 위해 ex -> 0111

        log.info("lastCode:{} ,en:{},number:{}", lastCode, en, number);

        if (number == 9999) {
            en += 1;
            num = "0001";
        } else {
            number += 1;
            num = "0000" + number;
            num = num.substring(num.length() - 4, num.length());  //뒤에 4자리만 가져온다
        }
        log.info("en:{},num:{},return:{}", en, num, "" + en + 'R' + num);
        return "" + en + 'R' + num;
    }

    public Room findByRoomCode(String roomCode){
        return roomRepository.findByRoomCode(roomCode).orElse(new Room());
    }
}
