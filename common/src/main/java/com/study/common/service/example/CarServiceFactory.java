package com.study.common.service.example;

import com.study.common.ifs.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CarServiceFactory {

    public PersonInterface getPerson(Person person){

        if(person.getCarInterface() instanceof BMW){
            return new FactoryBmw();
        }else{
            return new FactoryAudi();
        }
    }
}
