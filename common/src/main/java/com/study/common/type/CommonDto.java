package com.study.common.type;

import lombok.Data;

@Data
public class CommonDto {
    private String apiName;
    private String transTime;
    private String code;
    private String message;

    private String userName;
    private String address;
    private String billerName;
    private String phoneNumber;
}


