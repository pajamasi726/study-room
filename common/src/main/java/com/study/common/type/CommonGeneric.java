package com.study.common.type;

import lombok.Data;

@Data
public class CommonGeneric<T> {


    private String apiName;
    private String transTime;
    private String code;
    private String message;

    // type 을 알수 없다.
    private T Data;
}
