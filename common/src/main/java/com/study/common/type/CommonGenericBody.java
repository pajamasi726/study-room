package com.study.common.type;

import lombok.Data;

@Data
public class CommonGenericBody {

    private String userName;
    private String address;

}
