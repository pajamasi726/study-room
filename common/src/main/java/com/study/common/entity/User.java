package com.study.common.entity;

import com.study.common.enumClass.role.UserRole;
import com.study.common.enumClass.status.UserStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;


@Data //롬복을 통해 getter,setter만들어주는 역할
@Entity
@Accessors(chain = true)
public class User extends BaseEntity {
    private String userId;
    private String userPassword;
    private String email;
    private String phoneNumber;
    private String name;

    @Enumerated(value = EnumType.STRING)
    private UserStatus status;

    private int failCount;

    @Enumerated(value = EnumType.STRING)
    private UserRole role;
    private LocalDateTime pwChangeDate;
    private LocalDateTime loginAccessDate;


}
