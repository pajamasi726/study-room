package com.study.common.entity;

import com.study.common.converter.DescriptionConverter;
import com.study.common.dto.Description;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;

@Data
@Entity
public class RoomTime extends BaseEntity{


    private Long roomId;
    @Column(name = "time_01")
    private int time01;
    @Column(name = "time_02")
    private int time02;
    @Column(name = "time_03")
    private int time03;
    @Column(name = "time_04")
    private int time04;
    @Column(name = "time_05")
    private int time05;

    @Convert(converter = DescriptionConverter.class)
    private Description description;
}
