package com.study.common.entity;

import com.study.common.enumClass.BillerStatus;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = {"billerUserList","cafeList"})
public class Biller extends BaseEntity {
    private String name;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private BillerStatus billerStatus;

    private String account;
    private String callCenter;
    private String billerCode;
    private String ipAddress;

    @OneToMany(mappedBy = "biller",fetch = FetchType.LAZY)
    private List<BillerUser> billerUserList;

    @OneToMany(mappedBy = "biller",fetch = FetchType.LAZY)
    private List<Cafe> cafeList;
}
