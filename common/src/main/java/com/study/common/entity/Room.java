package com.study.common.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Column;

@Data
@Entity
@Accessors(chain = true)
@ToString(exclude = "cafe")
public class Room extends  BaseEntity{

    @ManyToOne
    private Cafe cafe;

    private String name;
    private int price;
    private String description;
    @Column(name = "img_url_01")
    private String imgUrl01;
    @Column(name = "img_url_02")
    private String imgUrl02;
    @Column(name = "img_url_03")
    private String imgUrl03;
    private int minSeating;
    private int maxSeating;
    private String roomCode;
    private String category;
}
