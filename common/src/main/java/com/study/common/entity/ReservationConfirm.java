package com.study.common.entity;

import com.study.common.enumClass.ReservationConfirmStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain=true)
public class ReservationConfirm extends BaseEntity{

    private static final long serialVersionUID = 1L;

    @ManyToOne
    private Room room;
    @ManyToOne
    private Cafe cafe;

    @Column(name="start_at")
    private LocalDateTime startAt;
    @Column(name = "end_at")
    private LocalDateTime endAt;

    @Enumerated(EnumType.STRING)
    private ReservationConfirmStatus status;

    @Column(name="pay_name")
    private String payName;
    @Column(name = "pay_by")
    private String payBy;
    @Column(name = "day")
    private LocalDate day;
    @Column(name = "requested_at")
    private LocalDateTime requested_at;
    @Column(name = "confirmed_at")
    private  LocalDateTime confirmed_at;
    private int amount;
    @Column(name = "person_count")
    private int personCount;
    @Column(name = "user_id")
    private Long userId;

}

