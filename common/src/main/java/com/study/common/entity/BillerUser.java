package com.study.common.entity;

import com.study.common.enumClass.BillerUserRole;
import com.study.common.enumClass.BillerUserStatus;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@Entity
public class BillerUser extends BaseEntity{

    @ManyToOne
    private Biller biller;

    private String userName;
    private String password;

    @Enumerated(EnumType.STRING)
    private BillerUserStatus status;

    @Enumerated(EnumType.STRING)
    private BillerUserRole role;

    private int loginFailCount;
    private LocalDateTime lastAccessAt;
    private LocalDateTime passwordUpdatedAt;

    public void create(Biller biller, String userName, String encodePassword) {

        this.biller = biller;
        this.userName = userName;
        this.password = encodePassword;
        this.status = BillerUserStatus.PRE_REGISTERED;
        this.role = BillerUserRole.ROLE_ADMIN;
        this.loginFailCount = 0;
        this.lastAccessAt = null;
        this.passwordUpdatedAt = LocalDateTime.now();
    }
}
