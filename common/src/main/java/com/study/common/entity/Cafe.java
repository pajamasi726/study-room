package com.study.common.entity;

import com.study.common.enumClass.CafeStatus;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
@ToString(exclude = {"biller","roomList"})
public class Cafe extends BaseEntity{

    private static final long serialVersionUID = 2013530553985337585L;

    @ManyToOne
    private Biller biller;

    private String cafeCode;
    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private CafeStatus cafeStatus;
    @Column(name = "img_url_01")
    private String imgUrl01;
    @Column(name = "img_url_02")
    private String imgUrl02;
    @Column(name = "img_url_03")
    private String imgUrl03;
    private String cafeName;
    private String category;
    private String website;
    private String description;
    private String summary;
    private Integer openAt;
    private Integer closeAt;
    private String precaution;
    private Integer heart;
    private Integer cancelInterval;     // 취소 가능 시간 간격 시간 단위
    private Integer cancelImpossible;   // 취소 불가능 시간 간격 분 단위

    @Column(name="location_x")
    private String locationX;
    @Column(name ="location_y")
    private String locationY;
    private String city;
    private String district;
    private String town;
    private String facilities;
    @OneToMany(mappedBy = "cafe",fetch = FetchType.EAGER)
    List<Room> roomList;

    public String getAddress(){
        String addressFormat = "%s %s %s";
        return String.format(addressFormat,getCity(),getDistrict(),getTown());
    }
}
