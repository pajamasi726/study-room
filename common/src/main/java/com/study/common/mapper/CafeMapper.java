package com.study.common.mapper;

import com.study.common.dto.CafeDto;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CafeMapper {
    List<CafeDto> getCafeList(Map<String, Object> params);
}
