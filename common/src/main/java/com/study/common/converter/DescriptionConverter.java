package com.study.common.converter;


import com.study.common.dto.Description;
import com.study.common.util.JsonUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DescriptionConverter implements AttributeConverter<Description,String> {
    //Object -> db
    @Override
    public String convertToDatabaseColumn(Description description) {
        return JsonUtils.toJson(description);
    }
    //db -> Object
    @Override
    public Description convertToEntityAttribute(String dbData) {
        return JsonUtils.toObject(dbData,Description.class).get();
    }
}
