package com.study.common.dto;

import com.study.common.component.SpringApplicationContext;
import com.study.common.service.PropertiesService;
import lombok.Data;

@Data
public class ContextTestDto {

    private String serverName;


    public ContextTestDto(){
        PropertiesService propertiesService = SpringApplicationContext.getBean(PropertiesService.class);
        serverName = propertiesService.moduleName();
    }

}
