package com.study.common.dto.internal.holding;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InternalHoldingRequest {

    @JsonProperty("biller_code")
    private String billerCode;

    @JsonProperty("cafe_code")
    private String cafeCode;

    @JsonProperty("room_code")
    private String roomCode;

    @JsonProperty("start_date_time")
    private String startDateTime;

    @JsonProperty("end_date_time")
    private String endDateTime;

    @JsonProperty("person_count")
    private int personCount;

    @JsonProperty("user_id")
    private Long userId;

}
