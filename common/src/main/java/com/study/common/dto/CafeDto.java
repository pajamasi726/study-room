package com.study.common.dto;

import com.study.common.enumClass.CafeStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class CafeDto {

    private Long id;
    private int billerId;
    private CafeStatus cafeStatus;
    private String name;
    private String chain;
    private String website;
    private String description;
    private String summary;
    private LocalDateTime openAt;
    private LocalDateTime closeAt;
    private String precaution;
    private  String imgUrl01;
    private  String imgUrl02;
    private  String imgUrl03;
    private int heart;
    private String city;
    private String district;
    private String town;

}
