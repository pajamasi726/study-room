package com.study.common.dto.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.study.common.enumClass.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommonHeader<T> implements Serializable {

    private static final long serialVersionUID = 26018804600974785L;

    @JsonProperty("api_name")
    private String apiName;

    @JsonProperty("trans_date_time")
    private String transDateTime;

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    T data;

    public static <T> CommonHeader<T> getResponse(String apiName, BaseResponseCode responseCode, T data){
        CommonHeader<T> externalHeader = new CommonHeader<>();
        externalHeader
                .setApiName(apiName)
                .setResponseCode(responseCode.getResponseCode())
                .setMessage(responseCode.getMessage())
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS")))
                .setData(data);
        return externalHeader;
    }

    public static <T> CommonHeader<T> getResponse(String apiName, String responseCode, String message, T data){
        CommonHeader<T> externalHeader = new CommonHeader<>();
        externalHeader
                .setApiName(apiName)
                .setResponseCode(responseCode)
                .setMessage(message)
                .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS")))
                .setData(data);
        return externalHeader;
    }
}
