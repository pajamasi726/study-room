package com.study.common.dto.internal.index;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CafeDetailsRoomResponse {

    @JsonProperty("room_list")
    private List<Room> roomList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Room{

        @JsonProperty("room_id")
        private Long id;

        @JsonProperty("room_name")
        private String roomName;

        @JsonProperty("price")
        private Integer price;

        @JsonProperty("img_url_01")
        private String imgUrl01;
    }
}
