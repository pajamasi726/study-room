package com.study.common.dto.internal.index.topList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TopListResponse {

    @JsonProperty("cafe_list")
    List<CafeList> cafeList;

    @Data
    public static class CafeList{

        @JsonProperty("id")
        private Long id;

        @JsonProperty("category")
        private String category;

        @JsonProperty("address")
        private String address;

        @JsonProperty("description")
        private String description;

        @JsonProperty("web_site_url")
        private String webSiteUrl;

        @JsonProperty("img_url_01")
        private String imgUrl01;

        @JsonProperty("like_count")
        private Integer likeCount;

        @JsonProperty("cafe_name")
        private String cafeName;
    }
}
