package com.study.common.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.study.common.enumClass.BaseResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExternalHeader<T> {

    @JsonProperty("api_name")
    private String apiName;

    @JsonProperty("trans_date_time")
    private String transDateTime;

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    T data;

    public static <T> ExternalHeader<T> getResponse(String apiName,BaseResponseCode responseCode,T data){
       ExternalHeader<T> externalHeader = new ExternalHeader<>();
       externalHeader
               .setApiName(apiName)
               .setResponseCode(responseCode.getResponseCode())
               .setMessage(responseCode.getMessage())
               .setTransDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS")))
               .setData(data);
       return externalHeader;
    }
}
