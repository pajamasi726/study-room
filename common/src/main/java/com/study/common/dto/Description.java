package com.study.common.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Description {

    private String title;
    private String description;
}
