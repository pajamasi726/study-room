package com.study.common.dto.external.holding;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain =true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HoldingFrontRequest {

    @JsonProperty("date_time")
    private String dateTime;

    @JsonProperty("start_at")
    private String startAt;

    @JsonProperty("end_at")
    private String endAt;

    @JsonProperty("room_code")
    private String roomCode;

    @JsonProperty("person_count")
    private Integer personCount;

    @JsonProperty("user_id")
    private String userId;
}
