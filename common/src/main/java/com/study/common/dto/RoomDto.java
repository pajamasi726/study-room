package com.study.common.dto;

import lombok.Data;

@Data
public class RoomDto {

    private String name;
    private int price;
    private String description;
    private String imgUrl01;
    private String imgUrl02;
    private String imgUrl03;
    private int minSeating;
    private int maxSeating;
    private String timeTable;

}
