package com.study.common.dto.internal.index;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CafeDetailsResponse {

        @JsonProperty("id")
        private Long id;

        @JsonProperty("category")
        private String category;

        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("description")
        private String description;

        @JsonProperty("web_site")
        private String webSite;

        @JsonProperty("img_url_01")
        private String imgUrl01;

        @JsonProperty("img_url_02")
        private String imgUrl02;

        @JsonProperty("img_url_03")
        private String imgUrl03;

        @JsonProperty("heart")
        private Integer heart;

        @JsonProperty("location_x")
        private String locationX;

        @JsonProperty("location_y")
        private String locationY;
}
