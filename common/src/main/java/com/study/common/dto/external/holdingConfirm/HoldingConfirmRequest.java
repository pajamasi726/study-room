package com.study.common.dto.external.holdingConfirm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.annotation.sql.DataSourceDefinitions;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class HoldingConfirmRequest {
    @JsonProperty("biller_code")
    private String billerCode;

    @JsonProperty("cafe_code")
    private String cafeCode;

    @JsonProperty("room_code")
    private String roomCode;

    @JsonProperty("start_date_time")
    private String startDateTime;

    @JsonProperty("end_date_time")
    private String endDateTime;

    @JsonProperty("reservation_code")
    private String reservationCode;
}
