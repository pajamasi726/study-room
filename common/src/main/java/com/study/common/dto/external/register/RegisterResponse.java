package com.study.common.dto.external.register;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegisterResponse {

    @JsonProperty("biller_code")
    private String billerCode;

    @JsonProperty("cafe_list")
    private List<BillerCafe> cafeList;


    @Data
    public static class BillerCafe {
        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("cafe_code")
        private String cafeCode;

        @JsonProperty("room_list")
        private List<BillerRoom> studyRoom;
    }

    @Data
    public static class BillerRoom {

        @JsonProperty("room_code")
        private String roomCode;

        @JsonProperty("room_name")
        private String roomName;
    }


}
