package com.study.common.dto.external.register;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegisterRequest {

    @NotBlank(message = "필수 입력 정보입니다.")
    @JsonProperty("biller_name")
    private String billerName;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("cafe_list")
    private List<BillerCafe> cafeList;

    @Data
    public static class BillerCafe {
        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("room_list")
        private List<BillerRoom> roomList;

        @JsonProperty("city")
        private String city;

        @JsonProperty("district")
        private String district;

        @JsonProperty("town")
        private String town;

        @JsonProperty("website")
        private String website;

        @JsonProperty("description")
        private String description;

        @JsonProperty("summary")
        private String summary;

        @JsonProperty("open_at")
        private Integer openAt;

        @JsonProperty("close_at")
        private Integer closeAt;

        @JsonProperty("precaution")
        private String precaution;

        @JsonProperty("facilities")
        private String facilities;

    }

    @Data
    public static class BillerRoom {
        @JsonProperty("room_name")
        private String roomName;

        @JsonProperty("max_seating")
        private int maxSeating;

        @JsonProperty("min_seating")
        private int minSeating;

        @JsonProperty("price")
        private int price;

        @JsonProperty("description")
        private String description;

        @JsonProperty("img_url_01")
        private String imgUrl01;

        @JsonProperty("img_url_02")
        private String imgUrl02;

        @JsonProperty("img_url_03")
        private String imgUrl03;


    }


}


