package com.study.common.dto.internal.room;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RoomInfoResponse {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("category")
    private String category;

    @JsonProperty("img_url_01")
    private String imgUrl_01;

    @JsonProperty("img_url_02")
    private String imgUrl_02;

    @JsonProperty("img_url_03")
    private String imgUrl_03;

    @JsonProperty("description")
    private String description;

    @JsonProperty("max_seating")
    private Integer maxSeating;

    @JsonProperty("min_seating")
    private Integer minSeating;

    @JsonProperty("room_code")
    private String roomCode;

    @JsonProperty("price")
    private Integer price;

}
