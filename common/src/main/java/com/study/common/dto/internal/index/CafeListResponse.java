package com.study.common.dto.internal.index;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CafeListResponse implements Serializable {

    private static final long serialVersionUID = -5908553928846261418L;

    @JsonProperty("cafe_list")
    private List<ChagedCafe> cafeList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class ChagedCafe implements Serializable{

        private static final long serialVersionUID = 2191059967986992834L;

        @JsonProperty("id")
        private Long id;

        @JsonProperty("category")
        private String category;

        @JsonProperty("cafe_name")
        private String cafeName;

        @JsonProperty("title")
        private String title;

        @JsonProperty("description")
        private String description;

        @JsonProperty("web_site")
        private String webSite;

        @JsonProperty("img_url_01")
        private String imgUrl01;

        @JsonProperty("heart")
        private int heart;
    }
}
