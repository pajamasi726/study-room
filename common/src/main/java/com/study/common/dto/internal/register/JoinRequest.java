package com.study.common.dto.internal.register;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class JoinRequest {


    private String userId;
    private String userPassword;
    private String email;
    private String phoneNumber;


}

