package com.study.common.dto.external.kakaoMap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class KakaoMapResponse {

    @JsonProperty("documents")
    private List<Address> address;

    @Data
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Address{

        @JsonProperty("x")
        private String x;

        @JsonProperty("y")
        private String y;
    }
}
