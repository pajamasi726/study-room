package com.study.common.ifs;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MyBatisInterface<T> {
    List<T> getList(Map<String,Object> searchParams);
    T getOne(Long id);
}
