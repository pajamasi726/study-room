package com.study.common.ifs;

import lombok.Data;

@Data
public class FactoryBmw implements PersonInterface{

    private PersonalityInterface personalityInterface = new Hard();
    private CarInterface carInterface = new BMW();

    @Override
    public void print() {
        System.out.println(personalityInterface.personality()+" : "+carInterface.name());
    }
}
