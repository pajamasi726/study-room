package com.study.common.ifs;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Person {
    private PersonalityInterface personalityInterface;
    private CarInterface carInterface;

    public void print(){
        System.out.println(personalityInterface.personality()+" : "+carInterface.name());
    }
}
