package com.study.common.ifs;

import lombok.Data;

@Data
public class FactoryAudi implements PersonInterface{
    private PersonalityInterface personalityInterface = new Hard();
    private CarInterface carInterface = new AUDI();

    @Override
    public void print() {
        System.out.println(personalityInterface.personality()+" : "+carInterface.name());
    }
}
